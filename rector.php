<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths(
        [
            __DIR__.'/web',
            __DIR__.'/protected',
        ]
    );
    $rectorConfig->skip(
        [
            __DIR__.'/protected/extensions',
            __DIR__.'/protected/tests',

        ]
    );

    $rectorConfig->phpVersion(\Rector\Core\ValueObject\PhpVersion::PHP_81);
    // register a single rule
    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
        $rectorConfig->sets(
            [
                LevelSetList::UP_TO_PHP_56,
                LevelSetList::UP_TO_PHP_74,
                LevelSetList::UP_TO_PHP_81,
                LevelSetList::UP_TO_PHP_82,

            ]
        );

        $rectorConfig->rule(\Rector\Php81\Rector\FuncCall\NullToStrictStringFuncCallArgRector::class);

};
