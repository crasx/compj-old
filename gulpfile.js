const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const eslint = require('gulp-eslint');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const jshint = require('gulp-jshint');
const plumber = require('gulp-plumber');


// Define source and output paths
const paths = {
    sassSrc: 'web/css/src/main.scss',
    sassDest: 'web/css/dist',
    sassWatch: 'web/css/src/**/*.scss',
    jsSrc: 'web/js/src/*.js',
    jsDest: 'web/js/dist',
};

// Compile Sass and apply autoprefixer
gulp.task('sass', () => {
    return gulp
        .src(paths.sassSrc)
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.sassDest))
        .pipe(browserSync.stream());
});

// Lint JavaScript files
gulp.task('lint', () => {
    return gulp
        .src(paths.jsSrc)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});
//
// // Compile JavaScript with js
gulp.task('js', () => {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/@popperjs/core/dist/umd/popper-lite.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/js-cookie/dist/js.cookie.js',
        paths.jsSrc
    ])
        // .pipe(plumber({
        //     errorHandler: onError
        // }))
        .pipe(sourcemaps.init())
        .pipe(jshint())
        // .pipe(jshint.reporter('gulp-jshint-html-reporter', {
        //     filename: __dirname + '/jshint-output.html',
        //     createMissingFolders: false
        // }))
        .pipe(concat('all.js'))
        // .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.jsDest))
        ;

});

gulp.task('icons', function() {
    return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulp.dest(paths.sassDest+'/webfonts/'));
});


// Watch for changes and reload the browser
gulp.task('watch', () => {
    browserSync.init({
        proxy: 'https://compj.ddev.site',
    });

    gulp.watch(paths.sassWatch, gulp.series('sass', 'icons'));
    gulp.watch(paths.jsSrc, gulp.series('js')).on('change', browserSync.reload);
    gulp.watch(['protected/views/**/*.php']).on('change', browserSync.reload);
});

// Default task
gulp.task('default', gulp.series('sass', 'icons', 'js', 'watch'));
gulp.task('build', gulp.series('sass', 'icons', 'js'));
