#!/bin/bash

PACKAGES="default-mysql-client \
git \
unzip \
libfreetype6-dev \
libjpeg-dev \
libmemcached-dev \
libpng-dev \
libpq-dev \
libwebp-dev \
libzip-dev"

# Installs x11?
#libmagickwand-dev \


set -eux;

if command -v a2enmod; then
  a2enmod rewrite;
fi

savedAptMark="$(apt-mark showmanual)";

apt-get update;
apt-get install -y --no-install-recommends $PACKAGES ;
  pecl install \
    memcached ;
#    imagick \

docker-php-ext-configure gd \
  --with-freetype \
  --with-jpeg=/usr \
  --with-webp	;

docker-php-ext-install -j "$(nproc)" \
  gd \
  opcache \
  pdo_mysql \
  pdo_pgsql \
  zip \
  bcmath;

docker-php-ext-enable \
  memcached \
;
#  imagick \
apt-mark auto '.*' > /dev/null;
apt-mark manual $savedAptMark > /dev/null;
apt-mark manual $PACKAGES > /dev/null;
apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
