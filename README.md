```shell
docker compose -f deploy/docker-compose.yml build  --push  

helm install --namespace compj-live yii1 deploy/helm --create-namespace  
helm upgrade --namespace compj-live yii1 deploy/helm
kubectl -n compj-live rollout restart deployments yii1-helm      

helm secrets encrypt deploy/config/secrets.yaml > deploy/config/secrets-enc.yaml 
helm secrets install --namespace compj-live yii1 deploy/helm -f deploy/config/secrets-enc.yaml --create-namespace
helm secrets upgrade --namespace compj-live yii1 deploy/helm -f deploy/config/secrets-enc.yaml

```