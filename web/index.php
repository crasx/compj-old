<?php
function __E($d)
{
    return htmlentities((string) $d);

}//end __E()


function __EF($d)
{
    return addslashes((string) $d);
}//end __EF()


if (!empty($_ENV['DDEV_VERSION'])) {
    define('YII_DEBUG', true);
    define('YII_TRACE_LEVEL', 3);
}
else {
    define('YII_DEBUG', false);
    define('YII_TRACE_LEVEL', 0);
}

$yii    = __DIR__.'/../vendor/yiisoft/yii/framework/yii.php';
$config = __DIR__.'/../protected/config/main.php';

require_once $yii;

// change the following paths if necessary
Yii::createWebApplication($config)->run();


if (YII_DEBUG) {
    ?>
    <script type="text/javascript">
        console.log(<?php echo Yii::getLogger()->getExecutionTime();?>);
    </script>
    <?php
}
