<?php
if (empty($_ENV['YII_DEBUG'])) {
    return;
}

// change the following paths if necessary
$yii    = __DIR__.'/../yii/framework/yii.php';
$config = __DIR__.'/../protected/config/gii.php';

require_once $yii;
Yii::createWebApplication($config)->run();
