jQuery(function ($) {

    $('#competitionList').bind('change', function(){getFieldList();});
    if(document.location.hash.length>0){
        var re = new RegExp("#(\\d+)");
        var matches = document.location.hash.match(re);
        if(matches.length>1 && matches[1]!=null){
            $('#competitionList').val(matches[1]);
            getFieldList();
        }
    }

});

function getFieldList(fun){
    var v = $('#competitionList').val();
    if(v==-1)return;
    document.location.hash=v;
    $.get(BASE+'/mc/slideshowFields', {'c':v}, function(data, r){
        $("#fieldList").html(data);
        if(fun!=undefined)fun();
    });
}