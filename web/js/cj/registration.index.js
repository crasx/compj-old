var regtable;
jQuery(function ($) {
    $.fn.dataTable.ext.type.order['cj-time'] = function (settings, col) {
        return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
            return $('span', td).text();
        });
    };

    regtable = $("#registrationTable").DataTable({
        "dom": '<"top">rt<"bottom"lp><"clear">',
        "columnDefs": [
            {"targets": [0,2], "orderable": false},
            {"targets": [3, 4], "type": "cj-time"}
        ],
        "serverSide": true,
        "ajax": $("#registrationTable").data("url"),
        "order": [
            [4, "desc"]
        ],
        lengthMenu:[10,25,30,50, 100],
        pageLength:50,
        "drawCallback": function (settings) {
            autoHidePager(settings, this);
        },
        autoWidth:false
    });

    var reloadTO;
    doReload = function () {
        regtable.ajax.reload(function () {
            if (reloadTO)
                clearTimeout(reloadTO);
            reloadTO = setTimeout("doReload()", 15000);
        }, false);
    };
    doReload();


});


function showSibling(f) {
    c = jQuery(f).siblings("span");
    c.slideToggle(500, function () {
        jQuery(f).text(c.is(":visible") ? "[-]" : "[+]");
    });
}
