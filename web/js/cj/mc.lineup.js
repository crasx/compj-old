var mcList;
var reloadTO = null;
var reloading = false;
var submitting = false;
var currentId = 0;

(function ($) {


    $(document).ready(function () {


        mcList = $("#MCTable").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": [2, 3], "orderDataType": 'dom-checkbox'}
            ],
            "order": [
                [0, "asc"]
            ],
            "ajax": {
                "url": $("#MCTable").data("url"),
                "data": function (d) {
                    d.c = $('#competitionList').val();
                    d.w = $('#show-withdrawn').is(':checked');
                },
                'beforeSend': function () {
                    $('#lineup-refresh .fa-refresh').addClass('fa-spin');
                }
            },
            lengthMenu: [10, 25, 30, 50, 100],
            pageLength: 25,
            initComplete: doReload,
            drawCallback: function (settings) {
                autoHidePager(settings, this);
            },
            "preDrawCallback": function (settings) {
                var api = new $.fn.dataTable.Api(settings);
                $(api.rows().nodes()).removeClass("highlight");
                if (currentId != 0) {
                    $("[data-entry=" + currentId + ']', api.rows().nodes()).first().closest("tr").addClass("highlight");
                }
            },

            rowCallback: function (row, data, index) {
                $('input.status-checkbox', row).bind('change', function () {
                    if ($(this).is('[id^="here"]')) {
                        switchCallback('here', this);
                    } else if ($(this).is('[id^="called"]')) {
                        switchCallback('called', this);
                    }
                });

                $('.contestant-info', row).bind('click', showInformation);
                $('td:eq(0)', row).bind('click', function (e) {  // Click on name
                    $('.contestant-info', row).trigger('click');

                });
            }
        });


        reloading = false;

        $('#lineup-refresh').bind('click', function () {
            doReload();
        });

        $('#competitionList, #show-withdrawn').bind('change', function () {
            doReload();
        });


    });

    function doReload() {
        if (reloadTO != null) {
            clearTimeout(reloadTO);
        }

        if (reloading) {
            console.log("Reload already in progress.");
            return;
        }

        if (submitting) {
            console.log("Submit in progress.");
            return;
        }

        reloading = true;
        $("#lineup-refresh .fa-refresh").addClass("fa-spin");


        // if ($('#competitionList').val() <= 0) {
        //     return;
        // }
        mcList.ajax.reload(function (json) {
            reloadCallback();
        }, false);


    }


    function setCompetitionReloadTimeout() {
        if (reloadTO !== null) {
            clearTimeout(reloadTO);
        }
        reloadTO = setTimeout(doReload, 15000);

    }

    function reloadCallback() {


        $("#lineup-refresh .updated").text("Updated " + new Date().toLocaleTimeString());
        $("#lineup-refresh .fa-refresh").removeClass("fa-spin");


        $.get(BASE + "/mc/competitions", {'type': 'lineup'}, function (data) {
            if (data.success) {
                t = data;
                for (var i = 0; i < data.competitions.length; i++) {
                    $("#competitionList option[value=" + data.competitions[i].id + "]").html(data.competitions[i].name);

                }
            }
            reloading = false;
            setCompetitionReloadTimeout();
        }, "json");
    }


    function switchCallback(type, element) {
        submitting = true;
        var localElement = element;

        // Show spinner
        $('.fa-spin', $(localElement).parent()).removeClass('d-none');

        // Reset elements
        $('.invalid-feedback', $(localElement).parent()).addClass('d-none');
        $(localElement).removeClass('is-invalid');

        $.get(BASE + "/mc/toggleStatus",
            {
                "competition": $(element).data('competition'),
                "entry": $(element).data('entry'),
                "id": $(element).data('id'),
                "status": $(element).is(":checked") ? true : false,

                "type": type
            }, null, 'json')
            .always(function (result) {
                $('.fa-spin', $(localElement).parent()).addClass('d-none');
                if (!result.success) {
                    // Reset checkbox
                    $(localElement).prop('checked', !$(localElement).is(":checked"));
                    // Show error
                    $(localElement).addClass('is-invalid');
                    $('.invalid-feedback', $(localElement).parent()).removeClass('d-none');
                    $('.invalid-feedback', $(localElement).parent()).html(result.message);

                    // Kill timeout - user should retrigger
                    clearTimeout(reloadTO);
                }
                submitting = false;
            });
    }

    function showInformation() {

        $.get(
            BASE + '/mc/entryInfo',
            {
                competition: $(this).data('competition'),
                entry: $(this).data('entry'),
                id: $(this).data('id'),
                "type": "lineup"
            },
            function (data) {
                $("#contestantInfo").html(data);
            }
        )
    }
})
(jQuery);
