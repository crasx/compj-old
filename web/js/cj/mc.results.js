var reloadTO;
var mcList;
jQuery(function ($) {
    mcList = $("#MCTable").DataTable({
        "dom": '<"top">rt<"bottom"lp><"clear">',
        "columnDefs": [
        ],
        "ajax": {
            "url": $("#MCTable").data("url"),
            "data": function (d) {
                d.c = $('#competitionList').val();
            }
        },
        "order": [
            [2, "desc"]
        ],
        "drawCallback": function (settings) {
            autoHidePager(settings, this);
        },
    });

    $('#competitionList').bind('change', function(){doReload();});

});


function populateContestant(c, e){
        $.get(BASE+"/mc/pcontestant",
            {"c":c, "e":e,"type":"results"},
            function(data){
                $("#contestantInfo").html(data);
            }
        );
}


function doReload(){
    if($('#competitionList').val()!=-1){
        mcList.ajax.reload(function(){
            if(reloadTO)
                clearTimeout(reloadTO);
            reloadTO = setTimeout("doReload()", 3000);
        }, false);

    }
}
