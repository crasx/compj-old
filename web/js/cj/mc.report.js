
jQuery(function ($) {
    $("#addFields").fancybox({
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
});


function populateContestant(c, e){
    $.get(BASE+"/mc/pcontestant",
        {"c":c, "e":e, "type":"report"},
        function(data){
            $.fancybox({
                content:data
            });
        }
    );
}


function updatePage(){
    selected = $("#fieldList input:checked");
    document.location='?l='+ $('#numrecords').val()+'&'+selected.serialize();
}
