(function ($) {
    var r_input;
    // var competitionList;
    var selectedCompetitionList;


    $(document).ready(function () {

        competitionList = $("#competitionList").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": [1, 2], "sortable": false}
            ],
            "order": [
                [0, "desc"]
            ],
            lengthMenu: [10, 25, 30, 50],

            "fnDrawCallback": function (oSettings) {
                if (oSettings.fnRecordsDisplay() <= 10) {
                    $('.dataTables_paginate').hide();
                }
                // Bind input fields to update additional fields.
                $('input[name^="registration[competitions]"]', this).change(function () {
                    var cid = $(this).attr('data-cid');
                    var entered = $(this).val() == 'entered';
                    var available = $(this).val() == 'available';
                    var withdrawn = $(this).val() == 'withdrawn';

                    competitionData[cid].selected = !available;
                    competitionData[cid].entryActive = entered;
                    updateCompetitionFields();
                });
            },
        });

        rebuildDataTable();
        updateCompetitionFields();
        $('.uploader').each(function () {
            initUploader(this);
        });

    });

    function rebuildDataTable() {
        competitionList.clear();

        for (var id in competitionData) {
            var row = competitionData[id];
            competitionList.row.add(buildAvailableRow(row));
        }

        competitionList.draw();
    }

    function buildAvailableRow(competition) {

        var competitionName = $('#tpl-competition-name').clone();
        $('.competition-name', competitionName).text(competition.cName);
        $('.competition-event', competitionName).text(competition.event);

        // Register button
        var registerBtn = $("#tpl-registration-checkbox").clone();
        var registerName = "registration[competitions][" + competition.cid + "]";

        // Set name across inputs.
        $('input', registerBtn).each(function () {
            $(this).prop("name", registerName);

            // Set unique id for each input.
            $(this).prop("id", $(this).prop("id") + competition.cid.toString());
            $(this).attr("data-cid", competition.cid);
        });

        // Set "for" property across labels.
        $('label', registerBtn).each(function () {
            $(this).prop("for", $(this).prop("for") + competition.cid);
        });

        // Hide button depending on registration status.
        if (competition.selected) {
            $('.cj-available', registerBtn).addClass('d-none');
            if (competition.entryActive) {
                $('.cj-entered input', registerBtn).attr('checked', 'checked');
            } else {
                $('.cj-withdrawn input', registerBtn).attr('checked', 'checked');
            }
        } else {
            $('.cj-withdrawn', registerBtn).addClass('d-none');
            $('.cj-available input', registerBtn).attr('checked', true);
        }

        // Competition Info
        var competitionInfo = $("#tpl-competition-info").clone();

        if (!competition.entryId) {
            $('.registration-id', competitionInfo).text(competition.nextId);
            $('.actual', competitionInfo).remove();
        } else {
            $('.registration-id', competitionInfo).text(competition.entryId);
            $('.estimated', competitionInfo).remove();
        }

        // Hide lineup if judging is disabled.
        if (!competition.selected) {
            $('.lineup', competitionInfo).remove();
        } else {
            var lineupName = "registration[here][" + competition.cid + "]";
            $('.lineup input', competitionInfo).prop("name", lineupName);
            $('.lineup input', competitionInfo).prop("id", lineupName);
            $('.lineup label', competitionInfo).prop("for", lineupName);
            if (competition.lineupStatus) {
                $('.lineup input', competitionInfo).attr('checked', 'checked');
            }
        }


        return [competitionName.html(), registerBtn.html(), competitionInfo.html()];
    }

    function updateCompetitionFields() {
        $("[data-fid].registration-field-wrapper").addClass('d-none').removeClass('required');
        $("input[data-fid]").prop('required', '');
        $("[data-fid] .required-indicator").addClass('d-none');
        for (var id in competitionData) {

            var competition = competitionData[id];
            if (competition.selected) {
                for(var field in competition.visibleFields) {
                    $("[data-fid=" + competition.visibleFields[field] + "].registration-field-wrapper").removeClass('d-none');
                }
                for(var field in competition.requiredFields) {
                    $("[data-fid=" + competition.visibleFields[field] + "] .required-indicator").removeClass('d-none');
                    $("[data-fid=" + competition.requiredFields[field] + "].registration-field-wrapper").addClass('required');
                    $("input[data-fid=" + competition.requiredFields[field] + "]").prop('required', 'required');

                }

            }
        }
    }

    function updateMaxIds() {
        $ = jQuery;
        if ($(".nextId").length) {
            $.get(BASE + "/register/ajaxIdList", {}, function (data) {
                for (i = 0; i < data.length; i++) {
                    var val = parseInt(data[i].mid) + 1;
                    if (isNaN(val)) val = 1;
                    if (val != $("#next-id-" + data[i].cid).text()) {
                        $("#next-id-" + data[i].cid).fadeOut(100).text(val).fadeIn(100);

                    }
                }
                setTimeout("updateMaxIds()", 3000);
            }, "json");
        }

    }


    function setPhoto(me, id) {
        $ = jQuery;
        row = jQuery(me).closest("tr");
        img = jQuery("img", row);
        r_input.siblings("img.display").attr("src", img.attr("src"));
        r_input.siblings("input[type='hidden']").val(id);
        jQuery.fancybox.close();
        return false;
    }

    function initUploader(obj) {
        $(obj).change(function (e) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                var img = new Image();
                img.onload = function () {
                    var MAX_WIDTH = 800;
                    var MAX_HEIGHT = 800;
                    var width = img.width;
                    var height = img.height;

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                        }
                    }

                    var canvas = document.createElement("canvas");
                    canvas.width = width;
                    canvas.height = height;
                    canvas.getContext("2d").drawImage(this, 0, 0, width, height);
                    var data = canvas.toDataURL();
                    $('img', $(obj).closest('div')).prop('src', data).removeClass('d-none');
                    // showAjaxLoadingOverlay("Uploading photo");
                    // $.ajax({
                    //     type: "POST",
                    //     url: BASE + "/register/upload",
                    //     data: ({imgData: data}),
                    //     cache: false,
                    //     dataType: "json",`1
                    //     success: function (result) {
                    //         closeAjaxLoadingOverlay();
                    //         if (result.success) {
                    //             $('input[type=hidden]', $(obj).closest('div')).val(result.id);
                    //         } else {
                    //             alert(result.error);
                    //         }
                    //     },
                    //     error: function (xhr, result, message) {
                    //         closeAjaxLoadingOverlay();
                    //         alert("Error uploading, please contact support - " + result + '\n' + message);
                    //     }
                    // });
                    //$(this).closest("div").append(this);//remove this if you don't want to show it
                }
                img.src = e.target.result;
            }
            fileReader.readAsDataURL(e.target.files[0]);
        });
    }

})(jQuery);
