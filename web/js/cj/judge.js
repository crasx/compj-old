(function ($) {
    var judgeTable;
    var reloadTO;
    var currentId = 0;
    var reloading = false;
    var submitting = false;

    $(document).ready(function ($) {

        var hash = document.location.hash;
        if (hash) {
            hash = hash.substring(1).split("/");
            if (hash.length > 0) {
                $('#competitionList').val(hash[0]);
            }
        }

        judgeTable = $("#JudgeTable").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": 0, "orderDataType": 'num-html'},
                {"targets": 1, "className": 'clickable'},
                {"targets": 2, "type": 'num-html'}
            ],
            autoWidth: false,

            "ajax": {
                "url": $("#JudgeTable").data("url"),
                "data": function (d) {
                    d.c = $('#competitionList').val();
                }
            },
            "order": [
                [0, "asc"]
            ],
            "drawCallback": function (settings) {
                autoHidePager(settings, this);
            },
            "preDrawCallback": function (settings) {
                var api = new $.fn.dataTable.Api(settings);
                $(api.rows().nodes()).removeClass("highlight");
                if (currentId != 0) {
                    $("[data-entry=" + currentId + ']', api.rows().nodes()).first().closest("tr").addClass("highlight");
                }
            },
            'rowCallback': function (row, data, index) {
                $(row).click(function (e) {
                    populateContestant($('a', this).data("competition"), $('a', this).data("entry"));
                    e.preventDefault()
                });
            },
            'initComplete': function (settings, json) {
                $('.dt-search').val(judgeTable.search());

                if (hash.length == 2) {
                    populateContestant(hash[0], hash[1], function () {
                    doReload();
                    });
                } else {
                    doReload();
                }

            },
            "stateSave": true
        });


        $('#lineup-refresh').bind('click', function () {
            doReload();
        });

        $('#competitionList').bind('change', function () {
            var comp = $('#competitionList').val();
            document.location.hash = comp;
            $('.dt-search').val('');
            judgeTable.search('')
            populateContestant(comp, 0);
            doReload();
        });


    });

    function setCompetitionReloadTimeout(cb) {
        reloadTO = setTimeout(cb, 15000);
    }

    function doReload() {
        if (reloading) {
            console.log("Reload already in progress.");
            return;
        }

        if (submitting) {
            console.log("Submit in progress.");
            return;

        }

        reloading = true;
        $("#lineup-refresh .fa-refresh").addClass("fa-spin");

        if (reloadTO) {
            clearTimeout(reloadTO);
        }

        judgeTable.ajax.reload(reloadCallback, false);
    }


    function reloadCallback() {

        reloading = false;

        $("#lineup-refresh .updated").text("Updated " + new Date().toLocaleTimeString());
        $("#lineup-refresh .fa-refresh").removeClass("fa-spin");


        $.get(BASE + "/judge/competitions", {}, function (data) {
            if (data.success) {
                for (var i = 0; i < data.competitions.length; i++) {
                    $("#competitionList option[value=" + data.competitions[i].id + "]").html(data.competitions[i].name);

                }
            }
            setCompetitionReloadTimeout(doReload);
        }, "json");
    }

    function populateContestant(competition, entry, cb) {
        document.location.hash = competition + "/" + entry;
        currentId = entry;
        judgeTable.draw('page')
        submitting = true;

        $("#entryFields .fields").fadeOut(500);
        $("#scoreList .fields").fadeOut(500, function () {
            if (entry == 0) {
                submitting = false;
                return;
            }
            $.get(BASE + "/judge/contestantData", {"c": competition, "e": entry}, function (data) {
                    if (data.success) {

                        $('#entryFields .fa-refresh').addClass('d-none');
                        $('#entryFields .fields').html(data.fields);
                        $('#scoreList .fields').html('');

                        for (var i = 0; i < data.scores.length; i++) {
                            var tpl = $("#sliderTemplate .sliderHolder").clone();

                            var start = data.scores[i].score ?? data.scores[i].min;
                            $('label', tpl).text(data.scores[i].name);
                            $('input', tpl)
                                .prop('step', data.scores[i].step)
                                .prop('min', data.scores[i].min)
                                .prop('max', data.scores[i].max)
                                .attr('data-entry', entry)
                                .attr('data-criteria', data.scores[i].criteria)
                                .attr('data-competition', competition);

                            $('.max', tpl).text(data.scores[i].max);
                            $('.min', tpl).text(data.scores[i].min);

                            $('input[type=range]', tpl).on('change', function () {
                                $('input[type=number]', $(this).closest('.sliderHolder')).val($(this).val());
                                updateScore($(this));
                            }).val(start);

                            $('input[type=number]', tpl).on('change', function () {
                                $('input[type=range]', $(this).closest('.sliderHolder')).val($(this).val());
                                updateScore($(this));
                            }).val(start);

                            $("#scoreList .fields").append(tpl);
                        }

                        judgeTable.draw('page');

                        $("#scoreList .fields, #entryFields .fields").fadeIn(500);
                    } else {
                        alert("Error loading contestant data.");
                    }
                    if (cb) {
                        cb();
                    }
                    submitting = false;
                },
                'json'
            );
        });
    }

    function updateScore(input) {

        attributes = {
            "comp": input.data("competition"),
            "criteria": input.data("criteria"),
            "entry": input.data("entry"),
            "value": input.val()
        };
        $.get(BASE + "/judge/update", attributes, function (data) {
            data = $.parseJSON(data);
            if (data && data.success) {
                var s = data.score;
                if (s != input.val()) {
                    input.val(s).trigger(change);
                }
            }
            doReload();
        });
    }

})
(jQuery);