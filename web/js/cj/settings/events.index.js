var userTable;

(function ($) {
    $(document).ready(function () {
        if (typeof userAssignments != 'undefined') {
            return;
        }

        $('input[name^=events-ajax]').bind("change", (function () {
            var data = {
                id: $(this).closest('tr').data('event'),
                type: $(this).val(),
                checked: $(this).is(':checked') ? 'on' : ''
            };

            $.post(BASE + '/settings/events/ajaxcb', data,
                function (data) {
                }
            );
        }));

        jQuery("#eventtable").DataTable({
            "dom": '<"top">rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": 0, "sortable": false},
                {"targets": [2, 3, 4, 5], "orderDataType": 'dom-checkbox'}

            ],
            "order": [
                [6, "asc"]
            ],
            "drawCallback": function (settings) {
                autoHidePager(settings, this)
            },
        });

    });

})(jQuery);
