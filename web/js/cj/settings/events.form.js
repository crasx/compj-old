var userTable;

(function ($) {
    $(document).ready(function () {

        if (typeof userAssignments == 'undefined' ) {
            return;
        }
        competitionTable = $("#competitionTable").DataTable({
            "dom": '<"top"rf>t<"bottom"p><"clear">',
            "columnDefs": [
                {"targets": 0, "sortable": false},
            ],
        });
        userTable = jQuery("#usertable").DataTable({
            "sDom": '<"top">rt<"bottom"lp><"clear">',
            "aoColumns": [
                {"bSortable": false},
                null,
                {"bSortable": false}
            ],
            'ajax': function (data, callback, settings) {
                var userTableData = {'data': []};
                for (var user in userAssignments) {
                    var userAssignment = userAssignments[user];

                    var actions = $('#tpl-user-actions').clone();
                    $('a', actions).attr('data-user', user);

                    var nameWrapper = $('#tpl-user-name').clone();
                    $('.user-name', nameWrapper).text(userAssignment.name);
                    $('input[name="event[users][0][name]"]', nameWrapper)
                        .prop('value', userAssignment.email)
                        .prop('name', 'event[users][' + user + '][name]')
                        .attr('data-user', user);

                    var permissionsWrapper = $('<div>')

                    for (var index in userAssignment.permissions) {
                        var permission = userAssignment.permissions[index];
                        // Make sure the permission is in the list of assignable permissions
                        if (userAssignablePermissions[permission] === undefined) {
                            continue;
                        }
                        // Create a pill for the permission
                        var pill = $('#tpl-user-permissions').clone();
                        $('span', pill)
                            .text(userAssignablePermissions[permission]['title'])
                            .addClass('bg-' + userAssignablePermissions[permission]['badge']);
                        $('input', pill).prop('value', permission)
                            .prop('name', 'event[permissions][' + user + '][]')
                            .attr('data-user', user);
                        // Add the pill to the permissions div
                        permissionsWrapper.append(pill.html());
                    }

                    userTableData.data.push([
                        actions.html(),
                        nameWrapper.html(),
                        permissionsWrapper.html(),
                        // userAssignment.permissions,
                    ]);
                }
                callback(userTableData);
            },
            drawCallback: function (settings) {
                autoHidePager(settings, this)
            },
            rowCallback: function (row, data) {

                $('.remove-button', row).click(function () {
                    var id = $(this).data('user');
                    deleteRow(id);
                });

                $('.edit-button', row).click(function () {
                    var id = $(this).data('user');
                    showUserModal(id);
                })
                ;

                // $('.modal .close-button').click(function () {
                //     $(this).closest('.modal').modal('hide');
                // });
                //
                // $('.modal .save-button').click(function () {
                //     updatePermissions(this);
                //     $(this).closest('.modal').modal('hide');
                // });


            }
        });

        $('#userFinderDiv button').click(function () {
            checkValidEmail();
        });

        $("#userFinder").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                checkValidEmail();
            }
        });

        // $('#userFinder').typeahead({
        //         hint: true,
        //         highlight: true,
        //         minLength: 1
        //     },
        //     {
        //         name: 'emails',
        //         displayKey: 'value',
        //         source: substringMatcher(emails)
        //     });

        //
        //
        //     $("#userFinderDiv").removeClass("has-error").removeClass("has-success");
        // }).focus(function () {
        //     $("#userFinderDiv").removeClass("has-error").removeClass("has-success");
        // });

        // Modal save button
        $('#userPermissionsModal .save-button').click(function () {
            var user = $(this).data('user');
            var permissions = [];
            $('#userPermissionsModal input:checked').each(function () {
                permissions.push($(this).val());
            });
            userAssignments[user].permissions = permissions;
            userTable.ajax.reload()
        });


    });

    function showSibling(f) {
        c = jQuery(f).siblings("span");
        c.slideToggle(500, function () {
            jQuery(f).text(c.is(":visible") ? "[-]" : "[+]");
        });
    }

    function showUserModal(userId) {
        var modal = $('#userPermissionsModal');
        var user = userAssignments[userId];
        if (user) {
            var title = user.name ? user.name : user.email;
            $('.modal-title .name', modal).text(title);
            $('.modal-body input[type=checkbox]', modal).each(function () {
                $(this).prop('checked', user.permissions.includes($(this).val()));
            });
            $('.save-button', modal).attr('data-user', userId);
        }

        modal.modal('show');

    }

    function checkValidEmail() {
        var email = $("#userFinder").val();
        // Validate email format
        if (!email.match(/.+@.+\..+/)) {
            alert("Invalid email address");
            return;
        }
        $.get(BASE + '/settings/events/ajaxuser', {email: email},
            function (data) {
                if (data.valid) {
                    var uid = data.data.uid
                    userAssignments[uid] = data.data;
                    userTable.ajax.reload();
                    $('#userFinder').val('');
                    showUserModal(uid);
                } else {
                    alert(data.error ? data.error : "Invalid email address");
                }
            }
            ,
            "json"
        );
    }

    function deleteRow(user) {
        delete userAssignments[user];
        userTable.ajax.reload();
    }

    function updatePermissions(t) {
        var row = $(t).closest('tr');
        $('td:nth-child(3)', row).html('');
        $('input:checked', row).each(function () {
            $('td:nth-child(3)', row).append('<span class="badge rounded-pill btn-' + $(this).data('badge') + '">' + $(this).text() + '</span> ');
        });
    }

    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({value: str});
                }
            });

            cb(matches);
        };
    };
})(jQuery);
