(function ($) {
    var judgeTable;
    var reloadTO;
    var currentId = 0;
    var reloading = false;
    var submitting = false;

    var criteriaTable;
    var registrationTable
    var competitionTable;

    $(document).ready(function ($) {

        // Listing
        competitionTable = $("#competitionTable").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            'columnDefs': [
                {'targets': [0,2,3], 'sortable': false},
            ],
            "order": [[6, "desc"]],
            "drawCallback": function (settings) {
                autoHidePager(settings, this);
            },
            'saveState': true,
        });

        criteriaTable = $("#criteriaTable").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": [0], "sortable": false},
                {"orderDataType": "dom-text-numeric", "targets": [4]},
                {"type": "numeric", "targets": [4]}

            ],
            "order": [[4, "asc"]],
            "drawCallback": function (settings) {
                autoHidePager(settings, this);
            },

        });

        registrationTable = $("#registrationTable").DataTable({
            "dom": '<"top" f>rt<"bottom"lp><"clear">',
            "columnDefs": [
                {"targets": [0], "sortable": false},
                {"orderDataType": "dom-text-numeric", "targets": [6]},
                {"type": "numeric", "targets": [6]}
            ],
            "order": [[6, "asc"]],
            "drawCallback": function (settings) {
                autoHidePager(settings, this);
            },
        });

        $('#addCriteriaButton').click(function (e) {
            showCriteriaModal();
            e.preventDefault()

        });

        $('#criteriaModal button.submit').click(function (e) {
            submitCriteriaModal();
            e.preventDefault()
        })

        $('#createCriteriaModal button.submit').click(function (e) {
            submitAddCriteriaModal();
            e.preventDefault()
        });

        $('#createCriteriaModal button.cancel').click(function (e) {
            showCriteriaModal();
            e.preventDefault()
        });


        $('#addRegistrationButton').click(function (e) {
            showRegistrationModal();
            e.preventDefault()
        });

        $('#registrationModal button.submit').click(function (e) {
            submitRegistrationModal();
            e.preventDefault()
        });

        $('#createRegistrationModal button.submit').click(function (e) {
            submitAddRegistrationModal();
            e.preventDefault()
        });

        $('#createRegistrationModal button.cancel').click(function (e) {
            showRegistrationModal();
            e.preventDefault()
        });


    });


    function removeThisRow(t) {
        $(t).closest("table").DataTable().row($(t).parents("tr")).remove().draw();
    }


//criteria
    function showCriteriaModal() {
        $('.modal').modal('hide');
        var data = {'cu[]': []};
        $("#criteriaTable tr .criteriah").each(function () {
            data['cu[]'].push($(this).val());
        });

        $.get(
            BASE + "/settings/criteria",
            data
        ).done(function (d) {
            $('#criteriaModal .modal-body').html(d);
            $('#criteriaModal .modal-body #addNewCriteriaButton').click(function (e) {
                showAddCriteriaModal();
                e.preventDefault()
            });

            $('#criteriaModal').modal('show');
        });
    }

    function showAddCriteriaModal() {
        $('.modal').modal('hide');
        $.get(
            BASE + "/settings/criteria/add"
        ).done(function (d) {
            $('#createCriteriaModal .modal-body').html(d);
            $('#createCriteriaModal').modal('show');
        });
    }

    function submitAddCriteriaModal() {
        $.ajax({
                type: 'POST',
                dataType: "html",
                url: BASE + "/settings/criteria/add",
                data: $('#createCriteriaModal .modal-body :input').serialize(),
                success: function (d) {
                    if ($('div.added', $(d)).length) {
                        showCriteriaModal();
                        return;
                    }
                    $('#createCriteriaModal .modal-body').html(d);
                },
            }
        );
    }

    function submitCriteriaModal() {
        $("#criteriaModal input[type=checkbox]:checked").closest(".wrapper").each(function () {
            criteriaTable.row.add([
                $('#removeButtons').html(),
                $('.row-template .name', this).html(),
                $('.row-template .min', this).html(),
                $('.row-template .max', this).html(),
                $('.row-template .input', this).html()
            ]).draw();
        });
        $('.modal').modal('hide');
    }

//////////////registration
    function showRegistrationModal() {
        $('.modal').modal('hide');
        var data = {'selected': []};
        $("#registrationTable tr .registrationh").each(function () {
            data['selected'].push($(this).val());
        });
        $.get(
            BASE + "/settings/registration",
            data
        ).done(function (d) {
            $('#registrationModal .modal-body').html(d);

            $('#registrationModal .modal-body #addNewRegistrationButton').click(function (e) {
                showAddRegistrationModal();
                e.preventDefault()
            });

            $('#registrationModal').modal('show');
        });
    }

    function showAddRegistrationModal() {
        $('.modal').modal('hide');
        $.get(
            BASE + "/settings/registration/add"
        ).done(function (d) {
            $('#createRegistrationModal .modal-body').html(d);
            $('#createRegistrationModal').modal('show');
        });
    }

    function submitAddRegistrationModal() {
        $.ajax({
                type: 'POST',
                dataType: "html",
                url: BASE + "/settings/registration/add",
                data: $('#createRegistrationModal .modal-body :input').serialize(),
                success: function (d) {
                    if ($('div.added', $(d)).length) {
                        showRegistrationModal();
                        return;
                    }
                    $('#createRegistrationModal .modal-body').html(d);
                },
            }
        );
    }

//modal callback
    function submitRegistrationModal() {
        $("#registrationModal input[type=checkbox]:checked").siblings("div").each(function () {
            var t = $(this);
            registrationTable.row.add([
                $('#removeButtons').html(),
                $('.name', t).html(),
                $('.required', t).html(),
                $('.mc', t).html(),
                $('.judge', t).html(),
                $('.report', t).html(),
                $('.input', t).html()
            ]).draw();
        });
    }


    function showSibling(f) {
        c = jQuery(f).siblings("span");
        c.slideToggle(500, function () {
            jQuery(f).text(c.is(":visible") ? "[-]" : "[+]");
        });
    }

    function expandAll(type) {
        c = jQuery(".expand." + type).siblings("span");
        $(c).each(function () {
            $(this).slideDown(500, function () {
                var f = $(this).siblings("a");
                jQuery(f).text(c.is(":visible") ? "[-]" : "[+]");
            });

        });
    }
})(jQuery);
