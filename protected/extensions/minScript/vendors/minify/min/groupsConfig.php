<?php
if (!isset($_GET['g'])) {
	return [];
}
if (($group = $this -> minScriptComponent -> minScriptGetGroup($_GET['g'])) === false) {
	return [];
}
return [$_GET['g'] => $group];
