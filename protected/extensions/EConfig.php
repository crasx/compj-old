<?php
class EConfig extends CApplicationComponent {
	final public const CACHE_KEY = 'Extension.Config';
	public $configObject= 'Settings';
	public $cacheID = false;
	public $strictMode = false;
	private $_db;
	private $_cache;
	private $_config;
	public function get($key) {
		$cache = $this->_getCache();
		if (null === $this->_config) {
			$this->_getConfig($cache);
		}
		if (false === is_array($this->_config) || false === array_key_exists($key, $this->_config)) {
			if (true === $this->strictMode) {
				throw new CException("Unable to get value - no entry present with key \"{$key}\".");
			} else {
				return null;
			}
		}
		return (null === $this->_config[$key]) ? null : unserialize($this->_config[$key]);
	}
	public function set($key, $value) {
		$cache = $this->_getCache();
		if (null === $this->_config)
		        		$this->_getConfig($cache);
		$c=$this->configObject;
		if (!is_array($this->_config) || !array_key_exists($key, $this->_config)) {
			if ($this->strictMode) {
				throw new CException("Unable to set value - no entry present with key \"{$key}\".");
			}
			$object=new $c();
			$object->key=$key;
			$object->client=Yii::app()->params['clientid'];
		} else {
			$object=$c::model()->findByAttributes(['key'=>$key, 'client'=>Yii::app()->params['clientid']]);
		}
		$object->value=$this->_config[$key] = serialize($value);
		$object->save();
		if (false !== $cache) {
			$cache->set(self::CACHE_KEY, $this->_config);
		}
	}
	private function _getCache() {
		if (false === $this->cacheID) {
			return false;
		} elseif (null !== $this->_cache) {
			return $this->_cache;
		} elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CCache) {
			return $this->_cache;
		} elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CDummyCache) {
			return $this->_cache;
		} else {
			throw new CException("Config.cacheID \"{$this->cacheID}\" is invalid. Please make sure it refers to the ID of a CCache application component.");
		}
	}
	private function _getConfig($cache) {
		if (false === $cache || false === ($this->_config = $cache->get(self::CACHE_KEY))) {
			$o=$this->configObject;
			$records=$o::model()->findAllByAttributes(['client'=>Yii::app()->params['clientid']]);
			if($records)
			                foreach ($records as $r) 
			      				$this->_config[$r->key] = $r->value;
			if (false !== $cache) {
				$cache->set(self::CACHE_KEY, $this->_config);
			}
		}
	}
}
?>