ALTER TABLE `scores` ADD `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

CREATE TABLE IF NOT EXISTS `results` (
  `competition` int(11) NOT NULL,
  `entry` int(11) NOT NULL,
  `score` double NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `results`
 ADD PRIMARY KEY (`competition`,`entry`), ADD KEY `competition` (`competition`);

