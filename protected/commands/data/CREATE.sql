--
--
-- Name: {{PREFIX}}AuthAssignment; Type: TABLE; Schema: public; Owner: uncrash; Tablespace: 
--

CREATE TABLE "{{PREFIX}}AuthAssignment" (
    itemname character varying(64) NOT NULL,
    userid character varying(64) NOT NULL,
    bizrule text,
    data text
);


ALTER TABLE public."{{PREFIX}}AuthAssignment" OWNER TO uncrash;

--
-- Name: {{PREFIX}}AuthItem; Type: TABLE; Schema: public; Owner: uncrash; Tablespace: 
--

CREATE TABLE "{{PREFIX}}AuthItem" (
    name character varying(64) NOT NULL,
    type integer NOT NULL,
    description text,
    bizrule text,
    data text
);


ALTER TABLE public."{{PREFIX}}AuthItem" OWNER TO uncrash;

--
-- Name: {{PREFIX}}AuthItemChild; Type: TABLE; Schema: public; Owner: uncrash; Tablespace: 
--

CREATE TABLE "{{PREFIX}}AuthItemChild" (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


ALTER TABLE public."{{PREFIX}}AuthItemChild" OWNER TO uncrash;

--
-- Name: {{PREFIX}}competition_criteria; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}competition_criteria (
    competition integer NOT NULL,
    criteria integer NOT NULL
);


ALTER TABLE public.{{PREFIX}}competition_criteria OWNER TO {{USER}};

--
-- Name: {{PREFIX}}competition_fields; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}competition_fields (
    field integer NOT NULL,
    competition integer NOT NULL,
    required boolean
);


ALTER TABLE public.{{PREFIX}}competition_fields OWNER TO {{USER}};

--
-- Name: {{PREFIX}}competitions; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}competitions (
    id integer NOT NULL,
    name character varying,
    event integer,
    type integer,
    description text,
    "maxContestants" integer,
    "bestSelect" integer,
    "bestOrderMatters" boolean,
    "currentId" integer DEFAULT 1
);


ALTER TABLE public.{{PREFIX}}competitions OWNER TO {{USER}};

--
-- Name: {{PREFIX}}competitions_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}competitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}competitions_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}competitions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}competitions_id_seq OWNED BY {{PREFIX}}competitions.id;


--
-- Name: {{PREFIX}}criteria; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}criteria (
    id integer NOT NULL,
    min double precision,
    max double precision,
    name character varying(255),
    description text,
    step double precision
);


ALTER TABLE public.{{PREFIX}}criteria OWNER TO {{USER}};

--
-- Name: {{PREFIX}}criteria_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}criteria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}criteria_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}criteria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}criteria_id_seq OWNED BY {{PREFIX}}criteria.id;


--
-- Name: {{PREFIX}}entry; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}entry (
    id integer NOT NULL,
    name character varying(255),
    description text,
    registered timestamp without time zone
);


ALTER TABLE public.{{PREFIX}}entry OWNER TO {{USER}};

--
-- Name: {{PREFIX}}entry_competitions; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}entry_competitions (
    entry integer NOT NULL,
    competition integer NOT NULL,
    id integer,
    here boolean DEFAULT false
);


ALTER TABLE public.{{PREFIX}}entry_competitions OWNER TO {{USER}};

--
-- Name: {{PREFIX}}entry_fields; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}entry_fields (
    entry integer NOT NULL,
    field integer NOT NULL,
    value text
);


ALTER TABLE public.{{PREFIX}}entry_fields OWNER TO {{USER}};

--
-- Name: {{PREFIX}}entry_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}entry_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}entry_id_seq OWNED BY {{PREFIX}}entry.id;


--
-- Name: {{PREFIX}}events; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}events (
    id integer NOT NULL,
    name character varying(255),
    "publicPermissions" integer,
    description text,
    registration boolean DEFAULT false NOT NULL,
    mc boolean DEFAULT false NOT NULL,
    judge boolean DEFAULT false NOT NULL,
    finished boolean DEFAULT false NOT NULL
);


ALTER TABLE public.{{PREFIX}}events OWNER TO {{USER}};

--
-- Name: {{PREFIX}}events_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}events_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}events_id_seq OWNED BY {{PREFIX}}events.id;


--
-- Name: {{PREFIX}}fields; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}fields (
    id integer NOT NULL,
    name character varying,
    type integer,
    description text,
    field boolean,
    mc boolean,
    judge boolean
);


ALTER TABLE public.{{PREFIX}}fields OWNER TO {{USER}};

--
-- Name: {{PREFIX}}fields_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}fields_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}fields_id_seq OWNED BY {{PREFIX}}fields.id;


--
-- Name: {{PREFIX}}results; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}results (
    entry integer NOT NULL,
    competition integer NOT NULL,
    score real
);


ALTER TABLE public.{{PREFIX}}results OWNER TO {{USER}};

--
-- Name: {{PREFIX}}scores; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}scores (
    criteria integer NOT NULL,
    score integer NOT NULL,
    "user" integer NOT NULL,
    entry integer NOT NULL,
    competition integer NOT NULL
);


ALTER TABLE public.{{PREFIX}}scores OWNER TO {{USER}};

--
-- Name: {{PREFIX}}settings; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}settings (
    key character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.{{PREFIX}}settings OWNER TO {{USER}};

--
-- Name: {{PREFIX}}user_events; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}user_events (
    "user" integer NOT NULL,
    event integer NOT NULL
);


ALTER TABLE public.{{PREFIX}}user_events OWNER TO {{USER}};

--
-- Name: {{PREFIX}}users; Type: TABLE; Schema: public; Owner: {{USER}}; Tablespace: 
--

CREATE TABLE {{PREFIX}}users (
    id integer NOT NULL,
    username character varying(255),
    email character varying(255),
    salt character varying(23),
    password character varying(255),
    "lastLogin" timestamp without time zone,
    "resetHash" character varying(128),
    permissions integer
);


ALTER TABLE public.{{PREFIX}}users OWNER TO {{USER}};

--
-- Name: {{PREFIX}}users_id_seq; Type: SEQUENCE; Schema: public; Owner: {{USER}}
--

CREATE SEQUENCE {{PREFIX}}users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.{{PREFIX}}users_id_seq OWNER TO {{USER}};

--
-- Name: {{PREFIX}}users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: {{USER}}
--

ALTER SEQUENCE {{PREFIX}}users_id_seq OWNED BY {{PREFIX}}users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competitions ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}competitions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}criteria ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}criteria_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}entry ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}events ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}fields ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}fields_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}users ALTER COLUMN id SET DEFAULT nextval('{{PREFIX}}users_id_seq'::regclass);


--
-- Name: criteria_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}criteria
    ADD CONSTRAINT criteria_pk PRIMARY KEY (id);


--
-- Name: {{PREFIX}}AuthAssignment_pkey; Type: CONSTRAINT; Schema: public; Owner: uncrash; Tablespace: 
--

ALTER TABLE ONLY "{{PREFIX}}AuthAssignment"
    ADD CONSTRAINT "{{PREFIX}}AuthAssignment_pkey" PRIMARY KEY (itemname, userid);


--
-- Name: {{PREFIX}}AuthItemChild_pkey; Type: CONSTRAINT; Schema: public; Owner: uncrash; Tablespace: 
--

ALTER TABLE ONLY "{{PREFIX}}AuthItemChild"
    ADD CONSTRAINT "{{PREFIX}}AuthItemChild_pkey" PRIMARY KEY (parent, child);


--
-- Name: {{PREFIX}}AuthItem_pkey; Type: CONSTRAINT; Schema: public; Owner: uncrash; Tablespace: 
--

ALTER TABLE ONLY "{{PREFIX}}AuthItem"
    ADD CONSTRAINT "{{PREFIX}}AuthItem_pkey" PRIMARY KEY (name);


--
-- Name: {{PREFIX}}competitiion_fields_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}competition_fields
    ADD CONSTRAINT {{PREFIX}}competitiion_fields_pk PRIMARY KEY (field, competition);


--
-- Name: {{PREFIX}}competition_criteria_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}competition_criteria
    ADD CONSTRAINT {{PREFIX}}competition_criteria_pk PRIMARY KEY (competition, criteria);


--
-- Name: {{PREFIX}}competitions_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}competitions
    ADD CONSTRAINT {{PREFIX}}competitions_pk PRIMARY KEY (id);


--
-- Name: {{PREFIX}}entry_competitions_id_uk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}entry_competitions
    ADD CONSTRAINT {{PREFIX}}entry_competitions_id_uk UNIQUE (entry, competition, id);


--
-- Name: {{PREFIX}}entry_competitions_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}entry_competitions
    ADD CONSTRAINT {{PREFIX}}entry_competitions_pk PRIMARY KEY (entry, competition);


--
-- Name: {{PREFIX}}entry_fields_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}entry_fields
    ADD CONSTRAINT {{PREFIX}}entry_fields_pk PRIMARY KEY (entry, field);


--
-- Name: {{PREFIX}}entry_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}entry
    ADD CONSTRAINT {{PREFIX}}entry_pk PRIMARY KEY (id);


--
-- Name: {{PREFIX}}fields_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}fields
    ADD CONSTRAINT {{PREFIX}}fields_pk PRIMARY KEY (id);


--
-- Name: {{PREFIX}}results_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}results
    ADD CONSTRAINT {{PREFIX}}results_pk PRIMARY KEY (entry, competition);


--
-- Name: {{PREFIX}}scores_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}scores
    ADD CONSTRAINT {{PREFIX}}scores_pk PRIMARY KEY (criteria, "user", entry, competition);


--
-- Name: {{PREFIX}}user_events_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}user_events
    ADD CONSTRAINT {{PREFIX}}user_events_pk PRIMARY KEY ("user", event);


--
-- Name: {{PREFIX}}users_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}users
    ADD CONSTRAINT {{PREFIX}}users_pk PRIMARY KEY (id);


--
-- Name: events_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}events
    ADD CONSTRAINT events_pk PRIMARY KEY (id);


--
-- Name: settings_pk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}settings
    ADD CONSTRAINT settings_pk PRIMARY KEY (key);


--
-- Name: username_email_uk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}users
    ADD CONSTRAINT username_email_uk UNIQUE (email);


--
-- Name: users_username_uk; Type: CONSTRAINT; Schema: public; Owner: {{USER}}; Tablespace: 
--

ALTER TABLE ONLY {{PREFIX}}users
    ADD CONSTRAINT users_username_uk UNIQUE (username);


--
-- Name: {{PREFIX}}AuthAssignment_itemname_fkey; Type: FK CONSTRAINT; Schema: public; Owner: uncrash
--

ALTER TABLE ONLY "{{PREFIX}}AuthAssignment"
    ADD CONSTRAINT "{{PREFIX}}AuthAssignment_itemname_fkey" FOREIGN KEY (itemname) REFERENCES "{{PREFIX}}AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: {{PREFIX}}AuthItemChild_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: uncrash
--

ALTER TABLE ONLY "{{PREFIX}}AuthItemChild"
    ADD CONSTRAINT "{{PREFIX}}AuthItemChild_child_fkey" FOREIGN KEY (child) REFERENCES "{{PREFIX}}AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: {{PREFIX}}AuthItemChild_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: uncrash
--

ALTER TABLE ONLY "{{PREFIX}}AuthItemChild"
    ADD CONSTRAINT "{{PREFIX}}AuthItemChild_parent_fkey" FOREIGN KEY (parent) REFERENCES "{{PREFIX}}AuthItem"(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: {{PREFIX}}competition_criteria_competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competition_criteria
    ADD CONSTRAINT {{PREFIX}}competition_criteria_competition_fk FOREIGN KEY (competition) REFERENCES {{PREFIX}}competitions(id);


--
-- Name: {{PREFIX}}competition_criteria_criteria_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competition_criteria
    ADD CONSTRAINT {{PREFIX}}competition_criteria_criteria_fk FOREIGN KEY (criteria) REFERENCES {{PREFIX}}criteria(id);


--
-- Name: {{PREFIX}}competition_event_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competitions
    ADD CONSTRAINT {{PREFIX}}competition_event_fk FOREIGN KEY (event) REFERENCES {{PREFIX}}events(id);


--
-- Name: {{PREFIX}}competition_fields_competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competition_fields
    ADD CONSTRAINT {{PREFIX}}competition_fields_competition_fk FOREIGN KEY (competition) REFERENCES {{PREFIX}}competitions(id);


--
-- Name: {{PREFIX}}competition_fields_field_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}competition_fields
    ADD CONSTRAINT {{PREFIX}}competition_fields_field_fk FOREIGN KEY (field) REFERENCES {{PREFIX}}fields(id);


--
-- Name: {{PREFIX}}entry_competitions_competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}entry_competitions
    ADD CONSTRAINT {{PREFIX}}entry_competitions_competition_fk FOREIGN KEY (competition) REFERENCES {{PREFIX}}competitions(id);


--
-- Name: {{PREFIX}}entry_competitions_entry_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}entry_competitions
    ADD CONSTRAINT {{PREFIX}}entry_competitions_entry_fk FOREIGN KEY (entry) REFERENCES {{PREFIX}}entry(id);


--
-- Name: {{PREFIX}}entry_fields_entry_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}entry_fields
    ADD CONSTRAINT {{PREFIX}}entry_fields_entry_fk FOREIGN KEY (entry) REFERENCES {{PREFIX}}entry(id);


--
-- Name: {{PREFIX}}entry_fields_field_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}entry_fields
    ADD CONSTRAINT {{PREFIX}}entry_fields_field_fk FOREIGN KEY (field) REFERENCES {{PREFIX}}fields(id);


--
-- Name: {{PREFIX}}fields_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}fields
    ADD CONSTRAINT {{PREFIX}}fields_type_fk FOREIGN KEY (type) REFERENCES field_types(id);


--
-- Name: {{PREFIX}}results_entry_competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}results
    ADD CONSTRAINT {{PREFIX}}results_entry_competition_fk FOREIGN KEY (entry, competition) REFERENCES {{PREFIX}}entry_competitions(entry, competition);


--
-- Name: {{PREFIX}}score_criteria_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}scores
    ADD CONSTRAINT {{PREFIX}}score_criteria_fk FOREIGN KEY (criteria, competition) REFERENCES {{PREFIX}}competition_criteria(criteria, competition);


--
-- Name: {{PREFIX}}score_entry_competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}scores
    ADD CONSTRAINT {{PREFIX}}score_entry_competition_fk FOREIGN KEY (entry, competition) REFERENCES {{PREFIX}}entry_competitions(entry, competition);


--
-- Name: {{PREFIX}}scores_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}scores
    ADD CONSTRAINT {{PREFIX}}scores_user_fk FOREIGN KEY ("user") REFERENCES {{PREFIX}}users(id);


--
-- Name: {{PREFIX}}user_events_event_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}user_events
    ADD CONSTRAINT {{PREFIX}}user_events_event_fk FOREIGN KEY (event) REFERENCES {{PREFIX}}events(id);


--
-- Name: {{PREFIX}}user_events_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: {{USER}}
--

ALTER TABLE ONLY {{PREFIX}}user_events
    ADD CONSTRAINT {{PREFIX}}user_events_user_fk FOREIGN KEY ("user") REFERENCES {{PREFIX}}users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: {{PREFIX}}AuthAssignment; Type: ACL; Schema: public; Owner: uncrash
--

REVOKE ALL ON TABLE "{{PREFIX}}AuthAssignment" FROM PUBLIC;
REVOKE ALL ON TABLE "{{PREFIX}}AuthAssignment" FROM uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthAssignment" TO uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthAssignment" TO PUBLIC;
GRANT ALL ON TABLE "{{PREFIX}}AuthAssignment" TO {{USER}};


--
-- Name: {{PREFIX}}AuthItem; Type: ACL; Schema: public; Owner: uncrash
--

REVOKE ALL ON TABLE "{{PREFIX}}AuthItem" FROM PUBLIC;
REVOKE ALL ON TABLE "{{PREFIX}}AuthItem" FROM uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthItem" TO uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthItem" TO {{USER}};


--
-- Name: {{PREFIX}}AuthItemChild; Type: ACL; Schema: public; Owner: uncrash
--

REVOKE ALL ON TABLE "{{PREFIX}}AuthItemChild" FROM PUBLIC;
REVOKE ALL ON TABLE "{{PREFIX}}AuthItemChild" FROM uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthItemChild" TO uncrash;
GRANT ALL ON TABLE "{{PREFIX}}AuthItemChild" TO {{USER}};


--
-- Name: {{PREFIX}}competition_criteria; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}competition_criteria FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}competition_criteria FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competition_criteria TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competition_criteria TO uncrash;


--
-- Name: {{PREFIX}}competition_fields; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}competition_fields FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}competition_fields FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competition_fields TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competition_fields TO uncrash;


--
-- Name: {{PREFIX}}competitions; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}competitions FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}competitions FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competitions TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}competitions TO uncrash;


--
-- Name: {{PREFIX}}competitions_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}competitions_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}competitions_id_seq FROM {{USER}};
GRANT SELECT,UPDATE ON SEQUENCE {{PREFIX}}competitions_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}competitions_id_seq TO uncrash;


--
-- Name: {{PREFIX}}criteria; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}criteria FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}criteria FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}criteria TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}criteria TO uncrash;


--
-- Name: {{PREFIX}}criteria_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}criteria_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}criteria_id_seq FROM {{USER}};
GRANT SELECT,UPDATE ON SEQUENCE {{PREFIX}}criteria_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}criteria_id_seq TO uncrash;


--
-- Name: {{PREFIX}}entry; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}entry FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}entry FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry TO uncrash;


--
-- Name: {{PREFIX}}entry_competitions; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}entry_competitions FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}entry_competitions FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry_competitions TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry_competitions TO uncrash;


--
-- Name: {{PREFIX}}entry_fields; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}entry_fields FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}entry_fields FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry_fields TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}entry_fields TO uncrash;


--
-- Name: {{PREFIX}}entry_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}entry_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}entry_id_seq FROM {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}entry_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}entry_id_seq TO uncrash;


--
-- Name: {{PREFIX}}events; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}events FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}events FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}events TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}events TO uncrash;


--
-- Name: {{PREFIX}}events_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}events_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}events_id_seq FROM {{USER}};
GRANT SELECT,UPDATE ON SEQUENCE {{PREFIX}}events_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}events_id_seq TO uncrash;


--
-- Name: {{PREFIX}}fields; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}fields FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}fields FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}fields TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}fields TO uncrash;


--
-- Name: {{PREFIX}}fields_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}fields_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}fields_id_seq FROM {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}fields_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}fields_id_seq TO uncrash;


--
-- Name: {{PREFIX}}results; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}results FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}results FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}results TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}results TO uncrash;


--
-- Name: {{PREFIX}}scores; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}scores FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}scores FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}scores TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}scores TO uncrash;


--
-- Name: {{PREFIX}}settings; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}settings FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}settings FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}settings TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}settings TO uncrash;


--
-- Name: {{PREFIX}}user_events; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}user_events FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}user_events FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}user_events TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}user_events TO uncrash;


--
-- Name: {{PREFIX}}users; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE {{PREFIX}}users FROM PUBLIC;
REVOKE ALL ON TABLE {{PREFIX}}users FROM {{USER}};
GRANT ALL ON TABLE {{PREFIX}}users TO {{USER}};
GRANT ALL ON TABLE {{PREFIX}}users TO uncrash;


--
-- Name: {{PREFIX}}users_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE {{PREFIX}}users_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE {{PREFIX}}users_id_seq FROM {{USER}};
GRANT SELECT,UPDATE ON SEQUENCE {{PREFIX}}users_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE {{PREFIX}}users_id_seq TO uncrash;


--
-- Name: field_types_id_seq; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON SEQUENCE field_types_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE field_types_id_seq FROM {{USER}};
GRANT ALL ON SEQUENCE field_types_id_seq TO {{USER}};
GRANT ALL ON SEQUENCE field_types_id_seq TO uncrash;


--
-- Name: field_types; Type: ACL; Schema: public; Owner: {{USER}}
--

REVOKE ALL ON TABLE field_types FROM PUBLIC;
REVOKE ALL ON TABLE field_types FROM {{USER}};
GRANT ALL ON TABLE field_types TO {{USER}};
GRANT ALL ON TABLE field_types TO uncrash;


--
-- PostgreSQL database dump complete
--

