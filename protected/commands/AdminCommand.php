<?php
class AdminCommand extends CConsoleCommand
{


    public function actionRecache()
    {
        $dir         = Yii::app()->basePath."/runtime/cache";
        $myDirectory = opendir($dir);
        // get each entry
        while ($entryName = readdir($myDirectory)) {
            $dirArray[] = $entryName;
        }

        // close directory
        closedir($myDirectory);
        // count elements in array
        $indexCount = count($dirArray);
        print ("$indexCount files\n");
        // sort 'em
        sort($dirArray);
        // print 'em
        for ($index = 0; $index < $indexCount; $index++) {
            print("$dirArray[$index]\t");
            if (!str_starts_with("$dirArray[$index]", ".")) {
                var_dump(unlink($dir."/".$dirArray[$index]));
            } else {
                print("Ignored\n");
            }
        }

    }//end actionRecache()


}//end class
