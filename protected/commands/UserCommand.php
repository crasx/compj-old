<?php
class UserCommand extends CConsoleCommand
{


    public function actionAdd($args=[])
    {

        if (sizeof($args) < 3) {
            print("Usage: user add <username> <email> <password> <permissions=0>\n");
            exit;
        }

        $u           = new User;
        $u->username = $args[0];
        $u->email    = $args[1];
        $u->makeHashedPassword($args[2]);
        $u->permissions = sizeof($args) >= 4 ? $args[3] : 0;

        if ($u->save()) {
            print("User created. Id:".$u->id."\n");
        } else {
            print_r($u->getErrors());
        }

    }//end actionAdd()


}//end class
