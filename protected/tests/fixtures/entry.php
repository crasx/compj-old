<?php
return [
    [
        'client'     => CJCLIENTID,
        'name'       => 'Test'.random_int(0, 99999),
        'registered' => new CDbExpression('NOW()'),
    ],
];
