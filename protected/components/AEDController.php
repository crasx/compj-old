<?php

class AEDController extends ControllerBase
{

    protected $_CLASS = "";

    protected $_OBJ = "";

    protected $_NAME = "";

    protected $_NAMEPLURAL = "";

    protected $_PATH = "";

    protected $callback = false;

    protected $ajaxIndex = 1;


    public function beforeAction($action)
    {
        if ($action->id == 'add' || $action->id == 'clone') {
            $this->checkLicense();
        }

        return parent::beforeAction($action);

    }//end beforeAction()


    protected function checkLicense()
    {
        $c = $this->_CLASS;
        // make sure license can handle this
        $license = Yii::app()->params['license'];
        $currentCount = Yii::app()->params['Client']->{"$c" . "Count"};
        if ($license && isset($license->$c) && $currentCount >= $license->$c) {
            Yii::app()->user->setFlash('error', 'Your license only allows for ' . $license->$c . ' ' . strtolower($license->$c == 1 ? $this->_NAME : $this->_NAMEPLURAL));
            header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            exit;
        } else if ($license && !isset($license->$c)) {
            // log error
        }

    }//end checkLicense()


    public function actionClone()
    {
        $c = $this->_CLASS;

        $id = (int)$_GET['id'];
        $cid = Yii::app()->params['clientid'];
        $obj = $c::model()->findByAttributes(['id' => $id, 'client' => $cid]);
        if ($obj == null) {
            $this->actionNotFound();
            return;
        }
        if ($this->pullForm($obj, $this->_OBJ, 'clone')) {
            if ($obj->clone()) {
                Yii::app()->user->setFlash('success', $this->_NAME . ' cloned successfully, editing clone');
                header("Location: " . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/edit/" . $obj->id);
            }
        }

        $this->render('clone', [$this->_OBJ => $obj]);

    }//end actionClone()


    public function actionAdd()
    {
        $this->breadcrumbs[] = [
            'label' => 'Add ' . $this->_NAME,
            'url' => Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/add/",
        ];
        $c = $this->_CLASS;
        $obj = new $c;

        if (!$this->pullForm($obj, $this->_OBJ, 'add')) {
            $this->render('add', [$this->_OBJ => $obj]);
        } else {
            // we processed it ok
            $obj->client = Yii::app()->params['clientid'];
            if (!$obj->save(false)) {
                // false=don't re-validate
                $this->render('add', [$this->_OBJ => $obj]);
            } else if ($this->callback == null) {
                Yii::app()->user->setFlash('success', $this->_NAME . ' added successfully');
                header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            } else {
                Yii::app()->user->setFlash('success', $this->_NAME . ' added successfully');
                $c = $this->callback;
                $this->$c($obj, "added");
            }
        }

    }//end actionAdd()


    /**
     * @param  $obj      object being valdiated
     * @param  $form     post key
     * @param  $scenario
     * @return bool whether or not validation was successful
     * Grab form from post and pass it to validator
     */
    protected function pullForm(&$obj, $form, $scenario)
    {
        if (!isset($_POST[$form])) {
            return false;
        }

        $obj->setScenario($scenario);
        $obj->attributes = $_POST[$form];

        return $obj->validate();

    }//end pullForm()


    public function actionDelete()
    {

        $id = (int)$_GET['id'];
        $c = $this->_CLASS;
        $cid = Yii::app()->params['clientid'];
        $obj = $c::model()->findByAttributes(['id' => $id, 'client' => $cid]);

        $this->breadcrumbs[] = [
            'label' => 'Delete ' . $this->_NAME,
            'url' => Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/deletee/" . $id ,
        ];

        //
        if ($obj == null) {
            if ($this->callback == null) {
                Yii::app()->user->setFlash('error', $this->_NAME . ' Invalid ');
                header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            } else {
                $c = $this->callback;
                $this->$c($obj, "invalid");
            }
            return;
        }

        if (isset($_POST[$this->_OBJ])) {
            $obj->delete();
            if ($this->callback == null) {
                Yii::app()->user->setFlash('success', $this->_NAME . ' deleted successfully');
                header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            } else {
                $c = $this->callback;
                $this->$c($obj, "deleted");
            }
        }

        $this->render('delete', [$this->_OBJ => $obj]);

    }//end actionDelete()


    public function actionEdit()
    {
        $c = $this->_CLASS;
        $id = (int)$_GET['id'];
        $cid = Yii::app()->params['clientid'];

        // Make sure the object exists.
        $obj = $c::model()->findByAttributes(['id' => $id, 'client' => $cid]);
        if ($obj == null) {
            $this->actionNotFound();
            return;
        }

        $this->breadcrumbs[] = [
            'label' => 'Edit ' . __E($obj->name),
            'url' => Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/edit/" . $id,
        ];

        // Check if the form was submitted.
        if (!$this->pullForm($obj, $this->_OBJ, 'edit')) {
            $this->render('edit', [$this->_OBJ => $obj]);
        } else {
            // we processed it ok
            $obj->update();
            if ($this->callback == null) {
                Yii::app()->user->setFlash('success', $this->_NAME . ' edited successfully');
                header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            } else {
                $c = $this->callback;
                $this->$c($obj, "edited");
            }
        }

    }//end actionEdit()


    protected function actionNotFound()
    {
        if ($this->callback == null) {
            Yii::app()->user->setFlash('success', $this->_NAME . ' not found');
            header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
        } else {
            $c = $this->callback;
            $this->$c($obj, "invalid");
        }

        exit;

    }//end actionNotFound()


    public function actionIndex()
    {
        ;
        if (!$this->ajaxIndex) {
            $c = $this->_CLASS;
            $cid = Yii::app()->params['clientid'];
            $obj = $c::model()->findAllByAttributes(['client' => $cid]);
            $this->render('index', [$this->_OBJ . "s" => $obj]);
        } else {
            $this->render('index', [$this->_OBJ . "s" => []]);
        }

    }//end actionIndex()


    protected function pullDTData($searchable = [], $sortable = [],
                                  $vars = [
                                      'offset' => 0,
                                      'limit' => 10,
                                      'order' => '',
                                  ]
        // defaults
    )
    {

        $vars['condition'] = 'client=' . Yii::app()->params['clientid'];

        $criteria = new CDbCriteria($vars);

        if (!$this->ajaxIndex) {
            return false;
        }

        if (isset($_GET['start']) && isset($_GET['length'])) {
            $s = intval($_GET['start']);
            $l = intval($_GET['length']);
            if ($l >= 0 && $s >= 0) {
                $criteria->offset = $s;
                $criteria->limit = $l;
            }
        }

        if (isset($_GET['order']) && is_array($_GET['order'])) {
            $sort = [];
            for ($i = 0; $i < intval($_GET['order']); $i++) {
                $col = intval($_GET['order'][$i]['column']);
                if (array_key_exists($col, $sortable)) {
                    $sort[] = $sortable[$col] . ' ' . ($_GET['order'][$i]['dir'] == 'asc' ? 'asc' : 'desc');
                }
            }

            if (!empty($sort)) {
                $criteria->order = implode(', ', $sort);
            }
        }

        if (!empty($_GET['search']) && !empty($_GET['search']['value'])) {
            foreach ($searchable as $k => $v) {
                $criteria->addSearchCondition($v, $_GET['search']['value']);
            }
        }

        /*
            for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if (array_key_exists($i, $searchable)&&isset($_GET['sSearch_'.$i])) {
                        $v=$searchable[$i];
                        $criteira->addSearchCondition($v, $_GET['sSearch_'.$i]);
                    }
                }
        */
        return $criteria;

    }//end pullDTData()


    public function accessRules()
    {
        return [
            [
                'allow', 'actions' => [
                'index',
                'ajaxuser',
                'edit',
                'add',
                'delete',
            ], 'users' => ['*']
            ],
            [
                'deny', 'users' => ['*']
            ],
        ];

    }//end accessRules()


    /*
        public function actionDelete()
        {
            $id=(int)$_GET['id'];
            $user=User::model()->findByPk($id);
            if($user==null){
                header("Location:".Yii::app()->params['baseurl']."/settings/users/?message=invalid");
                return;
            }
            if(isset($_POST['user'])){
                if($user->hasScores()){
                    if(isset($_POST['user']['scores'])){
                        //delete scores
                    }
                }
                $user->delete();
                header("Location:".Yii::app()->params['baseurl']."/settings/users/?message=deleted");
            }

            $this->render('delete', array("user"=>$user));
        }
    */

}//end class
