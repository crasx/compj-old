<?php

class WebUser extends CWebUser
{

    // Store model to not repeat query.
    final public const MAXLFA = 3;

    public $isgod = false;

    private $_model;

    private $uid;

    private bool $addFailF = false;

    private bool $addSuccessF = false;

    private $failedID = null;

    // max login fails allowed
    private $_permissions;


    public function getUser()
    {
        return $this->_model;

    }//end getUser()

    public function getClients()
    {
        $query = Yii::app()->db->createCommand('
        select distinct  e.client from user_events ue
         left join events e on e.id=ue.event
         where ue.user=:user');
        $query->bindParam(":user", $this->uid, PDO::PARAM_INT);
        $result = $query->queryColumn();
        return Clients::model()->findAllByPk($result);

    }


    function hasRole($roles, $event=-1)
    {

        if (!is_array($roles)) {
            return $this->hasRole([$roles]);
        }

        if (empty($this->getUser())) {
            return false;
        }

        if ($this->getUID() == Yii::app()->params['owner']) {
            if (empty($_COOKIE['ignore_owner'])) {
                Yii::app()->user->setFlash('warning-owner', 'Operating in owner mode - all permissions overridden for you');
            }

            if ((empty(Yii::app()->config->get('timezone')) || empty(Yii::app()->config->get('name'))) && !empty(Yii::app()->params['baseurl'])) {
                Yii::app()->user->setFlash('warning-owner-tz', 'Your competition set does not have a name or timezone set. Please set it <a href="'.Yii::app()->params['baseurl'].'/settings/configure">here</a>');
            }

            return true;
        }

        if ($this->isGod()) {
            if (empty($_COOKIE['ignore_god'])) {
                Yii::app()->user->setFlash('warning-god', 'Operating in godmode - all permissions overridden ');
            }

            return true;
        }

        if (!isset($this->_permissions)) {
            $command = Yii::app()->db->createCommand(
                'SELECT ue.permission
            from user_events ue
            where user =:uid
            and event in(select id from events where client=:client '.($event != -1 ? "and event =:event" : "").')'
            );
            $client  = Yii::app()->params['clientid'];
            $command->bindParam(":client", $client, PDO::PARAM_INT);
            if ($event != -1) {
                $command->bindParam(":event", $event, PDO::PARAM_INT);
            }

            $uid = $this->getUID();
            $command->bindParam(":uid", $uid, PDO::PARAM_INT);
            $this->_permissions = $command->queryColumn();
            if (sizeof($this->_permissions)) {
                $this->_permissions[] = '*';
            }
        }

        if (empty($roles) || empty($this->_permissions)) {
            return false;
        }

        if (in_array('*', $roles)) {
            return true;
        }

        if (in_array('@', $roles)) {
            return true;
        }

        $a = array_intersect($roles, $this->_permissions);
        return !empty($a);
        // if($role=='*') return !empty($this->_permissions);
        // return in_array($role, $this->_permissions);

    }//end hasRole()


    public function getUID()
    {
        if (!$this->_model) {
            return 0;
        }

        return $this->_model->id;

    }//end getUID()


    public function getName()
    {
        if (!$this->_model) {
            return '';
        }

        return __E($this->_model->firstName).' '.__E($this->_model->lastName);

    }//end getName()


    public function getFirstName()
    {
        return __E($this->_model->firstName);

    }//end getFirstName()


    public function getLastName()
    {
        return __E($this->_model->lastName);

    }//end getLastName()


    // Load user model.
    public function cInit()
    {
        $this->loadUser($this->id);
        // var_dump($this); die();
        // $this->fixPermissions();

    }//end cInit()


    protected function loadUser($id=null)
    {
        if ($this->_model === null) {
            if ($id !== null) {
                $this->_model = User::model()->findByAttributes(['email' => $this->id]);
                if ($this->_model != null) {
                    $this->uid   = $this->_model->id;
                    $this->isgod = $this->uid == 1;
                }
            }
        }

        return $this->_model;

    }//end loadUser()


    public function updateLastLogin()
    {
        $this->_model->lastLogin = new CDbExpression("current_timestamp");
        $this->_model->update();

    }//end updateLastLogin()


    private function getLoginAttempt($result=false, $uid=null)
    {
        $f = VisitorLoginAttempts::model()->find("visit=:visit and result=:r", [":visit" => $this->visitId, ":r" => $result]);
        if ($f == null) {
            $f        = new VisitorLoginAttempts();
            $f->visit = $this->visitId;
            $f->user  = $uid;
            $f->firstAttempt = new CDbExpression("NOW()");
            $f->lastAttempt  = new CDbExpression("NOW()");
            $f->times        = 0;
            $f->result       = $result;
            $f->save();
        }

        if ($uid != null) {
            $f->user = $uid;
            $f->save();
        }

        return $f;

    }//end getLoginAttempt()


    function isGod()
    {
        return $this->isgod;

    }//end isGod()


    function isOwner()
    {
        return $this->getUID() == Yii::app()->params['owner'];

    }//end isOwner()


    function isOwnerOrGod()
    {
        return $this->isGod() || $this->isOwner();

    }//end isOwnerOrGod()


}//end class
