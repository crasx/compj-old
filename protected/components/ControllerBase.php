<?php

/**
 * ControllerBase is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ControllerBase extends CController
{

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = [];

    /**
     * Returns the filters used by this controller.
     *
     * @return array An array of filters.
     */
    public function filters()
    {
        return ['access'];

    }//end filters()

    /**
     * This method is used to filter the access to the controller's actions.
     * It checks the action rules and if the user has the required role to access the action.
     * If the user does not have access, a HTTP exception is thrown.
     *
     * @param CFilterChain $c The filter chain.
     * @throws CHttpException If the user does not have access.
     */
    public function filterAccess($c)
    {

        foreach ($c->controller->_actionRules() as $rule) {
            if (isset($rule['actions']) && in_array($c->action->id, $rule['actions'])) {
                if (empty($rule['roles']) || Yii::app()->user->hasRole($rule['roles'])) {
                    $c->run();
                    return;
                }
            }
        }

        // always allow error
        if ($c->controller->getId() == 'site' && $c->action->getId() == 'error') {
            $c->run();
            return;
        }

        throw new CHttpException(403, 'You do not have access to that.');

    }//end filterAccess()

    /**
     * Returns the action rules for this controller.
     *
     * @return array An array of action rules.
     */
    public function _actionRules()
    {
        return [];

    }//end _actionRules()

    /**
     * This method is executed before an action is run.
     * It registers the necessary scripts and styles, and adds the application name to the breadcrumbs.
     *
     * @param CAction $action The action to be run.
     * @return bool Whether the action should be run.
     */
    public function beforeAction($action)
    {

        array_push($this->breadcrumbs, [
            'label' => '<i class="fa-solid fa-house"></i> ',
            'url' => '/',
        ],
        [
            'label' => __E((string)Yii::app()->config->get('name')),
            'url' => Yii::app()->params['baseurl'] . '/',
        ]
        );
        Yii::app()->clientScript->registerScriptFile('/js/dist/all.min.js');

        // default css
        Yii::app()->clientScript->registerCssFile('/css/dist/main.css');
        Yii::app()->clientScript->registerCssFile('https://cdn.datatables.net/v/dt/dt-1.13.10/datatables.min.css');
        Yii::app()->clientScript->registerScriptFile('https://cdn.datatables.net/v/dt/dt-1.13.10/datatables.min.js');
//<link href="https://cdn.datatables.net/v/dt/dt-1.13.10/datatables.min.css" rel="stylesheet">
//
//<script src="https://cdn.datatables.net/v/dt/dt-1.13.10/datatables.min.js"></script>

        Yii::app()->clientScript->registerScript("defComp", "BASE='" . Yii::app()->params['baseurl'] . "';", CClientScript::POS_HEAD);
        return parent::beforeAction($action);

    }//end beforeAction()

    /**
     * This method is executed after an action is run.
     *
     * @param CAction $a The action that was run.
     */
    public function afterAction($a)
    {

        parent::afterAction($a);

    }//end afterAction()

    /**
     * Returns the access rules for this controller.
     *
     * @return array An array of access rules.
     */
    public function accessRules()
    {
        return [
            [
                'allow', 'actions' => [
                'error',
                'login',
            ], 'users' => ['*']
            ],
            [
                'allow', 'actions' => [
                'logout',
                'index',
            ], 'users' => ['@']
            ],
            [
                'deny', 'users' => ['*']
            ],
        ];

    }//end accessRules()

    /**
     * Renders a view with the specified data.
     *
     * @param string $view The view to be rendered.
     * @param array|null $data The data to be extracted into PHP variables and made available to the view script.
     * @param bool $return Whether the rendering result should be returned instead of being displayed to end users.
     * @return string The rendering result. Null if the rendering result is not required to be returned.
     */
    public function render($view, $data = null, $return = false)
    {
        $d1 = ["baseurl" => Yii::app()->params['base']];
        $data = is_array($data) ? array_merge($data, $d1) : $d1;
        return parent::render($view, $data, $return);

    }//end render()

    /**
     * Returns an instance of the ReCaptcha class.
     *
     * @return \ReCaptcha\ReCaptcha An instance of the ReCaptcha class.
     */
    function recaptcha()
    {

        static $recaptcha = false;

        if ($recaptcha) {
            return $recaptcha;
        }

        Yii::import('ext.ReCaptcha.ReCaptcha', true);
        Yii::import('ext.ReCaptcha.RequestMethod', true);
        Yii::import('ext.ReCaptcha.RequestParameters', true);
        Yii::import('ext.ReCaptcha.Response', true);
        Yii::import('ext.ReCaptcha.RequestMethod.Post', true);
        Yii::import('ext.ReCaptcha.RequestMethod.Socket', true);
        Yii::import('ext.ReCaptcha.RequestMethod.SocketPost', true);

        // Add js.
        Yii::app()->clientScript->registerScriptFile('https://www.google.com/recaptcha/api.js', CClientScript::POS_END);
        $recaptcha = new \ReCaptcha\ReCaptcha(Yii::app()->params['recaptcha_private']);
        return $recaptcha;
    }

    /**
     * Registers the necessary scripts and styles for the fancybox plugin.
     */
    public function fancybox()
    {

        Yii::app()->clientScript->registerScriptFile("/js/theme/fancybox/jquery.fancybox.pack.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile('/js/theme/fancybox/jquery.fancybox.css');

    }//end fancybox()

    /**
     * Returns a human-readable error message for a file upload error.
     *
     * @param int $error The error code.
     * @return string|false The error message, or false if there was no error.
     */
    protected function fileErrorText($error)
    {
        $response = match ($error) {
            UPLOAD_ERR_OK => false,
            UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
            UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
            UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
            UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
            UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.',
            UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
            UPLOAD_ERR_EXTENSION => 'File upload stopped by extension. Introduced in PHP 5.2.0.',
            default => 'Unknown error',
        };

        return $response;

    }//end fileErrorText()


}//end class
