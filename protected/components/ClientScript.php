<?php

class ClientScript extends CClientScript
{


    public function isScriptFileRegistered($url, $position=self::POS_HEAD)
    {
        return in_array($url, $this->scriptFiles[$position]);

    }//end isScriptFileRegistered()


    public function isCssFileRegistered($url)
    {
        return in_array($url, $this->cssFiles);

    }//end isCssFileRegistered()


    public function registerScriptFile($url, $position=self::POS_HEAD, $place=0)
    {
        $this->hasScripts = true;
        $place            = (int) $place;
        while (isset($this->scriptFiles[$position][$place])) {
            $place++;
        }

        $this->scriptFiles[$position][$place] = $url;
        $params = func_get_args();
        $this->recordCachingAction('clientScript', 'registerScriptFile', $params);
        return $this;

    }//end registerScriptFile()


}//end class
