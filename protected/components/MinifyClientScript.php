<?php

/*
 * Copyright (c) 2012, "Klaas Sangers"<klaas@webkernel.nl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * MinifyClientScript class file.
 */
Yii::import('ext.wbkrnl.CssCompressor');
Yii::import('ext.jsmin.JSMin');

/**
 * MinifyClientScript manages JavaScript and CSS stylesheets for views.
 */
class MinifyClientScript extends CClientScript
{

    /**
     * Path to the 'original' CSS
     */
    private string|array|null $relativeCssPath = null;

    /**
     * Path to the minify runtime files
     *
     * @var string
     */
    private $minifyPath;

    /**
     * The POS_* constants that this class will parse
     */
    private ?array $minifyJsPositions = null;


    /**
     * Prepare the clientScript
     */
    public function init()
    {
        $this->minifyPath        = Yii::getPathOfAlias("application.runtime.MinifyClientScript");
        $this->minifyJsPositions = [
            self::POS_BEGIN,
            self::POS_END,
            self::POS_HEAD,
            self::POS_LOAD,
            self::POS_READY,
        ];
        parent::init();

    }//end init()


    /**
     * Callback method for preg_replace_callback in renderHead()
     *
     * @return array
     */
    protected function cssReplaceCallback(array $matches)
    {
        if (str_starts_with((string) $matches[1], "/")) {
            return $matches[0];
        } else if (str_starts_with((string) $matches[1], "http")) {
            return $matches[0];
        } else {
            return "url($this->relativeCssPath/$matches[1])";
        }

    }//end cssReplaceCallback()


    /**
     * Minify the css for optimal client-side performance
     */
    protected function minifyCss()
    {
        $cssArray = [];
        // remember the original pcre backtrace limit, we're going to put it back to default
        $pcreBacktrackLimitOriginal = ini_get('pcre.backtrack_limit');
        // determine the filenames
        $filenames = [];
        foreach ($this->cssFiles as $url => $media) {
            if (!isset($filenames[empty($media) ? 'default' : $media])) {
                $filenames[empty($media) ? 'default' : $media] = $url;
            } else {
                $filenames[empty($media) ? 'default' : $media] .= $url;
            }
        }

        foreach ($filenames as $media => $filename) {
            $filenames[empty($media) ? 'default' : $media] = preg_replace("/[^a-zA-Z0-9]+/", "-", $media)."-".hash('sha256', (string) $filename).".css";
        }

        // check whether the files need to be regenerated
        $compileCss = [];
        foreach ($filenames as $media => $filename) {
            $compileCss[empty($media) ? 'default' : $media] = (isset($compileCss[empty($media) ? 'default' : $media]) && $compileCss[empty($media) ? 'default' : $media]) || YII_DEBUG || !file_exists($this->minifyPath.DIRECTORY_SEPARATOR.$filename);
        }

        // append css files to the minified css store
        foreach ($this->cssFiles as $url => $media) {
            // skip if it's an external css
            if (str_starts_with((string) $url, "http") || str_starts_with((string) $url, "//")) {
                continue;
            }

            // make sure $media is defined
            if (empty($media)) {
                $media = "default";
            }

            $path = $this->getRealPath($url);
            // check if the file can be read
            if (!is_readable($path)) {
                throw new CException("CSS file '".$url."' is not readable {$path}.");
            }

            unset($this->cssFiles[$url]);
            if (isset($compileCss[$media]) && !$compileCss[$media]) {
                continue;
            }

            // initialize the css per media if necessary
            if (!isset($cssArray[$media])) {
                $cssArray[$media] = "";
            }

            $this->relativeCssPath = pathinfo((string) $url, PATHINFO_DIRNAME);

            $css = trim(file_get_contents($path));
            // don't parse this file if it's empty
            if (empty($css)) {
                continue;
            }

            $cssLength = strlen($css);
            // when the css is bigger than the original pcre backtrace limit increase the limit
            if ($pcreBacktrackLimitOriginal < $cssLength) {
                ini_set('pcre.backtrack_limit', $cssLength);
            }

            // replace url()'s in the css
            if (preg_match("#url\s*\(\s*['\"]?([^'\"\)]+)['\"]?\)#", $css) > 0) {
                $css = preg_replace_callback("#url\s*\(\s*['\"]?([^'\"\)]+)['\"]?\)#", $this->cssReplaceCallback(...), $css);
            }

            // decrease the pcre backtrace limit if required
            if ($pcreBacktrackLimitOriginal < $cssLength) {
                ini_set('pcre.backtrack_limit', $pcreBacktrackLimitOriginal);
            }

            // append the non-minified css
            $cssArray[$media] .= $css;
        }//end foreach

        foreach ($filenames as $media => $filename) {
            if ($compileCss[$media]) {
                file_put_contents("$this->minifyPath/$filenames[$media]", CssCompressor::deflate($cssArray[$media]), LOCK_EX);
            }

            $this->registerLinkTag("stylesheet", "text/css", Yii::app()->assetManager->getPublishedUrl($this->minifyPath)."/$filenames[$media]", $media == "default" ? null : $media);
        }

    }//end minifyCss()


    protected function minifyScriptFileByPosition($position)
    {
        $js    = "";
        $css   = '';
        $files = [];
        $jsMt  = 0;

        // generate filename for other scripts
        if (isset($this->scriptFiles[$position])) {
            foreach ($this->scriptFiles[$position] as $key => $scriptFile) {
                // ignore nonlocal files
                if (!str_starts_with((string) $scriptFile, "://") || str_starts_with((string) $scriptFile, "//")) {
                    continue;
                }

                $f = $this->getRealPath($scriptFile);
                // exclude or update modtime
                if (!file_exists($f)||!is_readable($f)) {
                     continue;
                    // NOTE KS - throw new CException("CSS file '" . Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . substr($scriptFile, strlen(Yii::app()->homeUrl)) . "' is not readable."); ?
                }

                $j = trim(file_get_contents($f));
                if (empty($j)) {
                    continue;
                }

                $js     .= $j;
                $jsMt    = max($jsMt, filemtime($f));
                $files[] = $f;
                unset($this->scriptFiles[$position][$key]);
            }//end foreach
        }//end if

        if (empty($files)) {
            return;
        }

        sort($files);
        // sort them to prevent unnessecary compression
        $filename = hash('sha256', serialize($files));

        // determine if the JS still needs to be 'compiled'
        $fullFilename = $this->minifyPath.DIRECTORY_SEPARATOR."$filename.js";

        $compileJs = !file_exists($fullFilename)||$jsMt > filemtime($fullFilename);

        if ($compileJs) {
            $js = JSMin::minify($js);
            if (file_exists($fullFilename)) {
                unlink($fullFilename);
            }

            file_put_contents($fullFilename, $js, LOCK_EX);
            touch($fullFilename, $jsMt);
        }

        $publishedMinifyPath = Yii::app()->assetManager->getPublishedUrl($this->minifyPath);
        $this->registerScriptFile($publishedMinifyPath."/$filename.js", $position);

    }//end minifyScriptFileByPosition()


    public function registerCoreScriptFile($script)
    {
        if (!isset($this->coreScripts)) {
            $this->coreScripts = [];
        }

        $this->coreScripts[] = $script;

    }//end registerCoreScriptFile()


    protected function minifyCoreJs()
    {
        $js    = "";
        $css   = '';
        $files = [];
        $jsMt  = 0;
        return;
        // generate filename for other scripts
        if (isset($this->coreScripts)) {
            foreach ($this->coreScripts as $key => $scriptFile) {
                // ignore nonlocal files
                if (!str_starts_with((string) $scriptFile, "://") || str_starts_with((string) $scriptFile, "//")) {
                    continue;
                }

                $f = $this->getRealPath($scriptFile);
                // exclude or update modtime
                if (!file_exists($f)||!is_readable($f)) {
                     continue;
                    // NOTE KS - throw new CException("CSS file '" . Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . substr($scriptFile, strlen(Yii::app()->homeUrl)) . "' is not readable."); ?
                }

                $j = trim(file_get_contents($f));
                if (empty($j)) {
                    continue;
                }

                $js     .= $j;
                $jsMt    = max($jsMt, filemtime($f));
                $files[] = $f;
            }//end foreach
        }//end if

        $this->coreScripts = null;

        if (empty($files)) {
            return;
        }

        sort($files);
        // sort them to prevent unnessecary compression
        $filename = 'core-'.hash('sha256', serialize($files));

        // determine if the JS still needs to be 'compiled'
        $fullFilename = $this->minifyPath.DIRECTORY_SEPARATOR."$filename.js";

        $compileJs = !file_exists($fullFilename)||$jsMt > filemtime($fullFilename);

        if ($compileJs) {
            $js = JSMin::minify($js);
            if (file_exists($fullFilename)) {
                unlink($fullFilename);
            }

            file_put_contents($fullFilename, $js, LOCK_EX);
            touch($fullFilename, $jsMt);
        }

        $publishedMinifyPath = Yii::app()->assetManager->getPublishedUrl($this->minifyPath);
        $this->registerScriptFile($publishedMinifyPath."/$filename.js", $position);

    }//end minifyCoreJs()


    public function renderCoreScripts()
    {
        $this->minifyCoreJs();

    }//end renderCoreScripts()


    /**
     * Inserts the scripts in the head section.
     *
     * @param string $output the output to be inserted with scripts.
     */
    public function renderHead(&$output)
    {
        if (!is_dir($this->minifyPath)) {
            mkdir($this->minifyPath, 0775, true);
        }

        $this->minifyCss();
        if ($this->enableJavaScript) {
            foreach ($this->minifyJsPositions as $position) {
                $this->minifyScriptFileByPosition($position);
            }
        } else {
            $this->scriptFiles = $this->scripts = null;
        }

        // force rewriting of assets when we're debugging, otherwise be lazy
        Yii::app()->assetManager->publish($this->minifyPath);
        parent::renderHead($output);

    }//end renderHead()


    /**
     *
     * @param string $id
     * @param string $css
     * @param string $media
     */
    public function registerCss($id, $css, $media='')
    {
        return parent::registerCss($id, CssCompressor::deflate($css), $media);

    }//end registerCss()


    /**
     *
     * @param string $id
     * @param string $script
     * @param int    $position
     */
    public function registerScript($id, $script, $position=self::POS_READY)
    {
        return parent::registerScript($id, JSMin::minify($script), $position);

    }//end registerScript()


    /**
     * @param string $path
     * @param string $media
     * @param string $defaultAssetUrl
     */
    public function registerCssFile($path, $media='')
    {
        $parsedPath = $this->parseAssetsPath($path);
        if ($parsedPath) {
            return parent::registerCssFile($this->getFileUrl($parsedPath['assetsUrl'], $parsedPath['path']), ($media ?? ''));
        } else {
            return parent::registerCssFile($path, $media);
        }

    }//end registerCssFile()


    /**
     * @param string $path
     * @param int    $position
     * @param string $defaultAssetUrl
     */
    public function registerScriptFile($path, $position=0)
    {
        $parsedPath = $this->parseAssetsPath($path);
        if ($parsedPath) {
            return parent::registerScriptFile($this->getFileUrl($parsedPath['assetsUrl'], $parsedPath['path']), $position);
        } else {
            return parent::registerScriptFile($path, $position);
        }

    }//end registerScriptFile()


    /**
     * @param  string $path
     * @return array
     */
    private function parseAssetsPath($path)
    {
        if (Yii::app()->theme !== null && str_contains($path, (string) Yii::app()->theme->baseUrl)) {
            // it's in the themes
            $baseUrl = Yii::app()->theme->baseUrl;
        } else if (str_contains($path, (string) Yii::app()->assetManager->baseUrl)) {
            // it's in the assets
            $baseUrl = Yii::app()->assetManager->baseUrl;
        } else {
            // could not be parsed
            return false;
        }

        $truncatedPath = substr($path, (strlen((string) $baseUrl) + 1));
        $splitted      = explode("/", $truncatedPath);
        return [
            'assetsUrl' => $baseUrl.'/'.$splitted[0],
            'path'      => substr($truncatedPath, strlen($splitted[0])),
        ];

    }//end parseAssetsPath()


    /**
     * Check if the theme folder contains the same file,
     * if so load that file instead of the default file
     *
     * @param  string $defaultAssetUrl
     * @param  string $path
     * @return string
     */
    private function getFileUrl($defaultAssetUrl, $path)
    {
        if (Yii::app()->theme !== null
            && file_exists(Yii::app()->theme->getBasePath().DIRECTORY_SEPARATOR.'assets'.$path)
        ) {
            $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::app()->theme->getBasePath().DIRECTORY_SEPARATOR.'assets');
            return $assetsUrl.$path;
        }

        return $defaultAssetUrl.$path;

    }//end getFileUrl()


    private function getRealPath($url)
    {
            $absolute = strlen((string) $url)&&str_starts_with((string) $url, '/');
            return $absolute ? Yii::getPathOfAlias('webroot').$url : Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$url;

    }//end getRealPath()


}//end class
