<?php

/**
 * ApplicationConfigBehavior is a behavior for the application.
 * It loads additional config parameters that cannot be statically
 * written in config/main
 */
class CJBehavior extends CBehavior
{


    /**
     * Declares events and the event handler methods
     * See yii documentation on behavior
     */
    public function events()
    {
        return array_merge(parent::events(), ['onBeginRequest' => 'beginRequest']);

    }//end events()


    /**
     * Load configuration that cannot be put in config/main
     */
    public function beginRequest()
    {

        // check against reserved
        //
        // login////////
        //
        if (!Yii::app()->user->isGuest && isset($_GET['logout'])) {
            Yii::app()->user->logout(false);
            header("location: /");
        } else if (!Yii::app()->user->isGuest) {
            Yii::app()->user->cInit();
        }

        if (isset($_GET['client']) && !in_array($_GET['client'], Yii::app()->params['reserved'])) {
            if ($_GET['client'] == 'gii' && YII_DEBUG) {
                return;
            }

            $client = Clients::model()->with("License")->findByAttributes(["path" => $_GET['client']]);
            if (!$client) {
                throw new CHttpException(404, 'The specified competition cannot be found.');
            }

            Yii::app()->params['baseurl']  = '/'.$client->path;
            Yii::app()->params['clientid'] = $client->id;
            Yii::app()->params['owner']    = $client->owner;
            Yii::app()->params['Client']   = $client;
            if ($client->License) {
                $data = json_decode((string) $client->License->data, null, 512, 0);
                Yii::app()->params['license']     = $data;
                Yii::app()->params['licensename'] = $client->License->name;
            } else {
                Yii::app()->params['license']     = false;
                Yii::app()->params['licensename'] = '';
            }

            $tz = Yii::app()->config->get("timezone");
            if (!empty($tz)) {
                date_default_timezone_set($tz);
            }

            if (!Yii::app()->user->hasRole("*") && !Yii::app()->user->isgod) {
                // Make sure they have access
                throw new CHttpException(403, 'You do not have access to that.');
            }
        }//end if

        // license
        /*
            $l=json_decode(LICENSE, 1);
                if($l)Yii::app()->params['license']=$l;
                else Yii::app()->params['license']=array();
        */

    }//end beginRequest()


}//end class
