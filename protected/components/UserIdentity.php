<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    final public const ERROR_CAPTCHA_INVALID = 3;

    private $_user;

    /**
     * Authenticates a user. this will be expanded as site progresses to pull user details
     *
     * @return boolean whether authentication succeeds.
     */


    public function authenticate($record=null)
    {

        /*
            if(Yii::app()->user->requireCaptcha()){
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
                    $securimage = new Securimage();
                    if(!$securimage->check($_POST['loginForm']['captcha'])){
                        $this->errorCode=self::ERROR_CAPTCHA_INVALID;
                        if(isset($_POST['loginForm']['captcha']))
                            Yii::app()->session['loginErrorMessage']="Invalid CAPTCHA";
                        else
                            Yii::app()->session['loginErrorMessage']="Please enter CAPTCHA";
                        return;
                    }

                }*/

        $record = User::model()->findByAttributes(['email' => $this->username]);
        if ($record === null) {
            Yii::app()->user->setFlash('e_login', "Invalid login");
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (strcmp((string) $record->password, (string) $record->applyHash($this->password)) != 0) {
            Yii::app()->user->setFlash('e_login', "Invalid login");
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_user        = $record;
            $this->errorCode    = self::ERROR_NONE;
            $this->_user->isgod = $this->_user->id == 1;
        }

        $command = Yii::app()->db->createCommand('insert into login_attempts(ip, session, time, username, error) values(:ip, :session, now(), :user, :success)');

        $ip  = ip2long($_SERVER['REMOTE_ADDR']);
        $sid = session_id();
        $command->bindParam(":ip", $ip, PDO::PARAM_INT);
        $command->bindParam(":session", $sid, PDO::PARAM_STR);
        $command->bindParam(":user", $this->username, PDO::PARAM_STR);
        $command->bindParam(":success", $this->errorCode, PDO::PARAM_INT);
        $command->query();
        return !$this->errorCode;

    }//end authenticate()


    public function getUser()
    {
        return $this->_user;

    }//end getUser()


}//end class
