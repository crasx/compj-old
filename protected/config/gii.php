<?php
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
    'basePath'   => __DIR__.DIRECTORY_SEPARATOR.'..',
    'name'       => 'Competition Judge',
    // autoloading model and component classes
    'import'     => [
        'application.models.*',
        'application.components.*',
    ],
    'modules'    => [
        // uncomment the following to enable the Gii tool
        'gii' => [
            'class'     => 'system.gii.GiiModule',
            'password'  => 'cjdev',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => ['*'],
        ],
    ],
    // application components
    'components' => [
        'urlManager' => [
            'urlFormat' => 'path',
            'rules'     => [
                'gii'                               => 'gii',
                'gii/<controller:\w+>'              => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
            ],
        ],
        /*
            'db'=>array(
                    'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
                ),*/
        'db'         => [
            'connectionString' => 'pgsql:host=localhost;port=5432;dbname=uncrash_cjdev',
            // 'emulatePrepare' => true,
            'username'         => 'uncrash_cj',
            'password'         => 'Fy(v2i}dhD?T',
        ],
    ],
];
