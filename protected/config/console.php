<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return [
    'basePath'   => __DIR__.DIRECTORY_SEPARATOR.'..',
    'name'       => 'My Console Application',
    // application components
    // autoloading model and component classes
    'import'     => [
        'application.models.*',
        'application.components.*',
    ],
    'components' => [
        'authManager' => [
            'class'           => 'CDbAuthManager',
            'itemTable'       => 'dev_AuthItem',
            'itemChildTable'  => 'dev_AuthItemChild',
            'assignmentTable' => 'dev_AuthAssignment',
        ],
        'db'          => [
            'connectionString' => 'pgsql:host=127.0.0.1;port=5432;dbname=uncrash_cjdev',
        // 'emulatePrepare' => true,
            'username'         => 'uncrash_cj',
            'password'         => 'Fy(v2i}dhD?T',
        ],
    ],
    'params'     => [
        'base'        => $uri,
        'tablePrefix' => 'dev_',
        'permissions' => include __DIR__.DIRECTORY_SEPARATOR.'permissions.php',
    ],
];
