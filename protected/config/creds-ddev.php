<?php

return [
    'db'    => [
        'connectionString' => 'mysql:host=db;dbname=db',
// 'emulatePrepare' => true,
        'username'         => 'db',
        'password'         => 'db',
// 'charset' => 'utf8',
    ],
    'cache' => [
        'class'    => 'CRedisCache',
        'hostname' => 'redis',
        'port'     => 6379,
        'database' => 0,
        'timeout'  => 10,
    ],
];
