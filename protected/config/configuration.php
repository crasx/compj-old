<?php
return [
    "name"      => ["multiple" => false],
    "precision" => [
        "multiple"      => false,
        'specialCheck'  => 'validateInt',
        'validateError' => 'Precision should be an integer',
    ],
    "flickr_id" => [
        "multiple"      => true,
        'specialCheck'  => 'validateFlickr',
        'validateError' => 'Invalid flickr ID',
    ],
    "timezone"  => [
        "multiple"      => false,
        'specialCheck'  => 'validateTimezone',
        'validateError' => 'Invalid timezone',
    ],
    "logo"      => [
        "multiple"  => false,
        'file'      => 1,
        'fileCheck' => 'validateLogo',
    ],
    "favicon"   => [
        "multiple"  => false,
        'file'      => 1,
        'fileCheck' => 'validateLogo',
        'ext'       => ['ico'],
    ],
];
