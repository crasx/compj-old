<?php
return [
    'db'    => [
        'connectionString' => 'mysql:host='.$_ENV['MYSQL_HOSTNAME'].':'.$_ENV['MYSQL_PORT'].';dbname='.$_ENV['MYSQL_DATABASE'],
// 'emulatePrepare' => true,
        'username'         => $_ENV['MYSQL_USERNAME'],
        'password'         => $_ENV['MYSQL_PASSWORD'],
// 'charset' => 'utf8',kee
    ],
    'cache' => [
        'class'    => 'CRedisCache',
        'hostname' => $_ENV['YII1_HELM_REDIS_SERVICE_HOST'],
        'port'     => $_ENV['YII1_HELM_REDIS_SERVICE_PORT'],
        'database' => 0,
        'timeout'  => 10,
    ],
];
