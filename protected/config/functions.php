<?php

function loadCredentials(){
    if (!empty($_ENV['MYSQL_HOSTNAME'])) {
        return include __DIR__ . '/creds-env.php';
    } else if (!empty($_ENV['DDEV_VERSION'])) {
        return include __DIR__ . '/creds-ddev.php';
    }
}

function grabLicenseData($data, $default=null){
    if(isset(Yii::app()->params['license'])&&isset(Yii::app()->params['license'][$data]))
        return Yii::app()->params['license'][$data];
    return $default;

}