<?php

require __DIR__.'/functions.php';

$creds = loadCredentials();

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
    'basePath'   => __DIR__.'/..',
    'name'       => 'Competition Judge',
    'homeUrl'    => '/',
    // preloading 'log' component
    'preload'    => ['log'],
    // autoloading model and component classes
    'import'     => [
        'application.models.*',
        'application.components.*',
        'application.components.models.*',
    ],
    'behaviors'  => ['CJBehavior'],
    'modules'    => [
        // uncomment the following to enable the Gii tool
        'gii' => YII_DEBUG ? [
            'class'     => 'system.gii.GiiModule',
            'password'  => 'cjpass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => ['127.0.0.1'],
        ] : [],
    ],
    // application components
    'components' => [
        'config'       => ['class' => 'application.extensions.EConfig'],
        'cache'        => $creds['cache'],
        'clientScript' => 1 ? [] : ['class' => 'application.components.MinifyClientScript'],
        'user'         => [
            // enable cookie-based authentication
            'class'           => 'WebUser',
            'loginUrl'        => "/user/login",
            'allowAutoLogin'  => true,
            'autoUpdateFlash' => false,
        ],
        'urlManager'   => [
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => [
                'gii'                                                                    => 'gii',
                'gii/<controller:\w+>'                                                   => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'                                      => 'gii/<controller>/<action>',
                '/'                                                                      => 'front/index',
            // see config: reserved
                'user/login'                                                                  => 'user/login',
                'user/register'                                                               => 'user/register',
                'user/restore'                                                                => 'user/restore',
                'user/forgot'                                                                 => 'user/forgot',
                'user/logout'                                                                 => 'user/logout',
                'user/profile'                                                               => 'user/profile',
                'a/create'                                                               => 'admin/create',
                'a/<action:\w+>/<id:\w+>'                                                => 'admin/<action>',
                'a/<action:\w+>'                                                         => 'admin/<action>',
                '<client:\w+>/slideshow'                                                 => 'front/slideshow',
                '<client:\w+>/slideshowFields'                                           => 'front/slideshowFields',
                '<client:\w+>/settings/<controller:\w+>'                                 => 'settings/<controller>/index',
                '<client:\w+>/settings/<controller:\w+>/<action:\w+>'                    => 'settings/<controller>/<action>',
                '<client:\w+>/settings/<controller:\w+>/<action:\w+>/<id:\w+>'           => 'settings/<controller>/<action>',
                '<client:\w+>/settings/<controller:\w+>/<action:\w+>/<id:\w+>/<id2:\w+>' => 'settings/<controller>/<action>',
                '<client:\w+>/settings/'                                                 => 'settings/configure/index',
                '<client:\w+>/<controller:\w+>'                                          => '<controller>/index',
                '<client:\w+>/<controller:\w+>/<action:\w+>'                             => '<controller>/<action>',
                '<client:\w+>/<controller:\w+>/<action:\w+>/<id:\w+>'                    => '<controller>/<action>',
                '<client:\w+>/*'                                                         => 'site/index',
            ],
        ],
        'authManager'  => [
            'class'           => 'CDbAuthManager',
            'itemTable'       => 'AuthItem',
            'itemChildTable'  => 'AuthItemChild',
            'assignmentTable' => 'AuthAssignment',
        ],
        /*
            'db'=>array(
              'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ),*/
        'db'           => $creds['db'],
//        'errorHandler' => [
//            // use 'site/error' action to display errors
//            'errorAction' => 'site/error',
//        ],
        'log'          => [
            'class'  => 'CLogRouter',
            'routes' => [
                [

                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'enabled'       => !YII_DEBUG,

                ],
                [
                    'class'         => 'CWebLogRoute',
                    'enabled'       => YII_DEBUG,
                    'showInFireBug' => YII_DEBUG,
                    'levels'        => 'trace',
                ],
            ],
        ],
    ],
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'     => [
        'permissions'       => include __DIR__.DIRECTORY_SEPARATOR.'permissions.php',
        'configuration'     => include __DIR__.DIRECTORY_SEPARATOR.'configuration.php',
        'reserved'          => [
            'a',
            'user',
            'register',
            'forgot',
            'restore',
            'logout',
            'settings',
        ],
        'upload_dir'        => realpath(__DIR__.'/../data/uploads/').'/',
        'recaptcha_public'  => '6LeMF2opAAAAAKT824-w3ZxLCXXaZat0BXZ5mY9B',
        'recaptcha_private' => '6LeMF2opAAAAALHboRXQnf0iEhiDRKYNvgpSewy-',
        'picasa_api'        => 'AIzaSyD-xYe00VIOektuwvQ33rbobclqlY97FRQ',
        'flickr_key'        => '3c9fdfbc9c74fe04fa9a4bc3a25184fd',
        'flickr_secret'     => 'b53f3ce28bdff47e',
        'name'              => 'Competition Judge',
        'url'               => 'http://'.$_SERVER['HTTP_HOST'].'/',
        'fromEmail'         => 'Competition Judge <noreply@'.$_SERVER['HTTP_HOST'].'>',
        'logo'              => '/images/logo-notext.png',
    ],
];
