<?php
return [
    'Admin'        => [
        'admin_config' => 'Configuration',
        'admin_events' => 'Events',
        'admin_events' => 'Competitions',
        '#badge'       => 'danger',
    ],
    'MC'           => [
        'mc_lineup'    => 'Lineup',
        'mc_results'   => 'Results',
        'mc_report'    => 'Report',
        'mc_breakdown' => 'Breakdown',
        'mc_slideshow' => 'Slideshow',
        '#badge'       => 'info',
    ],
    'Registration' => [
        'registration' => 'Registration',
        '#badge'       => 'primary',
    ],
    'Judge'        => [
        'judge'  => 'Judge',
        '#badge' => 'success',
    ],
];
