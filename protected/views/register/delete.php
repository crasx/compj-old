<h1>Delete Registration</h1>

<form action="?" method="post">
    <fieldset>
        <legend>Confirm delete</legend>
        <p>Are you sure you want to delete the registration for <?php echo __E($registration->name) ?>?</p>
        <p><em>This will remove all scores for this contestant and cannot be undone</em></p>
    </fieldset>
    <p><input type=hidden name="registration[confirm]" value="1"/>
        <input class="btn btn-danger" type="submit" value="Delete">
        <a class="btn btn-light" href="<?php echo Yii::app()->params['baseurl'] ?>/register">Cancel</a>
    </p>
</form>