<h1><?= $this->isClone ? 'Clone' : 'Add' ?> Registration</h1>
<?php
$this->renderPartial('form', ["registration" => $registration]);
