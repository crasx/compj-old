<?php
$this->renderPartial("//formtools");
/*
 * @var Entry $registration
 */
$errors = $registration->getErrors();

$this->renderPartial('//generate/messages');
$renderedFields = [];


$comps = $registration->getRegisterableCompetitions();
$allFields = $registration->getPossibleFields();

$competitionData = [];

foreach ($comps as $c) {
    $entry = $registration->EnteredCompetitions[$c->id] ?? NULL;
    // If this is a clone, don't clone competitions
    if ($this->isClone) {
        $entry = NULL;
    }
    $competitionData[$c->id] = [
        'cid' => $c->id,
        'cName' => $c->name,
        'event' => $c->Event->name,
        'registrationCount' => $c->EntryCount,
        'nextId' => $c->getMaxEntryId() + 1,
        'selected' => !empty($entry),
        'entryActive' => $entry?->active,
        'judgingEnabled' => $c->Event->judge,
        'visibleFields' => [],
        'requiredFields' => [],
        'entryActive' => $entry?->active,
        'entryId' => $entry?->id,
        'lineupStatus' => $entry?->here,
    ];

    foreach ($c->cRegFields as $f) {
        if (!isset($allFields[$f->field])) {
            $allFields[$f->field] = $f->Field;
        }
        $competitionData[$c->id]['visibleFields'][] = $f->field;
        if ($f->required) {
            $competitionData[$c->id]['requiredFields'][] = $f->field;
        }

    }

}//end foreach
?>

<script type="text/javascript">
    competitionData = <?php echo json_encode($competitionData, JSON_THROW_ON_ERROR) ?>;
</script>

<form action="?" method="post" id="registrationForm" enctype="multipart/form-data">
    <div class="row">

        <div class="col-lg-6 col-md-12">
            <div class="main-box clearfix">
                <header class="main-box-header clearfix">
                    <h2 class="pull-left pb-4">Available Competitions</h2>

                    <div class="filter-block pull-right">
                        <?php
                        $events = $registration->getRegisterableEvents();
                        ?>

                    </div>
                </header>

                <div class="main-box-body clearfix">
                    <?php printFormElementErrors(($errors['competitions'] ?? [])); ?>
                    <div class="table-responsive">

                        <table class="table fancy" id="competitionList">
                            <thead>
                            <tr>
                                <th>Competition</th>
                                <th>Entry Status</th>
                                <th>Entry Info</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-12">
            <div class="main-box clearfix">

                <header class="main-box-header clearfix">
                    <h2>Information</h2>
                </header>
                <div class="main-box-body clearfix">
                    <div class="form-group mb-1 registration-field-wrapper">

                        <?php
                        $this->renderPartial('//register/input/text', [
                            'field' => (object)['name' => 'Name', 'id' => 'name', 'description' => ''],
                            'errors' => $errors['name'] ?? [],
                            'val' => $registration->name,
                            'name' => 'registration[name]',
                        ]);
                        ?>
                    </div>
                </div>
                <hr/>
                <header class="main-box-header clearfix mb-4" id="additional_header">
                    <h2>Additional Fields</h2>
                    <em>As competitions are selected for this registration, new fields will show here that are specific
                        to that competition.</em>
                </header>
                <div class="main-box-body clearfix" id="additional">
                    <?php
                    foreach ($allFields as $fid => $field):
                        ?>
                        <div class="form-group mb-4 registration-field-wrapper" data-fid="<?php echo $fid ?>">

                            <?php

                            $this->renderPartial('//register/input/' . strtolower((string)$field->Type->validator), [
                                'field' => $field,
                                'errors' => $errors['fields'][$fid] ?? [],
                                'val' => ($registration?->getFieldValue($fid)  ?? ''),
                                'registration' => $registration,
                                'name' => "registration[fields][$fid]",
                            ]);
                            ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>


    <div class="main-box-body clearfix text-center">
        <br/>
        <input type="submit" class="btn btn-success" value="Submit">
        <a class="btn btn-danger" href="<?php echo Yii::app()->params['baseurl'] ?>/register">Cancel</a>
    </div>
</form>


<div class="d-none">
    <div id="tpl-competition-name">
        <div class="competition-name"></div>
        <div class="competition-event fst-italic"></div>
    </div>

    <div id="tpl-registration-checkbox">
        <div class="btn-group" role="group" aria-label="Entry Status">
            <div class="d-flex flex-column ">
                <div class="my-1 cj-available">
                    <input type="radio" class="btn-check" id="competition-none-" autocomplete="off" value="available">
                    <label class="btn btn-outline-secondary cj-available w-100" for="competition-none-" value="entered">Not
                        Entered</label>
                </div>
                <div class="my-1 cj-withdrawn">
                    <input type="radio" class="btn-check" id="competition-withdraw-" autocomplete="off"
                           value="withdrawn">
                    <label class="btn btn-outline-danger w-100" for="competition-withdraw-">Withdrawn</label>
                </div>
                <div class="my-1 cj-entered">
                    <input type="radio" class="btn-check" id="competition-enter-" autocomplete="off" value="entered">
                    <label class="btn btn-outline-success w-100" for="competition-enter-">Entered</label>
                </div>
            </div>
        </div>
    </div>

    <div id="tpl-competition-info">
        <span class="estimated">Estimated ID:</span>
        <span class="actual">ID: </span>
        <em class="registration-id"></em>
        <div class="form-check form-switch lineup">
            <input class="form-check-input" type="checkbox" role="switch">
            <label class="form-check-label">In line</label>
        </div>
    </div>

    <div class="d-none">
        <div id="flickrTableHolder">
            <table id="flickrTable" data-url="<?php echo Yii::app()->params['baseurl'] ?>/register/photos">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Uploaded</th>
                    <th>Preview</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
