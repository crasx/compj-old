<?php

$fields = $registration->getAllFields();

$hasimg = true;
// array("type"=>$f->Field->type, "required"=>array(), "value"=>"");
foreach ($fields as $k => $f) {
    ?>
    <p data-required="<?php echo json_encode($f['required'], JSON_THROW_ON_ERROR)?>" data-id="<?php echo $k?>" data-type="<?php echo $f['type']?>" ><label><?php echo __E($f['name'])?>
    <?php   if ($f['description'] != '') :?>
    <img src="/css/assets/icons/small_icons/Info.png" height=16px class="tooltip" title="<?php echo __E($f['description'])?>" />
    <?php endif; ?>
    <span class="required" >*</span>
    </label><?php
        $this->renderPartial("//registere/input/".$f['type'], ["name" => $name."[$k]", "data" => $f, "registration" => $registration]);
    ?><?php printError($registration->getError($name."[$k]"))?></p><?php
}
?>
<br />