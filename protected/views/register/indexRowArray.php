<?php


$b   = Yii::app()->params['baseurl'];
$a   = '/css/assets/';
$er  = ("Edit registration");
$dr  = ("Delete registration");
$ddr = ("Duplcate registration");

foreach ($records as $entry) :
    $r   = [];
    $r[] = <<<COL
        <div class="btn-group" role="group">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary dropdown-toggle"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Actions
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{$b}/register/edit/{$entry->id}"
                           class="dropdown-item btn">Edit</a>
                    </li>
                    <li>
    
                        <a href="{$b}/register/add/{$entry->id}"
                           class="dropdown-item btn">
                            Clone
                        </a>
                    </li>
                    <li>
                        <a href="{$b}/register/delete/{$entry->id}"
                           class="dropdown-item  btn danger">
                            Delete
                        </a>
    
                    </li>
                </ul>
            </div>
        </div>

COL;

    $r[] = __E($entry->name).'';
    $r[] = $this->renderPartial("competitions", ["entry" => $entry], 1);

    if ($entry->updated == "0000-00-00 00:00:00") {
        $entry->updated = $entry->registered;
    }

    $registrationCreated = new DateTime($entry->registered);
    $registrationUpdated = new DateTime($entry->updated);

    $tz       = Yii::app()->config->get('timezone');
    if (!empty($tz)) {
        $registrationCreated->setTimezone(new DateTimeZone($tz));
        $registrationUpdated->setTimezone(new DateTimeZone($tz));
    }

    $r[] = '<span style="display:none">'.$registrationCreated->getTimestamp().'</span>'.$registrationCreated->format("Y-m-d g:i:s a");
    $r[] = '<span style="display:none">'.$registrationUpdated->getTimestamp().'</span>'.$registrationUpdated->format("Y-m-d g:i:s a");
    $output['data'][] = $r;
endforeach;
echo json_encode($output, JSON_THROW_ON_ERROR);
