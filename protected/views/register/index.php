<?php
$this->renderPartial('//generate/messages');
?>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <div class="d-flex align-items-center justify-content-between">
                    <h1 class="my-5">Registrations</h1>
                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/register/add" class="btn btn-primary">
                        <i class="fa fa-plus fa-lg"></i> Add Registration
                    </a>
                </div>
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-12 col-md-4">
                            <div class="input-group">
                                <div class="input-group-text">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" class="form-control dt-search" placeholder="Search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-end">
                </div>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive">

                    <table class="table table-striped table-bordered" id="registrationTable"
                           data-url="<?php echo Yii::app()->params['baseurl'] ?>/register/ajaxIndex">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Registrations</th>
                            <th>Created</th>
                            <th>Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
