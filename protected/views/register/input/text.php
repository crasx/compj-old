<?php
$description = $description ?? $field->description;
$title = $title ?? $field->name;
$type = $type ?? 'text';
$inputClass = 'form-control' . (!empty($errors) ? " has-error" : "");
$id = $id ?? $name;
?>

<?php if ($title != '') : ?>
    <label for="<?php echo $id ?>"><?php echo __E($title) ?> <span class="required-indicator text-danger">*</span> </label>
<?php endif; ?>

<input id="<?php echo $id ?>" name="<?php echo $name ?>" class="form-control <?php echo (isset($errors[$name]) ? " has-error" : "") ?>"
       type="<?php echo $type ?>" value="<?php echo __EF($val) ?>" data-fid="<?php echo $field->id ?>">


<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors)): ?>
    <?php printFormElementErrors($errors); ?>
<?php endif; ?>
