<?php

$this->renderPartial('//register/input/text', [
    'field' => $field,
    'errors' => $errors,
    'registration' => $registration,
    'val' => $val,
    'type' => 'email',
    'name' => $inputName,
]);
