<?php
$description = $description ?? $field->description;
$title = $title ?? $field->name;
$type = $type ?? 'text';
$inputClass = 'form-control' . (!empty($errors) ? " has-error" : "");
$id = $id ?? $name;
?>

<?php if ($title != '') :
    ?><label for="<?php echo $id ?>" class="form-label"><?php echo __E($title) ?> <span class="required-indicator text-danger">*</span> <i class="fa fa-refresh fa-sm fa-spin d-none"><span class="visually-hidden">Load in progress</span></i> </label><?php
endif; ?>

<input id="<?php echo $id ?>" name="<?=$name?>" class="form-control uploader" type="file" accept="image/*" capture="camera" aria-describedby="<?php echo $id ?>--text" />


<?php if (!empty($errors[$name])): ?>
    <?php printFormElementErrors($errors[$name]); ?>
<?php endif; ?>

    <?php if ($description != '') :
        ?>
    <div class="form-text mb-4"><?php echo $description ?></div><?php
    endif; ?>

    <div  class="text-center">
    <?php if ($this->isClone):
        $val = '';
        ?>
        <div class="form-text my-3">Cloning images currently unsupported</em>
    <?php endif; ?>
    <?php if ($val != '') : ?>
        <div class="form-text my-3"> File Uploaded, re-uploading will replace current file.</div>
        <img src="<?php echo Yii::app()->params['baseurl'] ?>/register/image?i=<?php echo $val ?>" class="regimage w-50 m-auto"/>
    <?php else : ?>
        <img src="" class="d-none regimage w-50 m-auto" />
    <?php endif; ?>
    </div>
