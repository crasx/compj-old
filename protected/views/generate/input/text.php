<?php

$inputClass = 'form-control' . (isset($errors[$name]) ? " has-error" : "");
$inputName = $formName . '[' . $name . ']';
$type = $type ?? 'text';
$description = $description ?? '';
$required = $required ?? false;
$title = $title ?? '';
$placeholder = $placeholder ?? '';
?>


<input id="<?= $inputName ?>" <?= $required ? 'required':''?> name="<?= $inputName ?>"  placeholder="<?=__EF($placeholder)?>" class="form-control <?php echo (isset($errors[$inputName]) ? " has-error" : "") ?>"
       type="<?php echo $type ?>" value="<?php echo __EF($val) ?>">

<?php if ($title != '') : ?>
    <label for="<?php echo $inputName ?>"><?php echo __E($title) ?> <?php if($required): ?><span class="required-indicator text-danger">*</span><?php endif; ?></label>
<?php endif; ?>

<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors[$inputName])): ?>
    <?php printFormElementErrors($errors[$inputName]); ?>
<?php endif; ?>
