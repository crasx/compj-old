<?php

$inputClass = 'form-control' . (isset($errors[$name]) ? " has-error" : "");
$inputName = $formName . '[' . $name . ']';
$description = $description ?? '';
$required = $required ?? false;
$placeholder = $placeholder ?? '';
?>


<textarea class="form-control" <?= $required ? 'required':''?> placeholder="<?=__EF($placeholder)?>" id="<?= $inputName ?>" ><?=__E($val) ?></textarea>

<?php if ($title != '') : ?>
    <label for="<?php echo $inputName ?>" ><?php echo __E($title) ?> <?php if($required): ?><span class="required-indicator text-danger">*</span><?php endif; ?></label>
<?php endif; ?>

<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors[$inputName])): ?>
    <?php printFormElementErrors($errors[$inputName]); ?>
<?php endif; ?>
