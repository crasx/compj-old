<?php

$inputClass = 'form-control' . (isset($errors[$name]) ? " has-error" : "");
$inputName = $formName . '[' . $name . ']';
$type = $type ?? 'text';
$description = $description ?? '';
$required = $required ?? false;
$title = $title ?? '';
$options = $options ?? [];
?>

<select id="<?= $inputName ?>" <?= $required ? 'required':''?> name="<?= $inputName ?>" class="form-select <?php echo (isset($errors[$inputName]) ? " has-error" : "") ?>">
    <?php foreach ($options as $optionKey => $optionLabel) : ?>
        <option value="<?= __EF($optionKey) ?>" <?= $optionKey == $val ? 'selected' : '' ?>><?= __E($optionLabel) ?></option>
    <?php endforeach; ?>
</select>

<?php if ($title != '') : ?>
    <label for="<?php echo $inputName ?>"><?php echo __E($title) ?> <?php if($required): ?><span class="required-indicator text-danger">*</span><?php endif; ?></label>
<?php endif; ?>

<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors[$inputName])): ?>
    <?php printFormElementErrors($errors[$inputName]); ?>
<?php endif; ?>
