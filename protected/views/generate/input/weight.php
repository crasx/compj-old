<?php

$inputClass = 'form-control' . (isset($errors[$name]) ? " has-error" : "");
$inputName = $formName . '[' . $name . ']';
$description = $description ?? '';
$required = $required ?? false;
$title = $title ?? '';
$min = $min ?? '';
$max = $max ?? '';
$step = $step ?? '';
$placeholder = $placeholder ?? '';

?>


<input id="<?= $inputName ?>" type="number"  placeholder="<?=__EF($placeholder)?>" min="<?=$min?>" max="<?=$max?>" step="<?=$step?>" name="<?= $inputName ?>" class="form-control <?php echo (isset($errors[$inputName]) ? " has-error" : "") ?>"
        value="<?php echo __EF($val) ?>">

<?php if ($title != '') : ?>
    <label for="<?php echo $inputName ?>"><?php echo __E($title) ?> <?php if($required): ?><span class="required-indicator text-danger">*</span><?php endif; ?></label>
<?php endif; ?>

<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors[$inputName])): ?>
    <?php printFormElementErrors($errors[$inputName]); ?>
<?php endif; ?>
