<?php

$inputClass = 'form-control' . (isset($errors[$name]) ? " has-error" : "");
$inputName = $formName . '[' . $name . ']';
$description = $description ?? '';
$required = $required ?? false;
$title = $title ?? '';
$checked = ($checked ?? false) ? 'checked="checked"' : '';
?>


<input id="<?= $inputName ?>" type="checkbox" <?= $checked ?> name="<?= $inputName ?>" class="form-check-input <?php echo (isset($errors[$inputName]) ? " has-error" : "") ?>"
       value="<?php echo __EF($val) ?>">

<?php if ($title != '') : ?>
    <label for="<?php echo $inputName ?>" class="form-check-label" ><?php echo __E($title) ?> <?php if($required): ?><span class="required-indicator text-danger">*</span><?php endif; ?></label>
<?php endif; ?>

<?php if ($description != '') : ?>
    <span class="form-text pt-2 fst-italic"><?php echo __E($description) ?></span>
<?php endif; ?>

<?php if (!empty($errors[$inputName])): ?>
    <?php printFormElementErrors($errors[$inputName]); ?>
<?php endif; ?>
