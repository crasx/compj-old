<?php

        ;
        Yii::app()->clientScript->registerScriptFile('/js/listSelector.js');
        Yii::app()->clientScript->registerCssFile('/css/listSelector.css');

        $safeName = str_replace(["[", "]"], "_", (string) $inputName);
if (str_ends_with($safeName, "_")) {
    $safeName = substr($safeName, 0, (strlen($safeName) - 1));
}

?>
<table class="listselector dataTable <?php echo (intval($usecheckboxes) + 2)?>-col">
       <thead><tr><th>&nbsp;</th><th><?php echo $title?></th><?php if ($usecheckboxes) :
            ?><th><?php echo $checkboxtitle?></th><?php
      endif; ?></tr></thead>
        <tbody>
            <?php foreach ($items as $k => $v) :
                $a = '';
                if (is_array($v)) {
                    $tooltip = "title='".__E(__E($v['tooltip']))."' class='tooltip'";
                    $a       = $v['attributes'];
                    $v       = $v['value'];
                }
                ?>
                    <tr>
                        <td><input type="checkbox" name="<?php echo $inputName?>[]" <?php echo $a?> value="<?php echo $k?>" <?php echo (is_array($selected)&&array_key_exists($k, $selected) ? "checked" : "")?>  /></td>
                        <td><?php echo __E($v)?></td>
                        <?php if ($usecheckboxes) : ?>
                        <td><input type="checkbox" class="iphone" name="<?php echo $inputName."[{$checkboxname}][]"?> value="<?php echo $k?>" <?php echo (is_array($checked)&&in_array($k, $checked) ? "checked" : "")?> /></td>
                        <?php endif; ?>
                   </tr>
            <?php endforeach; ?>    
            
        </tbody>
 </table>
