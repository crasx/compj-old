<select id="<?php echo $id ?>" class="form-select">
<!--    <option value="-1"></option>-->
    <?php
    foreach ($events as $eid => $e) :
        ?>
        <optgroup label="<?php echo __EF($e['name']) ?>">
        <?php foreach ($e['competitions'] as $id => $c) : ?>
        <option value="<?php echo $id ?>"><?php echo __E($c['name']) ?> (<?php echo $c['count']?>)</option>
        <?php endforeach; ?>
        </optgroup><?php
    endforeach;
    ?>
</select>