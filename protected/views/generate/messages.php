<?php

$messages = Yii::app()->user->getFlashes();

if(empty($messages)){
    return;
}
?>
<div class="main-box-body clearfix">
<?php

foreach($messages as $type => $message):

    if (preg_match('/^danger/', $type)){
        $alertType = 'alert-danger';
        $icon = 'circle-error';
    } elseif (preg_match('/^success/', $type)){
        $alertType = 'alert-success';
        $icon = 'circle-check';
    } elseif (preg_match('/^warning/', $type)){
        $alertType = 'alert-warning';
        $icon = 'circle-warning';
    } else {
        $alertType = 'alert-info';
        $icon = 'circle-info';
    }

    ?>
    <div class="alert <?= $alertType ?> alert-dismissible fade show" role="alert">
        <?php
        echo $message; // Message should be escaped already?
        ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

<?php endforeach;