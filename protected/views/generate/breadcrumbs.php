<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <?php foreach ($this->breadcrumbs as $index => $breacrumb) : ?>
            <li class="breadcrumb-item <?php echo $index == (sizeof($this->breadcrumbs) - 1) ? "active" : "" ?>">
                <?php if (is_array($breacrumb)) : ?>
                    <a href="<?php echo $breacrumb['url'] ?? '#'; ?>">
                        <?php echo $breacrumb['label'] ?? ''; ?>
                    </a>
                <?php else : ?>
                    <span><?php echo $breacrumb ?></span>
                <?php endif; ?>
            </li>

        <?php endforeach; ?>
    </ol>
</nav>