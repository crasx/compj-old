<?php
$b = Yii::app()->params['baseurl'];

$m = [
    // array('label'=>'Home', 'url'=>$b,'icon'=>'/css/'.VERSION.'/assets/icons/small_icons_2/settings.png'),
    [
        'label' => 'Admin',
        'access' => 'admin',
        'icon' => 'fa-gear',
        'url' => '#',
        'activeCheck' => $b . '/settings/',
        // check this url
        'children' => [
            [
                'label' => 'Configuration',
                'access' => 'admin/settings',
                'icon' => '',
                'url' => $b . '/settings/configure',
                'active' => true,
            ],
            [
                'label' => 'Events',
                'access' => 'admin/events',
                'icon' => '',
                'url' => $b . '/settings/events',
                'active' => true,
            ],
            [
                'label' => 'Competitions',
                'access' => 'admin/competitions',
                'icon' => '',
                'url' => $b . '/settings/competitions',
                'active' => true,
            ],
            [
                'label' => 'Statistics',
                'access' => 'admin/competitions',
                'icon' => '',
                'url' => $b . '/settings/statistics',
                'active' => true,
            ],
        ],
    ],
    [
        'label' => 'MC',
        'access' => [
            'mc_lineup',
            'mc_results',
            'mc_report',
        ],
        'icon' => 'fa-microphone',
        'url' => '#',
        'activeCheck' => $b . '/mc/',
        'children' => [
            [
                'label' => 'Lineup',
                'access' => 'mc_lineup',
                'icon' => 'fa-bars',
                'url' => $b . '/mc/lineup',
                'active' => false,
            ],
            [
                'label' => 'Slideshow',
                'access' => 'mc_slideshow',
                'icon' => 'fa-desktop',
                'url' => $b . '/mc/slideshow',
                'active' => false,
            ],
            [
                'label' => 'Results',
                'access' => 'mc_results',
                'icon' => 'fa-sort-amount-desc',
                'url' => $b . '/mc/results',
                'active' => false,
            ],
            [
                'label' => 'Report',
                'access' => 'mc_report',
                'icon' => 'fa-building fa-rotate-180',
                'url' => $b . '/mc/report',
                'active' => false,
            ],
        ],
    ],
    [
        'label' => 'Registration',
        'access' => 'registration',
        'icon' => 'fa-id-card',
        'url' => $b . '/register',
        'activeCheck' => $b . '/register',
    ],
    // array('label' => 'Register', 'url' => $b . '/register/', 'visible' => Yii::app()->user->checkAccess('register'), 'icon' => '/css/assets/icons/small_icons/People.png'),
    [
        'label' => 'Judge',
        'access' => 'judge',
        'icon' => 'fa-balance-scale',
        'url' => $b . '/judge',
        'activeCheck' => $b . '/judge',
    ],

];

$generateMenu = function ($menu_items) use (&$generateMenu) {
    foreach ($menu_items as $index => $menu_item) {
        $activeC = !empty($menu_item['activeCheck']) ? $menu_item['activeCheck'] : $menu_item['url'];
        $active = str_contains((string)Yii::app()->request->requestUri, (string)$activeC) ? 'active open' : '';
        $children = $menu_item['children'] ?? [];

        if ($menu_item['access'] == '' || Yii::app()->user->hasRole($menu_item['access'])) :
            ?>
            <li class="<?php echo $active ?>">
                <?php if (!empty($children)) : ?>
                <a href="#"
                   class="d-flex dropdown-toggle text-light border-0 bg-transparent align-items-center justify-content-start"
                   data-bs-toggle="collapse"
                   data-bs-target="#submenu-<?php echo $index ?>"
                   aria-controls="sidebar-nav" aria-expanded="false" aria-label="Expand navigation">
                    <?php else : ?>
                    <a href="<?php echo $menu_item['url'] ?>"
                       class="d-flex align-items-center justify-content-start <?php echo $active ?>">
                        <?php endif; ?>
                        <?php echo !empty($menu_item['icon']) ? "<i class=\"fa ${menu_item['icon']}\"></i>" : '' ?>
                        <span class="px-3"><?php echo $menu_item['label'] ?></span>
                    </a>
                    <?php if (!empty($children)) {
                        echo '<ul class="submenu collapse ' . ($active ? 'show' : '') . '" id="submenu-' . $index . '">';
                        $generateMenu($menu_item['children']);
                        echo '</ul>';
                    }
                    ?>
            </li>
        <?php endif;
    }//end foreach

};

?>
<?php if (!empty($logo)) : ?>
    <div class="logo-container mt-3">
        <a href="<?php echo Yii::app()->params['baseurl'] ?>">
            <img src="<?php echo $logo ?>" alt=""/>
            <span class="visually-hidden">
            <?php echo __E(Yii::app()->config->get('name')); ?>
            </span>
        </a>
    </div>
<?php else: ?>
    <div class="logo-container mt-3 h2 text-center">
        <a href="<?php echo Yii::app()->params['baseurl'] ?>">
            <span class="">
            <?php echo __E(Yii::app()->config->get('name')); ?>
            </span>
        </a>
    </div>
<?php endif; ?>
    <div class="">
        <ul class="nav nav-pills flex-column p-0 mt-3 vw-50">
            <?php echo $generateMenu($m); ?>
        </ul>

    </div>
<?php



