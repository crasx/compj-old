<h2><a href="javascript:showInformation(<?php echo $comp?>, <?php echo $entryId?>)"><?php echo __E($entry->name)?> <img src="/css/assets/icons/small_icons/Info.png" height=16px  /></a></h2>
<em><?php echo __E($entry->description)?></em>
<?php
if ($criteria) {
    foreach ($criteria as $c) :
        $c  = $c->Criteria;
        $id = "criteria_".$comp."_".$entryId."_".$c->id;
        $v  = "";
        $v2 = $c->min;
        if (isset($scores[$c->id])) {
            $v = $v2 = $scores[$c->id]->score;
        }
        ?>
        <div class="sholder">
            <label for="<?php echo $id?>" class="tooltip" title="<?php echo __EF($c->description)?>" ><?php echo __E($c->name)?></label><br />
            <span class="min"><?php echo $c->min?></span>
            <span class="max"><?php echo $c->max?></span>
            <div class="slider" data-min="<?php echo __EF($c->min)?>" data-max="<?php echo __EF($c->max)?>" data-step="<?php echo __EF($c->step)?>" data-value="<?php echo __EF($v2)?>" id="<?php echo $id?>" ></div>
            <input type="text" name="<?php echo $id?>" data-competition="<?php echo $comp?>" data-entry="<?php echo $entryId?>" data-criteria="<?php echo $c->id?>" value="<?php echo __EF($v)?>" />
        </div>
        <?php
    endforeach;
};
