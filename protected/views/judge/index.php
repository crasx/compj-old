<h1><?php echo __E(Yii::app()->config->get("name")) ?> Judging</h1>

<div class="row">
    <div class="col-12 col-md-6 ">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">

                    <h2 class="mb-3">Contestants</h2>
                    <span class="fsti-talic mb-4 d-block">Select a competition from the dropdown to view the contestants in that competition. </span>
                    <div class="form-floating mb-4">
                        <?php $this->renderPartial("//generate/competitionDD", ["events" => $this->getEventList(), "id" => "competitionList"]); ?>
                        <label for="competitionList">Select competition:</label>
                    </div>
            </header>

            <div class="main-box-body clearfix">

                <div class="table-responsive">

                    <table id="JudgeTable" class="fancy   table-striped table-bordered"
                           data-url="<?php echo Yii::app()->params['baseurl'] ?>/judge/contestants"
                           data-sliderurl="<?php echo Yii::app()->params['baseurl'] ?>/judge/sliders"
                    >
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Average</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3" class="text-center">
                                <a href="#" title="Last updated" class="btn btn-link pull-right w-100"
                                   id="lineup-refresh">
                                    <i class="fa fa-refresh fa-lg me-2"></i> <span class="updated text-italic"></span>
                                </a>
                                <div class="legend">
                                    <span class="badge bg-danger">Not here</span>
                                    <span class="badge bg-warning">Here</span>
                                    <span class="badge bg-success">On stage</span>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card bg-white mb-3" id="scoreList">
            <div class="card-body">
                <h2 class="card-title h4">Scores <i class="fa fa-refresh fa-spin fa-small d-none"></i></h2>
                <div class="card-text fields">
                </div>
            </div>
        </div>

        <div class="card bg-white" id="entryFields">
            <div class="card-body">
                <h2 class="card-title h4">Contestant Information <i class="fa fa-refresh fa-spin fa-small d-none"></i>
                </h2>
                <div class="card-text fields">
                </div>
            </div>
        </div>

    </div>
</div>

<div class="d-none" id="sliderTemplate">
    <div class="sliderHolder my-5">
        <label for="" class="form-label">Slider</label>
        <input type="range" class="d-block w-100 form-range" min="0" max="0" step="0"/>

        <div class="d-flex justify-content-between">
            <span class="min">0</span>
            <input type="number" min="0" max="0" step="0"/>
            <span class="max">0</span>

        </div>

    </div>
</div>