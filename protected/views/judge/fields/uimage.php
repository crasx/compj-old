<dt><?php echo __E($name) ?></dt>
<dd class="text-center"><?php if (!empty($value)) : ?>
        <a class="contestant-image-preview"
           href="<?php echo Yii::app()->params['baseurl'] ?>/judge/image?i=<?php echo __E($value) ?>" target="_blank">
            <img src="<?php echo Yii::app()->params['baseurl'] ?>/judge/image?i=<?php echo __E($value) ?>"
                 class="contestant-image-preview w-50"/>

        </a>
    <?php endif; ?>
</dd>