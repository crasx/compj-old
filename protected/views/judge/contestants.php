<?php
$this->renderPartial("//formtools");
$rdata = ['data' => []];

if ($competition != null) {
    $c = $competition->CriteriaCount;
    foreach ($competition->CEntries as $entry) :
        if (!$entry->active) {
            continue;
        }

        $data   = [];
        $data[] = $entry->id;

        $s      = $entry->Entry->getScoreCount($competition->id);
        $avg    = round($entry->Entry->getJudgeAverage($competition->id, Yii::app()->user->getUID()) ?? 0, 2);
        $n      = __E($entry->Entry->name);

        if (!$entry->here) {
            $status = 'bg-danger';
            $statusText = 'Not here';
        }else if ($entry->called) {
            $status = 'bg-success';
            $statusText = 'On stage';
        } else if ($entry->here) {
            $status = 'bg-warning';
            $statusText = 'Here';
        }

        $data[] = <<<link
        <a href="#" id="c-{$entry->entry}" class="entryAnchor position-relative" data-entry="{$entry->entry}" data-competition="{$entry->competition}">{$n}
          <span class="badge {$status} p-1 border border-light rounded-circle">
          <span class="visually-hidden">{$statusText}</span>
</span>
      </a>
link;
        $currentPercentage   = 100;
        if ($c != 0) {
            $currentPercentage = ($s / $c * 100);
        }

        $background = 'bg-success';
        if ($s != $c) {
            $background = 'bg-warning';
        }

        $cc     = $c == 1 ? "" : "s";
        $data[] = <<<progress
        <div class="average">{$avg}</div>
        <div class="scores" >{$s} of {$c} score{$cc}</div>
        <div class="progress">
            <div class="progress-bar {$background}" role="progressbar" aria-valuenow="{$currentPercentage}" aria-valuemin="0" aria-valuemax="100" style="width: {$currentPercentage}%">
            </div>
        </div>

progress;

        $rdata['data'][] = $data;
    endforeach;
}//end if

echo json_encode($rdata, JSON_THROW_ON_ERROR);
