<?php
// Check if the FORMTOOLS constant is not defined
if (!defined('FORMTOOLS')) {

    /**
     * Function to print error messages
     *
     * @param mixed $obj The error message to be printed
     */
    function printError($obj)
    {
        ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $obj ?>
        </div> <?php

    }//end printError()

    /**
     * Function to print success messages
     *
     * @param mixed $obj The success message to be printed
     */
    function printSuccess($obj)
    {
        ?>
        <span class="validate_success"><?php echo $obj ?></span><?php

    }//end printSuccess()

    /**
     * Function to print descriptions
     *
     * @param mixed $obj The description to be printed
     */
    function printDescription($obj)
    {
        ?>
        <span class="help-block"><?php echo $obj ?></span><?php

    }//end printDescription()

    /**
     * Function to print form element errors
     *
     * @param mixed $errors The errors to be printed
     */
    function printFormElementErrors($errors)
    {
        if (!is_array($errors)) {
            $errors = [$errors];
        }

        foreach ($errors as $error) {
            echo '<span class="text-danger">' . $error . '</span>';
        }

    }//end printFormElementErrors()

    /**
     * Function to get input class based on errors
     *
     * @param array $errors The array of errors
     * @param string $index The index of the error
     * @return string Returns the class name based on the error
     */
    function getInputClass($errors = [])
    {
        return empty($errors) ? "has-error" : "";
    }//end getInputClass()

    /**
     * Function to make a standard textarea
     *
     * @param string $title The title of the textarea
     * @param string $formName The name of the form
     * @param string $index The index of the textarea
     * @param array $errors The array of errors
     * @param string $val The value of the textarea
     * @param bool $required Whether the textarea is required or not
     */
    function makeStandardTextarea($title, $formName, $index, $errors, $val, $required = false)
    {
        ?>
        <div class="form-group <?php echo isset($errors[$index]) ? "has-error" : "" ?>">
            <label for="<?php echo $formName . '_' . $index ?>"><?php echo $title ?><?php if ($required) :
                    ?><span class="required">*</span><?php
                endif; ?></label>
            <textarea id="<?php echo $formName . '_' . $index ?>" name="<?php echo $formName . '[' . $index . ']' ?>"
                      class="form-control"><?php echo __E($val) ?></textarea>
            <?php if (!empty($errors[$index])) {
                printFormElementErrors($errors[$index]);
            } ?>
        </div>


        <?php

    }//end makeStandardTextarea()

    /**
     * Function to make a standard text input
     *
     * @param string $title The title of the input
     * @param string $formName The name of the form
     * @param string $index The index of the input
     * @param array $errors The array of errors
     * @param string $val The value of the input
     * @param string $type The type of the input
     * @param string $description The description of the input
     * @param bool $required Whether the input is required or not
     */
    function makeStandardTextInput($title, $formName, $index, $errors, $val, $type = 'text', $description = '', $required = false)
    {
        $requiredIndicator = $required ? '<span class="required">*</span>' : '';
        $inputClass = 'form-control' . (isset($errors[$index]) ? " has-error" : "");
        $id = $formName . '_' . $index;
        $name = $formName . '[' . $index . ']';
        ?>
        <div class="form-floating mb-4">


            <input id="<?php echo $id ?>" name="<?php echo $name ?>" class="<?php echo $inputClass ?>"
                   type="<?php echo $type ?>" value="<?php echo __EF($val) ?>">

            <?php if ($title != '') : ?>
                <label for="<?php echo $id ?>"><?php echo __E($title) . $requiredIndicator ?></label>
            <?php endif; ?>

            <?php if ($description != '') : ?>
                <span class="form-text pt-2 fst-italic"><?php echo $description ?></span>
            <?php endif; ?>

            <?php if (!empty($errors[$index])): ?>
                <?php printFormElementErrors($errors[$index]); ?>
            <?php endif; ?>

        </div>
        <?php

    }//end makeStandardTextInput()

    /**
     * Function to make a standard select input
     *
     * @param string $title The title of the select input
     * @param string $formName The name of the form
     * @param string $index The index of the select input
     * @param array $errors The array of errors
     * @param string $val The value of the select input
     * @param array $options The options for the select input
     * @param bool $required Whether the select input is required or not
     */
    function makeStandardSelect($title, $formName, $index, $errors, $val, $options, $required = false)
    {
        ?>
        <div class="form-group <?php echo isset($errors[$index]) ? "has-error" : "" ?>">
            <label for="<?php echo $formName . '_' . $index ?>"><?php echo $title ?><?php if ($required) :
                    ?><span class="required">*</span><?php
                endif; ?></label>
            <select id="<?php echo $formName . '_' . $index ?>" name="<?php echo $formName . '[' . $index . ']' ?>"
                    class="form-control">
                <?php foreach ($options as $k => $v) : ?>
                    <option value="<?php echo $k ?>" <?php echo $k == $val ? "selected" : "" ?>><?php echo __E($v) ?></option>
                <?php endforeach; ?>
            </select>
            <?php if (!empty($errors[$index])) {
                printFormElementErrors($errors[$index]);
            } ?>
        </div>
        <?php

    }//end makeStandardSelect()

    // Define the FORMTOOLS constant
    define("FORMTOOLS", 1);
}//end if