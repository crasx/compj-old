<?php
  $b = Yii::app()->params['baseurl']."/settings";

      $m = [
          [
              'label'   => 'Configuration',
              'url'     => $b."/configure",
              'visible' => Yii::app()->user->checkAccess('settings_configure'),
              'icon'    => '/css/assets/icons/dashboard/57.png',
              'title'   => 'CJ Account settings',
          ],
          // array('label'=>'Users', 'url'=>$b."/users",'visible'=>Yii::app()->user->checkAccess('settings_user'),'icon'=>'/css/assets/icons/dashboard/46.png', 'title'=>'Modify users'),
          // array('label'=>'Criteria', 'url'=>$b.'/criteria', 'visible'=>Yii::app()->user->checkAccess('settings_criteria'), 'icon'=>'/css/'.VERSION.'/assets/icons/dashboard/117.png', 'title'=>'Create and edit criteria'),
          [
              'label'   => 'Events',
              'url'     => $b.'/events',
              'visible' => Yii::app()->user->checkAccess('settings_event'),
              'icon'    => '/css/assets/icons/dashboard/4.png',
              'title'   => 'Create and edit competitions',
          ],
          [
              'label'   => 'Competitions',
              'url'     => $b.'/competitions',
              'visible' => Yii::app()->user->checkAccess('settings_competition'),
              'icon'    => '/css/assets/icons/dashboard/1.png',
              'title'   => 'Create and edit competitions',
          ],
      ];
      // <li class="current"><a href="#" class="dashboard"><img src="/css/".VERSION."/assets/icons/small_icons_3/dashboard.png" alt="" /><span class="current">Dashboard</span></a></li>
        ?>  <h1>Settings dashboard</h1>

    <ul class="dash">
<?php
foreach ($m as $mm) {
    if (!isset($mm['visible'])||$mm['visible']) {
        ?>
        <li class="fade_hover tooltip" title="<?php echo $mm['title']?>" >
            <a href="<?php echo $mm['url']?>" class="dashboard">
                <img src="<?php echo $mm['icon']?>" alt="" />
                <span><?php echo $mm['label']?></span>
            </a>
        </li>
        <?php
    }
}
?>
    </ul>
