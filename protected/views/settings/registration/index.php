<?php
$this->renderPartial('//generate/messages');

$get = [];
if (isset($_GET['fu']) && is_array($_GET['fu'])) {
    $get = $_GET['fu'];
}

$fcnt = 0;
foreach ($fields as $field) :
    if (in_array($field['id'], $get)) {
        continue;
    }

    $fid = $field['id'];
    $formName = "registration[$fid]";
    $fcnt++;
    ?>
    <div class="wrapper">
        <div class="form-check">
            <input type="checkbox" name="registration[<?php echo $fid ?>]" id="registration_<?php echo $fid ?>" data-fid="<?=$fid?>">
            <label for="registration_<?php echo $fid ?>"><?php echo __E($field['name']) ?> </label>
            <div class="form-text pt-2 fst-italic">(<?php echo __E($field->Type->name) ?>) <?php echo __E($field['description']) ?></div>
        </div>
        <div class="d-none">
            <div class="name"><?php echo __E($field['name']) ?> - <em><?php echo __E($field->Type->name)?></em><br /><em><?php echo __E($field['description'])?></em></div>
            <div class="type"><?php echo __E($field->Type->name)?></div>
            <div class="form-check mb-3 form-switch mc">
                <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => $formName, 'name' => 'mc', 'val' => $fid]) ?>
            </div>
            <div class="form-check mb-3 form-switch judge">
                <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => $formName, 'name' => 'judge', 'val' => $fid]) ?>
            </div>
            <div class="form-check mb-3 form-switch report">
                <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => $formName, 'name' => 'report', 'val' => $fid]) ?>
            </div>
            <div class="form-check mb-3 form-switch required">
                <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => $formName, 'name' => 'required', 'val' => $fid]) ?>
            </div>
            <div class="input"><?php echo $this->renderPartial('//generate/input/weight', ['formName' => $formName, 'name' => 'weight', 'val' => '']) ?></div>
        </div>
    </div>
<?php endforeach;

if ($fcnt == 0) :?>
    <em>No Fields Available</em>
    <?php
endif; ?>



<div class="pull-right">
    <a href="#" id="addNewRegistrationButton" class="btn btn-primary">
        <i class="fa fa-plus-circle fa-lg"></i> New Field
    </a>
</div>