<br /><h1>Delete registration field</h1>

<form action="<?php echo $_SERVER["REQUEST_URI"]?>" method="post" id="registrationForm" >
<fieldset>
    <legend>Confirm delete</legend>
    
    <p>Are you sure you want to delete field <?php echo __E($registration->name)?>?</p>
    
    <?php if ($registration->hasValues()) :?>
      <p><em>This will also delete registration values under this field</em></p>
    <?php endif; ?>

  </fieldset>
   <p><input type=hidden name="registration[confirm]" value="1" />
    <input class="button" type="submit" onclick="javascript:ajaxSubmit($j('#registrationForm'), function(data){registrationCallback(data);});return false;" value="Yes">
       <a class="button_link" href="javascript:$j.fancybox.close()">No</a></p>

</form>