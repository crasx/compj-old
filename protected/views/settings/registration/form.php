<?php
$errors = $registration->getErrors();
$this->renderPartial('//generate/messages');

//makeStandardTextInput('Field Name', 'registration', 'name', $errors, $registration->name);
//makeStandardTextarea('Description', 'registration', 'description', $errors, $registration->description);
//makeStandardSelect('Type', 'registration', 'type', $errors, $registration->type, $registration->getValidTypes());
?>

<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/text', ['formName' => $this->_OBJ, 'name' => 'name', 'title' => 'Field Name', 'val' => $registration->name, 'errors' => $errors, 'required' => TRUE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/textarea', ['formName' => $this->_OBJ, 'name' => 'description', 'title' => 'Description', 'val' => $registration->description, 'errors' => $errors, 'required' => FALSE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/select', ['formName' => $this->_OBJ, 'name' => 'type', 'title' => 'Type', 'val' => $registration->type, 'errors' => $errors, 'required' => TRUE, 'options' => $registration->getValidTypes()]); ?>
</div>
