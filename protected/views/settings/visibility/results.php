<?php
    Yii::app()->clientScript->registerScriptFile("/js/settings/visibility.score.js", CClientScript::POS_HEAD);
    $this->renderPartial("//formtools");

?><h1>Calculate results</h1>
<h2>Events</h2>
<?php
if ($success) {
    printMessageSuccess($success);
}

if ($error) {
    printMessageError($error);
}
?>
<table  class="fancy fullwidth" id="eventtable" >
    <thead>
    <tr>
        <th>Event</th>
        <th>Competitions</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($events as $e) :?>
        <tr>

            <td><?php echo __E($e->name)?><br /><em><?php echo __E($e->description)?></em></td>
            <td><?php echo $this->renderPartial("//settings/events/listCompetitions", ["event" => $e])?></td>
            <td>
        <?php if ($e->finished) :?>
                <a href="<?php echo Yii::app()->params['baseurl']?>/settings/visibility/results?remove=<?php echo $e->id?>" title="1" class="tooltip table_icon"><img src="/css/assets/icons/small_icons/Erase.png" alt=""></a>
                <a href="<?php echo Yii::app()->params['baseurl']?>/settings/visibility/results?calculate=<?php echo $e->id?>" title="1" class="tooltip table_icon"><img src="/css/assets/icons/small_icons/Calculator.png" alt=""></a>
                
                <?php else : ?>
                <a href="<?php echo Yii::app()->params['baseurl']?>/settings/visibility/results?calculate=<?php echo $e->id?>" title="1" class="tooltip table_icon"><img src="/css/assets/icons/small_icons/Calculator.png" alt=""></a>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach; ?>    
    </tbody>
</table>