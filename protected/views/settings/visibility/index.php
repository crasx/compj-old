<h1>Visibility settings</h1>

<h2>Events</h2>

<?php
    Yii::app()->clientScript->registerScriptFile("/js/settings/visibility.js", CClientScript::POS_HEAD);

?>
<form id="visibilityForm" action="/<?php echo CLIENT?>/settings/visibility/update">
<table class="fancy fullwidth" id="eventtable">
    <thead>
        <tr>
            <th>Name</th>
            <th>Registration</th>
            <th>MC</th>
            <th>Judging</th>
            <th>Competitions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($events as $e) :?>
        <tr>                              
            <td><?php echo __E($e->name)?><br /><em><?php echo __E($e->description)?></em></td>
            <td><input type="checkbox" class="iphone event" name="event[<?php echo $e->id?>][registration]" <?php echo $e->registration ? "checked" : "" ?> /></td>
            <td><input type="checkbox" class="iphone event" name="event[<?php echo $e->id?>][mc]"<?php echo $e->mc ? "checked" : "" ?> /></td>
            <td><?php if ($e->finished) :?>
                <em>Scores have been calculated.<br />Visit <a href="/<?php echo CLIENT?>/settings/visibility/results">calculate results</a> to clear them</em>
            <?php else :
                ?><input type="checkbox" class="iphone  event" name="event[<?php echo $e->id?>][judge]" <?php echo $e->judge ? "checked" : "" ?> /><?php
            endif; ?></td>
            <td><?php echo $this->renderPartial("//settings/events/listCompetitions", ["event" => $e])?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
   </form>