<?php
$this->renderPartial("//formtools");
$cities = $this->getTimezones();

$errors = $this->getErrors();

$this->renderPartial('//generate/messages');

?>
<h1>Configuration</h1>


<form action="?" method="post" id="configureForm" enctype="multipart/form-data">
    <div class="col-lg-6 col-md-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Basic Information</h2>
            </header>
            <div class="main-box-body clearfix">
                <div class="form-group <?php echo isset($errors['name']) ? "has-error" : "" ?>">
                    <label for="configureName">Competition Name</label>
                    <input id="configureName" name="configure[name]" type="text"
                           value="<?php echo __EF(Yii::app()->config->get('name')) ?>">
                    <?php if (!empty($errors['name'])) {
                        printFormElementErrors($errors['name']);
                    } ?>
                </div>

                <div class="form-group <?php echo isset($errors['timezone']) ? "has-error" : "" ?>">
                    <label for="configureTimezone">Timezone</label>
                    <select name="configure[timezone]">
                        <?php foreach ($cities as $key => $value) {
                            echo "<optgroup label='$key'>";
                            foreach ($value as $k => $c) {
                                echo "<option value='$c' ".(strcmp((string) $c, (string) Yii::app()->config->get('timezone')) == 0 ? "selected" : "")." >$c</option>";
                            }

                            echo "</optgroup>";
                        }?>
                    </select>
                    <?php if (!empty($errors['timezone'])) {
                        printFormElementErrors($errors['timezone']);
                    } ?>
                </div>

            </div>
        </div>
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Settings</h2>
            </header>

            <div class="main-box-body clearfix">
            <div class="form-group <?php echo isset($errors['name']) ? "has-error" : "" ?>">
                <label for="precision">Result rounding precision</label>
                <input id="precision" name="configure[precision]" type="text"
                       value="<?php echo __EF(Yii::app()->config->get('precision')) ?>">
                <?php if (!empty($errors['precision'])) {
                    printFormElementErrors($errors['precision']);
                } ?>
                <?php printDescription("Number of points after the decimal to round to.") ?>
            </div>
                <div class="form-group <?php echo isset($errors['flickr_id']) ? "has-error" : "" ?>">
                    <label for="configureName">Flickr ID</label>
                    <input id="configureName" name="configure[flickr_id]" type="text"
                           value="<?php echo __EF(Yii::app()->config->get('flickr_id')) ?>">
                    <?php if (!empty($errors['flickr_id'])) {
                        printFormElementErrors($errors['flickr_id']);
                    } ?>
                    <?php printDescription("The last part of your home url, ie: https://www.flickr.com/photos/[flickr id]/") ?>
                </div>


            </div>
        </div>
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>License</h2>
            </header>
            <div class="main-box-body clearfix">
                <em><?php echo __E(Yii::app()->params['licensename'])?></em>

            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Branding</h2>
            </header>
            <div class="main-box-body clearfix">
                <div class="form-group <?php echo isset($errors['logo']) ? "has-error" : "" ?>">
                    <label for="configureLogo">Logo</label>
                    <input name="configure[logo]" id="configureLogo" type="file"/>
                    <?php
                    // printDescription("Will be resized to height of 30px;")
                    ?>
                    <?php if (!empty($errors['logo'])) {
                        printFormElementErrors($errors['logo']);
                    } ?>
                    <img style="height:30px;width:auto;" src="<?php echo __EF(Yii::app()->config->get('logo')) ?>"/><br/>
                </div>

                <div class="form-group <?php echo isset($errors['favicon']) ? "has-error" : "" ?>">
                    <label for="configureFavicon">Favicon</label>
                    <input name="configure[favicon]" id="configureFavicon" type="file"/>
                    <?php printDescription("16x16, 32x32. .ico") ?>
                    <?php if (!empty($errors['favicon'])) {
                        printFormElementErrors($errors['favicon']);
                    } ?>
                    <img src="<?php echo __EF(Yii::app()->config->get('favicon')) ?>"/><br/>
                </div>


            </div>
        </div>
    </div>
    <div class="main-box-body clearfix text-center">
        <br/>
        <input type="submit" class="btn btn-primary" value="Submit">
        <input class="btn btn-warning" type="reset" value="Reset"></p>
    </div>
 </form>