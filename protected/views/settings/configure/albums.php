<?php
    $albums = $this->getAlbums();
?>
<select name="<?php echo $name?>" >
    <option value="-1"></option>
    <?php
    foreach ($albums as $id => $name) :
        ?>
    <option value="<?php echo $id?>" <?php echo $value == $id ? "selected" : ""?> ><?php echo __E($name)?></option>
        <?php
    endforeach;
    ?>
</select>
        