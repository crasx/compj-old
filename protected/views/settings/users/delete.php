<h1>Delete user</h1>

<form action="?" method="post" >
<fieldset>
    <legend>Confirm delete</legend>
    
    <p>Are you sure you want to delete user <?php echo __E($user->username)?>?</p>
    
    <?php if ($user->hasScores()) :?>
      <p><em>This will also delete their scores</em> </p>
    <?php endif; ?>

  </fieldset>
   <p><input type=hidden name="user[confirm]" value="1" />
   <input class="button" type="submit" value="Yes"> <a class="button_link" href="<?php echo Yii::app()->params['baseurl']?>/settings/users">No</a></p>
</form>