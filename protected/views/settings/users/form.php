<?php $this->renderPartial('//formtools');
if (isset($error)) {
    printMessageError($error);
}
?>

<form action="?" method="post" >
<fieldset>
    <legend>Basic information</legend>
    
    <p><label>Username:</label> <input class="sf" name="user[username]" type="text" value="<?php echo __EF($user->username)?>"><?php printError($user->getError('username'))?> </p>
    <p><label>Email:</label> <input class="sf" name="user[email]" type="text" value="<?php echo __EF($user->email)?>"> <?php printError($user->getError('email'))?>
    <p><label>Password:</label> <input class="sf" name="user[password][0]" type="password" value="">    
<?php printError($user->getError('password'))?> <?php if ($user->scenario == 'edit') {
    printDescription(__T('Leave password blank to keep the same'));
}?></p>
    <p><label>Verify password:</label> <input class="sf" name="user[password][1]" type="password" value=""><?php printError($user->getError('password2'))?> </p>
</fieldset>
<fieldset>
        <legend>Permissions</legend>


    <div class="one_fourth">
    <?php printError($user->getError('permissions'))?>
        <table class="permissions"><?php foreach (Yii::app()->params['permissions'] as $id => $arr) :?>
            <tr><td><label for="cb<?php echo $id?>" ><?php echo $arr[0]?></label></td>
            <td><input type="checkbox" name="user[permissions][<?php echo $id?>]" id="cb<?php echo $id?>" <?php echo $user->hasPermission($arr[1]) ? "checked" : ""?>></td>
            </tr>
       <?php endforeach;?>
        </table>
        <?php if ($user->scenario == 'edit' && $user->hasScores()) {
            printDescription(__T('Removing judge permission will delete all scores'));
        }?>
    </div>
</fieldset>
    <p><input class="button" type="submit" value="Submit"> <input class="button" type="reset" value="Reset"><a class="button_link" href="<?php echo CLIENT?>/settings/users">Cancel</a></p>
