<?php
$this->breadcrumbs = ['Users'];?>
<h1>Users</h1>
<?php
if (isset($_GET['message'])) {
    $this->renderPartial("//formtools");
    switch ($_GET['message']) {
    case "edited":
        printMessageSuccess("User successfully edited");
        break;
    case "added":
        printMessageSuccess("User successfully added");
        break;
    case "deleted":
        printMessageSuccess("User successfully deleted");
        break;
    case "invalid":
        printMessageError("Invalid user, please notify support if error persists");
        break;
    }
}

    $code = <<<DT
	jQuery("#usertable").dataTable({"sDom": '<"top"f>rt<"bottom"lp><"clear">', "aoColumns":[
	{"bSortable":false},null,null,null,{"bSortable":false}
	],"fnDrawCallback":function(){
        if(jQuery("#usertable").find("tr:not(.ui-widget-header)").length<=5){
            jQuery("#usertable_paginate").css('display', "none");
        } else {
            jQuery("#usertable_paginate").css('display', "block");
        }
    }});
DT;
    Yii::app()->clientScript->registerScript("dt", $code, CClientScript::POS_READY);
?>
<table class="fancy fullwidth" id="usertable">
                        <thead>
                            <tr>
                                <th> </th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Last login</th>
                                <th>Permissions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ($users) {
                            foreach ($users as $u) :?>
                            <tr class="odd">
                                <td>
                                    <a href="<?php echo Yii::app()->params['baseurl']?>/settings/users/edit/<?php echo $u->id?>" title="1" class="tooltip table_icon"><img src="/css/assets/icons/actions_small/Pencil.png" alt=""></a>
                                    <a href="<?php echo Yii::app()->params['baseurl']?>/settings/users/delete/<?php echo $u->id?>" title="1" class="tooltip table_icon"><img src="/css/assets/icons/actions_small/Trash.png" alt=""></a>
                                </td>
                                <td><?php echo __E($u->username)?></td>
                                <td><?php echo __E($u->email)?></td>
                                <td><?php echo __E($u->lastLogin == 0 ? __T("Never") : date("Y/m/d H:i:s",  strtotime((string) $u->lastLogin))) ?></td>
                                <td><?php
                                    $_p = Yii::app()->params['permissions'];

                                foreach ($_p as $perm => $bit) {
                                    if (($u->permissions & $bit[1]) == $bit[1]) {
                                        echo '<img src="'.$bit[2].'" title="'.$bit[0].'" class="tooltip" /> ';
                                    }
                                }
                                ?></td>
                            </tr>
                            <?php endforeach;
                        }//end if

                        ; ?>
                        </tbody>
                    </table>
                    <br class=c />
                    <br class=c />
        <a href="<?php echo Yii::app()->params['baseurl']?>/settings/users/add" class="button_link">Add user</a>