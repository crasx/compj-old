<?php
$this->renderPartial('//generate/messages');

$get = [];
if (isset($_GET['cu']) && is_array($_GET['cu'])) {
    $get = $_GET['cu'];
}

$ccnt = 0;
foreach ($criteria as $c) :
    if (in_array($c['id'], $get)) {
        continue;
    }

    $cid = $c['id'];
    $ccnt++;
    ?>
    <div class="wrapper">
        <div class="form-check pb-3">
            <input type="checkbox" name="criteria[<?php echo $cid ?>]" id="criteria_<?php echo $cid ?>" data-name="<?php echo __EF($c['name']) ?>">
            <label for="criteria_<?php echo $cid ?>"><?php echo __EF($c['name']) ?> (<?php echo $c['min'].' - '.$c['max'] ?>)</label>
            <div class="form-text fst-italic"><?php echo __E($c['description']) ?></div>
        </div>
        <div class="d-none row-template">
            <div class="name"><?php echo __E($c['name'])?></div>
            <div class="min"><?php echo __E($c['min'])?></div>
            <div class="max"><?php echo __E($c['max'])?></div>
            <div class="input"><?php echo $this->renderPartial('//generate/input/weight', ['formName' => 'criteria[weight]', 'name' => $c['id'],'val' => 0]) ?></div>
        </div>
    </div>
<?php endforeach;

if ($ccnt == 0) :?>
<em>No Criteria Available</em>
    <?php
endif; ?>

<br />
<br />
<div class="pull-right">
    <a href="#" id="addNewCriteriaButton" class="btn btn-primary">
        <i class="fa fa-plus-circle fa-lg"></i> New Criteria
    </a>
</div>