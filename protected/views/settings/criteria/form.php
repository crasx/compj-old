<?php
$errors = $criteria->getErrors();
$this->renderPartial('//generate/messages');

?>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/text', ['formName' => $this->_OBJ, 'name' => 'name', 'title' => 'Event Name', 'description' => '', 'val' => $criteria->name, 'errors' => $errors, 'required' => TRUE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/textarea', ['formName' => $this->_OBJ, 'name' => 'description', 'title' => 'Description', 'description' => '', 'val' => $criteria->description, 'errors' => $errors, 'required' => FALSE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/text', ['formName' => $this->_OBJ, 'name' => 'min', 'title' => 'Min score', 'description' => '', 'val' => $criteria->min, 'errors' => $errors, 'required' => TRUE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/text', ['formName' => $this->_OBJ, 'name' => 'max', 'title' => 'Max score', 'description' => '', 'val' => $criteria->max, 'errors' => $errors, 'required' => TRUE]); ?>
</div>
<div class="form-floating mb-4">
    <?php $this->renderPartial('//generate/input/text', ['formName' => $this->_OBJ, 'name' => 'step', 'title' => 'Score Increment', 'description' => '', 'val' => $criteria->step, 'errors' => $errors, 'required' => TRUE]); ?>
</div>


