<br /><h1>Delete criteria field</h1>

<form action="<?php echo $_SERVER["REQUEST_URI"]?>" method="post" id="criteriaForm" >
<fieldset>
    <legend>Confirm delete</legend>
    
    <p>Are you sure you want to delete field <?php echo __E($criteria->name)?>?</p>
    
    <?php if ($criteria->hasValues()) :?>
      <p><em>This will also delete scores under this field</em></p>
    <?php endif; ?>

  </fieldset>
   <p><input type=hidden name="criteria[confirm]" value="1" />
    <input class="button" type="submit" onclick="javascript:ajaxSubmit($j('#criteriaForm'), function(data){criteriaCallback(data);});return false;" value="Yes"> <a class="button_link" href="javascript:$j.fancybox.close()">No</a></p>

</form>