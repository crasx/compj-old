<?php
$this->renderPartial('//generate/messages');
?>

    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active"><span>Dashboard</span></li>
            </ol>

            <h1>Dashboard</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-sm-6 col-xs-12">
            <div class="main-box infographic-box">
                <i class="fa fa-user red-bg"></i>
                <span class="headline">Users</span>
                <span class="value"><?php echo sizeof($users)?></span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-xs-12">
            <div class="main-box infographic-box">
                <i class="fa fa-shopping-cart emerald-bg"></i>
                <span class="headline">Entries</span>
                <span class="value"><?php echo $entries?></span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-xs-12">
            <div class="main-box infographic-box">
                <i class="fa fa-money green-bg"></i>
                <span class="headline">Events</span>
                <span class="value"><?php echo $events?></span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-xs-12">
            <div class="main-box infographic-box">
                <i class="fa fa-eye yellow-bg"></i>
                <span class="headline">Competitions</span>
                <span class="value"><?php echo sizeof($competitions)?></span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 col-xs-12">
            <div class="main-box infographic-box">
                <i class="fa fa-eye yellow-bg"></i>
                <span class="headline">Scores</span>
                <span class="value"><?php echo sizeof($competitions)?></span>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Users</h2>

            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive clearfix">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Roles</th>
                            <th>Events</th>
                            <th>Competitons</th>
                            <th>Scores entered</th>
                            <th>Score percentile</th>
                            <th>Score deviation</th>
                            <th>Last Active</th>
                        </tr>
                        <tbody>
                        </thead>
                        <?php

                        $round = Yii::app()->config->get('round');
                        if (empty($round)) {
                            $round = 2;
                        }

                        foreach ($users as $u) :?>
                            <tr>
                            <td><a href="<?php echo Yii::app()->params['baseurl']?>/settings/statistics/user/<?php echo $u['id']?>"><?php echo __E($u['firstName'].' '.$u['lastName'])?></a></td>
                            <td><?php echo __E($u['email'])?></td>
                            <td></td>
                            <td><?php echo $u['events']?></td>
                            <td><?php echo $u['competitions']?></td>
                            <td><?php echo !empty($scores[$u['id']]) ? $scores[$u['id']]['scores'] : '0'?></td>
                            <td><?php echo !empty($scores[$u['id']]) ? round($scores[$u['id']]['percentage'], $round).'%' : ''?></td>
                            <td><?php echo !empty($scores[$u['id']]) ? round($scores[$u['id']]['stddev'], $round).'%' : ''?></td>
                            <td><?php echo $u['lastLogin']?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Competitions</h2>

            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive clearfix">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Event</th>
                            <th># Judges</th>
                            <th>Entries</th>
                            <th>Scores Entered</th>
                            <th># Criteria</th>
                            <th>Registration Fields</th>
                        </tr>
                        <tbody>
                        </thead>
                        <?php
                        foreach ($competitions as $c) :
                            $eventName  = empty($c->Event) ? "<em>None</em>" : __E($c->Event->name);
                            $judgeCount = empty($c->Event) ? 0 : $c->Event->JudgeCount;
                            ?>
                        <tr>
                            <td><a href="<?php echo Yii::app()->params['baseurl']?>/settings/statistics/competition/<?php echo $c->id?>"><?php echo __E($c['name'])?></a></td>
                            <td><?php echo $eventName?></td>
                            <td><?php echo $judgeCount?></td>
                            <td><?php echo $c['EntryCount']?></td>
                            <td><?php echo $c['ScoreCount']?> of <?php echo ($judgeCount * $c['CriteriaCount'] * $c['EntryCount']) ?></td>
                            <td><?php echo $c['CriteriaCount']?></td>
                            <td><?php echo $c['FieldCount']?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
