<?php
// @var $this StatisticsController
?>
<h1>User Data <?php echo __E($user['firstName'].' '.$user['lastName']) ?></h1>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Competitions</h2>

            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive clearfix">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Event</th>
                            <th>Scores entered</th>
                            <th>Score percentile</th>
                            <th>Score deviation</th>
                        </tr>
                        <tbody>
                        </thead>
                        <?php

                        $round = Yii::app()->config->get('round');
                        if (empty($round)) {
                            $round = 2;
                        }

                        foreach ($scores as $s) :?>
                            <tr>
                                <td><a href="<?php echo Yii::app()->params['baseurl']?>/settings/statistics/userc/<?php echo $user['id']?>/<?php echo $s['id']?>"><?php echo __E($s['cname'])?></a></td>
                                <td><?php echo __E($s['ename'])?></td>
                                <td><?php echo $s['scores']?> (of <?php echo $s['criteria']?>)</td>
                                <td><?php echo round($s['percentage'], $round)?></td>
                                <td><?php echo round($s['stddev'], $round)?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

