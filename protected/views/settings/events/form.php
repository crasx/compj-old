<?php
$this->renderPartial('//formtools');

$errors = $event->getErrors();
$this->renderPartial('//generate/messages');
?>

<form action="?" method="post" id="eventForm" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="mb-4 card-title">Event Information</h4>
                    <div class="card-text">
                        <div class="form-floating mb-4">
                            <?php $this->renderPartial('//generate/input/text', ['formName' => 'event', 'name' => 'name', 'title' => 'Event Name', 'description' => '', 'val' => $event->name, 'errors' => $errors]); ?>
                        </div>
                        <div class="form-floating mb-3">
                            <?php $this->renderPartial('//generate/input/text', ['formName' => 'event', 'name' => 'weight', 'title' => 'Sort Order', 'description' => 'Controls where this competition will show when listed with other competitsions. Sorts from small to large', 'val' => $event->weight, 'errors' => $errors, 'type' => 'number']); ?>
                        </div>
                        <div class="form-floating">
                            <?php $this->renderPartial('//generate/input/textarea', ['formName' => 'event', 'name' => 'description', 'title' => 'Description', 'val' => $event->description, 'errors' => $errors]); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <h3 class="h4">Status</h3>
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-input"
                               id="event_registration" <?php echo $event->registration ? 'checked' : '' ?>
                               name="event[registration]">
                        <label for="event_registration" class="form-check-label">Registration Enabled</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-input"
                               id="event_mc" <?php echo $event->mc ? 'checked' : '' ?>
                               name="event[mc]">
                        <label for="event_mc" class="form-check-label">MC Accessible</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-input"
                               id="event_judging" <?php echo $event->judge ? 'checked' : '' ?>
                               name="event[judge]">
                        <label for="event_judging" class="form-check-label">Judging Allowed</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox"
                               class="form-check-input"
                               id="event_results" <?php echo $event->results ? 'checked' : '' ?>
                               name="event[results]">
                        <label for="event_results" class="form-check-label">Results Visible</label>
                    </div>
                </div>
                <div class="form-actions card-body text-center">
                    <input type="submit" class="btn btn-success" value="Submit">
                    <a class="btn btn-link" href="<?= Yii::app()->params['baseurl'] . '/settings/events' ?>">Cancel</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="mb-4 card-title">User Roles</h4>


                    <div class="main-box">
                        <header class="main-box-header clearfix">
                            <h2>Users</h2>
                        </header>
                        <div class="main-box-body clearfix">
                            <div class="form-group" id="userFinderDiv">
                                <div class="input-group">
                                    <div class="form-floating">
                                        <input id="userFinder" class="form-control" placeholder="email@example.com"
                                               type="email">
                                        <label for="userFinder">Add user by email</label>
                                    </div>
                                    <button type="button" class="btn btn-primary">Search</button>
                                </div>
                                <span class="help-block"></span>
                            </div>

                            <div class="table-responsive">
                                <table class="fancy fullwidth table" id="usertable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Roles</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>


                            Legend:
                            <span class="badge rounded-pill bg-info">MC</span>
                            <span class="badge rounded-pill bg-primary">Registration</span>
                            <span class="badge rounded-pill bg-success">Judge</span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <div class="main-box">
                <header class="main-box-header clearfix">
                    <h2>Competitions</h2>
                </header>
                <div class="main-box-body clearfix">
                    <div class="">
                        <table class="table fancy" id="competitionTable">
                            <thead class="d-none">
                            <tr>
                                <th></th>
                                <th>Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($event->Competitions as $c) : ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions/edit/<?php echo $c->id ?>"
                                           target="_blank"
                                           class="table-link">
                                                <span class="fa-stack">
                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                    <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                </span>
                                        </a>
                                    </td>
                                    <td>
                                        <?php echo __E($c->name); ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="main-box-body clearfix">
                    <div class="alert alert-info fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                        </button>
                        <i class="fa fa-info-circle fa-fw fa-lg"></i>
                        You can add more competitions from the <strong><a
                                    href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions"
                                    target="_blank">competition
                                edit</a></strong> page
                    </div>
                </div>
            </div>
        </div>


    </div>

</form>

<div class="modal fade" id="userPermissionsModal" tabindex="-1" role="dialog"
     aria-labelledby="userPermissionModal-title"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userPermissionModal-title">Permissions for <em class="name"></em></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?php
                foreach ($this->getAssignablePermissions() as $permissionId => $permissionData) :

                    ?>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" value="<?php echo $permissionId ?>"
                               id="modal_permission_<?= $permissionId ?>"
                               data-badge="<?= $permissionData['badge'] ?>" data-permission="<?= $permissionId ?>"
                               data-title="<?php echo __EF($permissionData['title']) ?>"
                               data-row-id=""
                        >

                        <label for="modal_permission_<?= $permissionId ?>"
                               class="form-check-label"><?php echo __E($permissionData['title']) ?></label>
                    </div>
                <?php
                endforeach;
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-button" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-button" data-bs-dismiss="modal">
                    Save changes
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="d-none">
    <div id="tpl-user-name">
        <input type="hidden" name="event[users][0][name]" value="">
        <input type="hidden" name="event[users][0][uid]" value="">
        <div class="user-name"></div>

    </div>
    <div id="tpl-user-actions">
        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown"
                        aria-expanded="false">Actions
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#" class="dropdown-item btn edit-button" data-user="">Edit</a>
                    </li>
                    <li>
                        <a class="dropdown-item btn-danger remove-button" data-user=""> Delete</a>

                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div id="tpl-user-permissions">
        <span class="badge rounded-pill"></span>
        <input type="hidden" name="event[permissions][{id}[]" value="">
    </div>
</div>
<script type="text/javascript">


    var emails = <?php echo $this->listUsedEmails()?>;

    var userAssignablePermissions = <?php echo json_encode($this->getAssignablePermissions()) ?>;
    var userAssignments = <?php echo json_encode($this->getAssignedPermissions($event)) ?>;

</script>
