<h1>Edit Event <em><?= __E($event->name) ?></em></h1>

    <?php
    $this->renderPartial('//settings/events/form', ["event" => $event]);
