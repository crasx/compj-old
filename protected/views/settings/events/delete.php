<h1>Delete Events</h1>

<form action="?" method="post">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="main-box">
                <header class="main-box-header clearfix">
                    <h2>Delete</h2>
                </header>
                <div class="main-box-body clearfix">
                    <p>Are you sure you want to delete event <?php echo __E($event->name) ?>?</p>
                    <input type=hidden name="event[confirm]" value="1"/>
                </div>
            </div>
        </div>
    </div>
    <div class="main-box-body clearfix text-center">
        <br/>
        <input type="submit" class="btn btn-danger" value="Delete">
        <a class="btn btn-light" href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events">Cancel</a>
    </div>
</form>
