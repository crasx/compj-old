<?php
$name = __E($user->firstName).' '.__E($user->lastName);
$id = $user->id;

?>
<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-primary dropdown-toggle"
                data-bs-toggle="dropdown" aria-expanded="false">
            Actions
        </button>
        <ul class="dropdown-menu">
            <li>
                <a href="#" class="dropdown-item btn edit-button" data-user="<?= $id ?>">Edit</a>
            </li>
            <li>
                <a class="dropdown-item btn-danger remove-button" data-user="<?= $id ?>"> Delete</a>

            </li>
        </ul>
    </div>
</div>

