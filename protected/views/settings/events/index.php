<?php
$this->renderPartial('//generate/messages');
?>
<h1>Events</h1>
<div class="description">An event is a group of competitions. Events allows you to control access to and the status of
    multiple competitions at once. An example of an event would be "Saturday night competitions"
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">

                <div class="filter-block pull-right">
                    <div class="form-group pull-left">
                        <input type="text" class="form-control dt-search" placeholder="Search...">
                        <i class="fa fa-search search-icon"></i>
                    </div>

                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events/add"
                       class="btn btn-primary pull-right">
                        <i class="fa fa-plus-circle fa-lg"></i> Add event
                    </a>
                </div>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive">

                    <table class="table table-striped table-bordered" id="eventtable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Registration</th>
                            <th>MC</th>
                            <th>Judging</th>
                            <th>Results</th>
                            <th>Sort order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($events as $e) : ?>
                            <tr data-event="<?=$e->id?>">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                Actions
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>

                                                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events/edit/<?php echo $e->id ?>"
                                                       class="dropdown-item btn">
                                                        Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events/clone/<?php echo $e->id ?>"
                                                       class="dropdown-item btn">Clone</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events/delete/<?php echo $e->id ?>"
                                                       class="dropdown-item  btn danger">
                                                        Delete
                                                    </a>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                </td>
                                <td><?php echo __E($e->name) ?><br/> <em><?php echo __E($e->description) ?></em></td>
                                <td>
                                    <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => 'events-ajax', 'val' => 'registration', 'name' => $e->id, 'checked' => $e->registration]) ?>
                                </td>
                                <td>
                                    <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => 'events-ajax', 'val' => 'mc', 'name' => $e->id, 'checked' => $e->mc]) ?>
                                </td>
                                <td>
                                    <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => 'events-ajax', 'val' => 'judge', 'name' => $e->id, 'checked' => $e->judge]) ?>
                                </td>
                                <td>
                                    <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => 'events-ajax', 'val' => 'results', 'name' => $e->id, 'checked' => $e->results]) ?>
                                </td>
                                <td><?php echo $e->weight ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
