<h1>Clone Event</h1>

<form action="?" method="post">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="main-box">
                <header class="main-box-header clearfix mb-4">
                    <h2>Clone <?php echo __E($event->name) ?></h2>
                    <div class="form-text fst-italic mb-3>">This will create a copy of
                        <em><?php echo __E($event->name) ?></em>. You can control how this event is cloned using the
                        options below.
                    </div>
                </header>
                <div class="main-box-body clearfix">
                    <?php if ($event->hasErrors()): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php foreach ($event->getErrors() as $attribute => $errors): ?>
                                    <?php foreach ($errors as $error): ?>
                                        <li><?php echo $error ?></li>
                                    <?php endforeach ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif ?>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" checked value="1" id="cloneCompetitions"
                               name="event[cloneOptions][cloneCompetitions]">
                        <label class="form-check-label" for="cloneCompetitions">Clone competitions</label>
                        <div class="form-text fst-italic">This will create a copy of all competitions within this event
                            and assign them to the new event. Text replacement rules will apply to the cloned
                            competitions.
                        </div>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" checked value="1" id="cloneUsers"
                               name="event[cloneOptions][cloneUsers]">
                        <label class="form-check-label" for="cloneUsers">Clone permissions</label>
                        <div class="form-text fst-italic">This will copy user permissions to the new event. Judges, MCs,
                            and Admin access will be preserved.
                        </div>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="checkbox" value="1" id="cloneStatusFields"
                               name="event[cloneOptions][cloneStatusFields]">
                        <label class="form-check-label" for="cloneStatusFields">Clone event statuses</label>
                        <div class="form-text fst-italic">This will copy the event statuses to the new event. If the
                            event has judging enabled, the new event will also have judging enabled..
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="form-check">
                            <input checked class="form-check-input" type="checkbox" value="1" id="cloneRegistrations"
                                   name="event[cloneOptions][replaceEnabled]"/>
                            <label class="form-check-label" for="cloneRegistrations">Replace text in title and
                                descriptions </label>
                        </div>
                    </div>
                    <div class="replace-fields mb-3">
                        <div class="input-group">
                            <label class="input-group-text" for="cloneReplaceSource">Replace</label>
                            <input type="text" class="form-control" id="cloneReplaceSource"
                                   name="event[cloneOptions][replaceText]"/>
                            <label class="input-group-text" for="cloneReplaceWith">With</label>
                            <input type="text" class="form-control" id="cloneReplaceWith"
                                   name="event[cloneOptions][replaceWith]"/>
                        </div>
                    </div>
                    <div class="weighte-fields">
                        <div class="input-group">
                            <div class="form-check">
                                <input checked class="form-check-input" type="checkbox" value="1" id="weightAdjustment"
                                       name="event[cloneOptions][weightAdjust]"/>
                                <label class="form-check-label" for="weightAdjustment">Adjust weights</label>
                                <div class="form-text fst-italic">This will adjust the weights of cloned events and competitions by a fixed amount. "Lighter" weights float to the top of the list, and "heavier" weights sink to the bottom. This value is usually a negative number.
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <label class="input-group-text" for="weightAdjustmentAmount">Adjustment Amount</label>
                            <input type="number" class="form-control" id="weightAdjustmentAmount"  value="-10" name="event[cloneOptions][weightAdjustmentAmount]"/>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="main-box-body clearfix text-center">
        <br/>
        <input type="submit" class="btn btn-success" name="confirm" value="Submit">
        <a class="btn btn-danger" href="<?php echo Yii::app()->params['baseurl'] ?>/settings/events">Cancel</a>
    </div>
</form>