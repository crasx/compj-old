<?php
$this->renderPartial('//generate/messages');
?>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Competitions</h2>

                <div class="filter-block pull-right">
                    <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions/add"
                       class="btn btn-primary pull-right">
                        <i class="fa fa-plus-circle fa-lg"></i> Add Competition
                    </a>
                </div>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="competitionTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Criteria<br /><a href="#" class="small text-capitalize" onclick="expandAll('criteria')">Expand all</th>
                            <th>Registration fields<br /><a href="#" class="small text-capitalize" onclick="expandAll('reg')">Expand all</th>
                            <th>Contestants</th>
                            <th>Event Order</th>
                            <th>Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($competitions as $c) : ?>
                            <tr>
                                <td>
                                    <div class="btn-group" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                    data-bs-toggle="dropdown" aria-expanded="false">
                                                Actions
                                            </button>
                                            <ul class="dropdown-menu">
                                               <li>
                                                <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions/edit/<?php echo $c->id ?>"
                                                       class="dropdown-item btn">Edit</a>
                                                </li>
                                                <li>

                                                <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions/clone/<?php echo $c->id ?>"
                                                       class="dropdown-item btn">
                                                        Clone
                                                    </a>
                                                </li>
                                                <li>
                                                <a href="<?php echo Yii::app()->params['baseurl'] ?>/settings/competitions/delete/<?php echo $c->id ?>"
                                                       class="dropdown-item  btn danger">
                                                        Delete
                                                    </a>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </td>
                                <td><?php echo __E($c->name) ?>
                                    <div class="form-text">
                                    <?php echo $c->Event != null ? __E($c->Event->name) : "<em>none</em>" ?>
                                    </div>
                                </td>
                                <td><?php $this->renderPartial("//settings/competitions/typeDisplay", ["obj" => $c]) ?></td>
                                <td><?php $this->renderPartial("//settings/competitions/fieldDisplay", ["obj" => $c]) ?></td>
                                <td><?php echo __E(sizeof($c->Entries)) ?></td>
                                <td><?php echo $c->Event != null ? __E($c->Event->weight) : "<em>none</em>" ?></td>
                                <td><?php echo $c->weight ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>