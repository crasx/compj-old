<?php
$this->renderPartial('//formtools');

$errors = $competition->getErrors();
$this->renderPartial('//generate/messages');
?>

<form action="?"  id="competitionForm" method="post">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <h2 class="card-title h4 mb-3">Competition Information</h2>
                    <div class="form-floating mb-4">
                        <?php $this->renderPartial('//generate/input/text', ['formName' => 'competition', 'name' => 'name', 'title' => 'Competition Name', 'description' => '', 'val' => $competition->name, 'errors' => $errors]); ?>
                    </div>
                    <div class="form-floating mb-4">
                        <?php $this->renderPartial('//generate/input/textarea', ['formName' => 'competition', 'name' => 'description', 'title' => 'Description', 'description' => '', 'val' => $competition->description, 'errors' => $errors]); ?>
                    </div>
                    <div class="form-floating mb-4">
                        <?php $this->renderPartial('//generate/input/text', ['formName' => 'competition', 'name' => 'weight', 'title' => 'Sort Order', 'description' => 'Controls where this competition will show when listed with other competitions. Sorts from small to large', 'val' => $competition->weight, 'errors' => $errors, 'type' => 'number']); ?>
                    </div>
                    <div class="form-floating mb-4">
                        <?php $this->renderPartial('//generate/input/select', ['formName' => 'competition', 'name' => 'event', 'title' => 'Associated Event',
                            'description' => 'Events are used to group competitions together. If you do not see the event you want, you may need to create it first.',
                            'val' => $competition->event, 'options' => $competition->getSelectableEvents(), 'errors' => $errors]); ?>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <h2 class="card-title h4 mb-3">Criteria</h2>
                    <div class="form-text mb-4 fst-italic">Criteria are used to judge an entry. You can add as many
                        criteria as you like. Each criteria has a min, max, and increment (or "step") value. These
                        values are normalized and averaged to calculate the overall score of an entry.
                    </div>
                    <?php
                    if (isset($errors['criteria'])) {
                        foreach ($errors['criteria'] as $e) {
                            printError($e);
                        }
                    }
                    ?>


                    <a href="#" id="addCriteriaButton" class="btn btn-primary pull-right">
                        <i class="fa fa-plus-circle fa-lg"></i> Add Criteria
                    </a>

                    <table class="table fancy" id="criteriaTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Min</th>
                            <th>Max</th>
                            <th>Sort Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $selectedCriteria = $competition->getSelectedCriteria();
                        foreach ($selectedCriteria as $f) :
                            $c = $f->Criteria;
                            ?>

                            <tr>
                                <td>
                                    <div class="btn-group">
                                                <span class="fa-stack">
                                            <a href="#" onclick="removeThisRow(this);return false;"
                                               class="table-link danger">
                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                </span>
                                        </a>
                                    </div>
                                </td>
                                <td><?php echo __E($c->name) ?></td>
                                <td><?php echo __E($c->min) ?></td>
                                <td><?php echo __E($c->max) ?></td>
                                <td><?php echo $this->renderPartial('//generate/input/weight', ['formName' => 'criteria[weight]', 'name' => $c->id, 'errors' => $errors, 'val' => $f->weight]) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="card mb-4">
                <div class="card-body">
                    <h2 class="card-title h4 mb-3">Registration Fields</h2>
                    <div class="form-text mb-4 fst-italic">An entry will have these fields when registered. You can add
                        as
                        many fields as you like. Each field's visibility and required status are controlled on a
                        per-competition basis.
                    </div>
                    <?php
                    if (isset($errors['registration'])) {
                        foreach ($errors['registration'] as $e) {
                            printError($e);
                        }
                    }
                    ?>


                        <a href="#" id="addRegistrationButton" class="btn btn-primary float-right">
                            <i class="fa fa-plus-circle fa-lg"></i> Add Field
                        </a>
                    <div class="table-responsive">
                        <table class="table fancy" id="registrationTable">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Required</th>
                                <th>MC Visible</th>
                                <th>Judge Visible</th>
                                <th>Report Visible</th>
                                <th>Sort Order</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $fields = $competition->getSelectedRegistrationFields();
                            foreach ($fields as $s) :
                                $f = $s->Field;
                                ?>
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" onclick="removeThisRow(this);return false;"
                                               class="table-link danger">
                                                                <span class="fa-stack">
                                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                                </span>
                                            </a>
                                        </div>
                                    </td>
                                    <td><?php echo __E($f['name']) ?> -
                                        <em><?php echo __E($f->Type->name) ?></em><br/><em><?php echo __E($f['description']) ?></em>
                                    </td>
                                    <td>
                                        <div class="form-check mb-3 form-switch">
                                            <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => "registration[{$s->field}]", 'name' => 'required', 'val' => $f['id'], 'checked' => $s['required']]) ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check mb-3 form-switch">
                                            <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => "registration[{$s->field}]", 'name' => 'mc', 'val' => $f['id'], 'checked' => $s['mc']]) ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check mb-3 form-switch">
                                            <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => "registration[{$s->field}]", 'name' => 'judge', 'val' => $f['id'], 'checked' => $s['judge']]) ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check mb-3 form-switch">
                                            <?php echo $this->renderPartial('//generate/input/checkbox', ['formName' => "registration[{$s->field}]", 'name' => 'report', 'val' => $f['id'], 'checked' => $s['report']]) ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <?php echo $this->renderPartial('//generate/input/weight', ['formName' => "registration[{$s->field}]", 'name' => 'weight', 'errors' => $errors, 'val' => $s['weight']]) ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="main-box-body clearfix text-center">
        <br/>
        <input type="submit" class="btn btn-success" value="Submit">
    </div>

</form>


<div class="modal fade" id="criteriaModal" tabindex="-1" role="dialog" aria-labelledby="criteriamodal-title"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="criteriamodal-title">Add Criteria</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success submit">Add</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="createCriteriaModal" tabindex="-1" role="dialog" aria-labelledby="createcriteriamodal-title"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createcriteriamodal-title">Create Criteria</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default cancel">Cancel</button>
                <button type="button" class="btn btn-success submit"
                ">Add</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationmodal-title"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="registrationmodal-title">Add Registration Fields</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success submit">Add</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="createRegistrationModal" tabindex="-1" role="dialog"
     aria-labelledby="createregistrationmodal-title"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="createregistrationmodal-title">Create Registration Field</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cancel">Cancel</button>
                <button type="button" class="btn btn-success submit"
                ">Add</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="d-none" id="removeButtons">

    <div class="btn-group">
        <a href="#" class="table-link danger" onclick="removeThisRow(this);return false;">
                                                <span class="fa-stack">
                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                    <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                </span>
        </a>
    </div>
</div>