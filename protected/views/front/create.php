<?php
/*
    @var $this FrontController */
// @var $model Clients
$this->breadcrumbs = [
    'Clients' => ['index'], 'Create'
];

$this->menu = [
    [
        'label' => 'List Clients',
        'url'   => ['index'],
    ],
    [
        'label' => 'Manage Clients',
        'url'   => ['admin'],
    ],
];
?>

<h1>Create Clients</h1>

<?php $this->renderPartial('_form', ['model' => $model]);
