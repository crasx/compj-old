<?php
/*
    @var $this FrontController */
// @var $model Clients
$this->breadcrumbs = [
    'Clients' => ['index'], $model->id
];

$this->menu = [
    [
        'label' => 'List Clients',
        'url'   => ['index'],
    ],
    [
        'label' => 'Create Clients',
        'url'   => ['create'],
    ],
    [
        'label' => 'Update Clients',
        'url'   => [
            'update', 'id' => $model->id
        ],
    ],
    [
        'label'       => 'Delete Clients',
        'url'         => '#',
        'linkOptions' => [
            'submit'  => [
                'delete', 'id' => $model->id
            ],
            'confirm' => 'Are you sure you want to delete this item?',
        ],
    ],
    [
        'label' => 'Manage Clients',
        'url'   => ['admin'],
    ],
];
?>

<h1>View Clients #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', ['data' => $model, 'attributes' => ['id', 'path', 'owner', 'license']]);
