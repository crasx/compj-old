<?php
/*
    @var $this FrontController */
// @var $model Clients
$this->breadcrumbs = [
    'Clients'  => ['index'],
    $model->id => [
        'view', 'id' => $model->id
    ], 'Update'
];

$this->menu = [
    [
        'label' => 'List Clients',
        'url'   => ['index'],
    ],
    [
        'label' => 'Create Clients',
        'url'   => ['create'],
    ],
    [
        'label' => 'View Clients',
        'url'   => [
            'view', 'id' => $model->id
        ],
    ],
    [
        'label' => 'Manage Clients',
        'url'   => ['admin'],
    ],
];
?>

<h1>Update Clients <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', ['model' => $model]);
