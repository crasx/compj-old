<?php
/*
    @var $this FrontController */
// @var $dataProvider CActiveDataProvider
$this->breadcrumbs = ['Clients'];

$this->menu = [
    [
        'label' => 'Create Clients',
        'url' => ['create'],
    ],
    [
        'label' => 'Manage Clients',
        'url' => ['admin'],
    ],
];
?>

Select an event <br />
<div class="list-group border-4 border home-event-list">
    <?php
    $this->renderPartial('//generate/messages');
    $i = 0;
    $listed = [];
    foreach ([...$owned, ...$partof] as $p) {

        if (in_array($p->id, $listed)) {
            continue;
        }

        $listed[] = $p->id;
        $name = '';

        if (!empty($p->Name?->value)) {
            $name = __E(unserialize($p->Name->value));
        } else {
            $name = '<em>Unnamed</em>';
        }

        // Check if the user owns this event
        if (!empty(array_filter($owned, fn($comp) => $comp->id == $p->id))) {
            $name .= ' *';
        }

        echo '<a class="py-4 list-group-item list-group-item-action list-group-item-'
            . ($i % 2 ? 'dark' : 'light') . '" title="' . $name
            . '" href="/' . urlencode((string)$p->path) . '">' . $name . '</a>';
        $i++;
    }

    ?>
</div>

<div class="row">
    <div class="col-xs-9">
        <div class="checkbox-nice">
            Logged in as <?php echo __E(Yii::app()->user->getName()); ?>
        </div>
    </div>
    <a href="/?logout=1" class="col-xs-3">
        Logout
    </a>
    <br/>
    <br/>
    <?php if (sizeof($owned)) {
        echo '<p class="small text-right">* You own these</p>';
    } ?>
</div>