<?php
/*
    @var $this FrontController */
/*
    @var $model Clients */
// @var $form CActiveForm
?>

<div class="form">

<?php $form = $this->beginWidget(
    'CActiveForm',
    [
        'id'                   => 'clients-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ]
); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'path'); ?>
        <?php echo $form->textField($model, 'path', ['size' => 32, 'maxlength' => 32]); ?>
        <?php echo $form->error($model, 'path'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'owner'); ?>
        <?php echo $form->textField($model, 'owner'); ?>
        <?php echo $form->error($model, 'owner'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'license'); ?>
        <?php echo $form->textField($model, 'license'); ?>
        <?php echo $form->error($model, 'license'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->