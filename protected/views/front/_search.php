<?php
/*
    @var $this FrontController */
/*
    @var $model Clients */
// @var $form CActiveForm
?>

<div class="wide form">

<?php $form = $this->beginWidget('CActiveForm', ['action' => Yii::app()->createUrl($this->route), 'method' => 'get']); ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id', ['size' => 20, 'maxlength' => 20]); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'path'); ?>
        <?php echo $form->textField($model, 'path', ['size' => 32, 'maxlength' => 32]); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'owner'); ?>
        <?php echo $form->textField($model, 'owner'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'license'); ?>
        <?php echo $form->textField($model, 'license'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->