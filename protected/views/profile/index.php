<h1>Profile</h1>

<?php $this->renderPartial('//formtools');
if (isset($error)) {
    printMessageError($error);
}
?>

<form action="?" method="post" >
<fieldset>
    <legend>Basic information</legend>
    
    <p><label>Username:</label> <?php echo __E($user->username)?> </p>
    <p><label>Email:</label> <input class="sf" name="user[email]" type="text" value="<?php echo __EF($user->email)?>"> <?php printError($user->getError('email'))?>
    <p><label>Current Password:</label> <input class="sf" name="user[current_password]" type="password" value=""><?php printError($user->getError('current_password'))?></p>
    <p><label>New Password:</label> <input class="sf" name="user[password][0]" type="password" value="">    
<?php printError($user->getError('password'))?> <?php printDescription(__T('Leave password blank to keep the same'))?></p>
    <p><label>Verify password:</label> <input class="sf" name="user[password][1]" type="password" value=""><?php printError($user->getError('password2'))?> </p>
</fieldset>
    <p><input class="button" type="submit" value="Submit"> <a class="button_link" href="<?php echo CLIENT?>/">Cancel</a></p>
