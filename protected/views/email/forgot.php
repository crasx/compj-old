<?php echo __E($name) ?>,

A request to reset the password for your account has been made at <?php echo Yii::app()->params['name'] ?>.

You may now log in by clicking this link or copying and pasting it to your browser:


<a href="<?php echo Yii::app()->getBaseUrl(1); ?><?php echo Yii::app()->createUrl('front/restore', ['hash' => $hash]);?>"><?php echo Yii::app()->getBaseUrl(1); ?><?php echo Yii::app()->createUrl('front/restore', ['hash' => $hash]);?></a>

This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it's not used.

--  <?php echo Yii::app()->params['name'] ?> team