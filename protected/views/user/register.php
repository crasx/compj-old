<form role="form" method="post">
<h2>Create Account</h2>
            <?php
            $errors   = $user->getErrors();
            $name_err = isset($errors['firstName']) || isset($errors['lastName']);
            $pass_err = isset($errors['password']) || isset($errors['password2']);
            $this->renderPartial("//formtools");


            $this->renderPartial('//generate/messages');

            ?>



        <div class="form-group <?php echo $name_err ? "has-error" : "" ?>" >
            <input class="form-control" id="firstName" type="text" placeholder="First Name" name="firstName" value="<?php echo __EF($user->firstName)?>">
            <?php if (!empty($errors['firstName'])) {
                printFormElementErrors($errors['firstName']);
            } ?>
            <input class="form-control" type="text" placeholder="Last Name" name="lastName" value="<?php echo __EF($user->lastName)?>">
            <?php if (!empty($errors['lastName'])) {
                printFormElementErrors($errors['lastName']);
            } ?>
        </div>
<br />
        <div class="form-group <?php echo isset($errors['email']) ? "has-error" : "" ?>"  >
            <input class="form-control" id="email" type="email" placeholder="Email address" name="email"  value="<?php echo __EF($user->email)?>" >
            <?php if (!empty($errors['email'])) {
                printFormElementErrors($errors['email']);
            } ?>
        </div>
        <br />
        <div class="form-group  <?php echo $pass_err ? "has-error" : "" ?>">
            <input type="password" id="password" class="form-control" placeholder="Password" name="password" >
            <?php if (!empty($errors['password'])) {
                printFormElementErrors($errors['password']);
            } ?>
            <input type="password" class="form-control" placeholder="Repeat Password" name="password2" >
            <?php if (!empty($errors['passwword2'])) {
                printFormElementErrors($errors['password2']);
            } ?>
        </div>

        <div id="remember-me-wrapper">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox-nice">
                        <input type="checkbox" name="tos" value="1" checked="checked" />
                        <label for="remember-me">
                            I agree to the <a href="/tos">TOS</a>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-success col-xs-12">Register</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="register-text"><a href="/user/login">Login to existing account</a></p>
            </div>
        </div>

    </form>
