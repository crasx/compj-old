<form role="form" action="/user/login" method="post">
    <div class="d-flex flex-column pt-3">
        <h2>Login</h2>

        <?php $this->renderPartial('//generate/messages'); ?>

        <div class="input-group mb-3">
            <span class="input-group-text">
                <i class="fa fa-paper-plane"></i>
            </span>
            <div class="form-floating">
                <input class="form-control" name="loginForm[email]" id="login-email"
                       type="email" value="<?php echo __EF(($_POST['loginForm']['email'] ?? '')) ?>">
                <label for="login-email">Email Address</label>
            </div>
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text">
                <i class="fa fa-key"></i>
            </span>
            <div class="form-floating">
                <input type="password" name="loginForm[password]" class="form-control" id="login-password">
                <label for="login-password">Password</label>
            </div>
        </div>

        <div class="row">
            <div class="col"><!--
                    <div class="checkbox-nice">
                        <input type="checkbox" id="remember-me" />
                        <label for="remember-me">
                            Remember me
                        </label>
                    </div>-->
                <button type="submit" class="btn btn-success btn-lg w-100">Login</button>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-6">
                <a href="/user/forgot" id="login-forget-link" class="btn w-100">
                    Forgot password?
                </a>
            </div>
            <div class="col-6"><a href="/user/register" class="w-100 btn btn-link">Need an account?</a></div>
        </div>
    </div>
    <input type="hidden" value="<?php echo $_SESSION['fkey'] ?>" name="fkey"/>
</form>
