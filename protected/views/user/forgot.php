<form role="form" action="/user/forgot" method="post" id="forgot-form">
    <h2>Reset Password</h2>
    <?php
    // Yii::app()->user->setFlash("error", 'test123');
    $this->renderPartial('//generate/messages');

    ?>
    <p>Enter your email address and we will send you a link to reset your password.</p>
    <div class="input-group">
        <span class="input-group-text">
            <i class="fa fa-paper-plane"></i>
        </span>
        <div class="form-floating">
            <label class="visually-hidden" for="email">Password</label>
            <input class="form-control" name="forgot[email]" id="email" type="text" placeholder="Email address">

        </div>
    </div>

    <div class="row">
        <div class="col">
            <button
                    data-sitekey="<?php echo Yii::app()->params['recaptcha_public'] ?>"
                    type="submit"
                    class="btn btn-success w-100 btn-large g-recaptcha"
                    data-callback="onSubmit"
                >Submit</button>
            <script type="text/javascript">
                function onSubmit(token) {
                    document.getElementById("forgot-form").submit();
                }
            </script>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/" class="mt-3 btn w-100" ><i class="fa fa-arrow-left"></i> Return To Login</a>
        </div>
    </div>
</form>
