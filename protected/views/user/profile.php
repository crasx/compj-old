<form role="form" method="post" class="container">
    <div class="row">
        <div class="col">
            <h2>Update Account</h2>

            <?php
            $name_err = isset($errors['firstName']) || isset($errors['lastName']);
            $pass_err = isset($errors['password']) || isset($errors['password2']);
            $this->renderPartial("//formtools");
            $this->renderPartial('//generate/messages');
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-floating mb-3 <?php echo getInputClass($errors['firstName'] ?? []); ?>">
                <input class="form-control" id="firstName" type="text" placeholder="First Name" name="profile[firstName]"
                       value="<?php echo __EF($user->firstName) ?>" required>
                <label for="firstName">First Name</label>
                <?php echo printFormElementErrors($errors['firstName'] ?? []); ?>
            </div>
        </div>
        <div class="col">
            <div class="form-floating mb-3 <?php echo getInputClass($errors['lastName'] ?? []) ?>">
                <input class="form-control" type="text" placeholder="Last Name" name="profile[lastName]"
                       value="<?php echo __EF($user->lastName) ?>" required>
                <label for="lastName">Last Name</label>
                <?php echo printFormElementErrors($errors['lastName'] ?? []); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-floating mb-3 <?php echo getInputClass($errors['email'] ?? []) ?>">
                <input class="form-control" id="email" type="email" placeholder="Email address" name="profile[email]"
                       value="<?php echo __EF($user->email) ?>" required>
                <label for="email">Email address</label>
            </div>
            <?php if (!empty($errors['email'])) {
                printFormElementErrors($errors['email']);
            } ?>
            <div class="form-floating mb-3 <?php echo getInputClass($errors['passwordCurrent'] ?? []) ?>">
                <input type="password" id="password" class="form-control" placeholder="Current Password"
                       name="profile[passwordCurrent]">
                <label for="passwordCurrent">Current Password</label>
                <div class="form-text">
                    Leave this blank to keep your current password.
                </div>
            </div>
            <?php if (!empty($errors['passwordCurrent'])) {
                printFormElementErrors($errors['passwordCurrent']);
            } ?>
            <div class="form-floating mb-3 <?php echo getInputClass($errors['password'] ?? []) ?>">
                <input type="password" class="form-control" placeholder="New Password" name="profile[password]">
                <label for="password">New Password</label>
            </div>
            <?php if (!empty($errors['password'])) {
                printFormElementErrors($errors['password']);
            } ?>
            <div class="form-floating mb-3 <?php echo getInputClass($errors['password2'] ?? []) ?>">
                <input type="password" class="form-control" placeholder="Repeat New Password" name="profile[password2]">
                <label for="password2">Repeat New Password</label>
            </div>
            <?php if (!empty($errors['password2'])) {
                printFormElementErrors($errors['password2']);
            } ?>
            <div class="row mt-3">
                <div class="col-6">
                    <button type="submit" class="btn btn-success w-100">Update</button>
                </div>
                <div class="col-6">
                    <a href="/user/profile" class="btn btn-link w-100">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</form>
