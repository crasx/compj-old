<form role="form" action="?<?php if(!empty($_GET['hash'])){echo "hash=".urlencode((string) $_GET['hash']);} ?>" method="post">
    <h2>Restore Password</h2>
    <?php
    //            Yii::app()->user->setFlash("error", 'test123');
    $this->renderPartial('//generate/messages');

    $pass_err=isset($errors['password']) || isset($errors['password2']);
    $this->renderPartial("//formtools");

    ?>
    <em>For security purposes please re-enter your email</em>
    <br />
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
        <input class="form-control" name="restore[email]" type="text" placeholder="Email address">
        <input type="hidden" value="<?php echo !empty($_GET['hash'])?__EF($_GET['hash']):''?>" name="restore[hash]" />
        <br />
    </div>
    <div class="input-group" style="width:100%">
        <div class="form-group  <?=$pass_err?"has-error":"" ?>">
            
            <input type="password" id="password" class="form-control" placeholder="Password" name="restore[password]" >
            <?php if(!empty($errors['password'])) printFormElementErrors($errors['password']); ?>
            <input type="password" class="form-control" placeholder="Repeat Password" name="restore[password2]" >
            <?php if(!empty($errors['passwword2'])) printFormElementErrors($errors['password2']); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="g-recaptcha" data-sitekey="<?php echo Yii::app()->params['recaptcha_public']?>"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success col-xs-12">Submit</button>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="register-text"><a href="/">Login</a></p>
            <p class="register-text"><a href="/register">Need an account?</a></p>
        </div>
    </div>
</form>
