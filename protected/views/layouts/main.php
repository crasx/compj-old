<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name=”apple-mobile-web-app-capable” content=”yes”/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>


    <?php
    $favicon = Yii::app()->config->get('favicon');
    if (empty($favicon)) {
        $favicon = '/favicon.ico';
    }

    ?>
    <link rel="icon" href="<?php echo $favicon ?>" type='image/x-icon'>
    <link rel="shortcut icon" href="<?php echo $favicon ?>" type='image/x-icon'>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-73061275-2', 'auto');
        ga('send', 'pageview');

    </script>

</head>

<body>

<div class="container-fluid g-0">
    <div class="row px-3" id="eyebrowNav">
        <div class="col">
            <button type=button class="ps-2 menu-toggle h-100 border-0 bg-transparent collapsed" data-bs-toggle="collapse"
                    data-bs-target="#sidebarNav"
                    aria-controls="sidebarNav" aria-expanded="true" aria-label="Expand navigation">
                <i class="fa fa-bars open-button"></i>
                <i class="fa fa-close close-button"></i>
                <span class="visually-hidden">Collapse navigation</span>
            </button>
        </div>
        <div class="col d-flex justify-content-end">
            <ul class="nav">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                       aria-expanded="false">
                        <i class="fa fa-user"></i>
                        <span class="visually-hidden">User</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li>
                            <!-- user profile button -->
                            <a href="/user/profile" class="dropdown-item">
                                <i class="fa fa-user fa-flip-horizontal"></i>
                                <span class="">User Profile</span>
                            </a>
                        </li>
                        <li>
                            <!-- logout button -->
                            <a href="/?logout=1" class="dropdown-item">
                                <i class="fa fa-sign-out fa-flip-horizontal"></i>
                                <span class="">Logout</span>
                            </a>

                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <!-- logo -->
                    <a href="/" title="Dashboard">
                        <img class="logo" src="<?php echo Yii::app()->params['logo'] ?>" alt="Competitiom Judge Logo"/>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="position-absolute collapse" id="sidebarNav">
        <?php $this->renderPartial('//generate/menu'); ?>
    </nav>
    <div class="row">
        <div class="col">
            <div class="container-xxl">
                <div class="row">
                    <div class="col-12 py-3">
                        <?php $this->renderPartial('//generate/breadcrumbs'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="row">

        <div id="powered-by">Powered by Competition Judge &copy; 2008-<?php echo date("Y"); ?><br/>
        </div>
    </footer>


</div>
</body>
</html>
