<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!--[if lt IE 9]>
    <script src="/js/theme/html5shiv.js"></script>
    <script src="/js/theme/respond.min.js"></script>
    <![endif]-->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-26951244-8', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body id="login-page" class="single-column-layout container-fluid">
<div class="container m-auto mt-4">
    <div class="row justify-content-center">
        <div class="col col-md-9 login-box">
            <h1>
                <img src="/css/assets/logologin.png" alt="Competition Judge"/>
            </h1>
            <div class="p-2">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>