<?php

if ($results == null) {
    fputcsv($outstream, ["No results available"], ',', '"');
    return;
}

$i = 1;

foreach ($results as $r) :
    if (!empty($faddfields)) {
        $entry = Entry::model()->with("Fields")->findByPk($r[3]);
    }

    $o = [
        $i++,
        $r[0],
        $r[1],
        $r[2],
    ];
    foreach ($faddfields as $field) {
        if (!empty($entry->Fields[$field])) {
            $o[] = $entry->Fields[$field]->value;
        }
    }

    fputcsv($outstream, $o, ',', '"');
endforeach;
