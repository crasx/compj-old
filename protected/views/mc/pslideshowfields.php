<h2>Click field to start slideshow</h2>
<?php
foreach ($fieldData as $fid => $field) {
    ?>
    <div class="slideshow_field">
        <a id="field-<?php echo $fid ?>" class="field_name"><?php echo __E($field['name']); ?></a>
        <script type="text/javascript">
            function openGallery<?php echo $fid ?>(open) {
                slideshowspeed = 5000;
                $gallery<?php echo $fid ?> = $("a[rel=field-<?php echo $fid ?>]").colorbox({
                    slideshow: true,
                    photo: true, slideshowSpeed: slideshowspeed,
                    title: function () {
                        var t = $(this).attr('alt');
                        return t;
                    },
                    close: '',
                    loop: false,
                    open: open,
                    current: '{current}',
                    previous: '',
                    next: '',
                    onComplete: function () {
                        var c = parseInt($("#cboxCurrent").text());
                        var t = $("#list-<?php echo $fid ?> li").length;
                        if (c == t) {

                            setTimeout(function () {
                                getFieldList(function () {
                                    openGallery<?php echo $fid ?>(true);
                                });
                            }, slideshowspeed);
                        }
                    }

                });

            }

            $("a#field-<?php echo $fid ?>").click(function (e) {
                openGallery<?php echo $fid ?>(true);
            });
        </script>
        <ul class='entries' id="list-<?php echo $fid ?>" style='display: none'>
            <?php
            foreach ($field['images'] as $eid => $image) {
                echo '<li>';
                echo '<a alt="'.__EF($image['name']).'<br/>'.implode(', ', $image['competitions']);
                echo '" rel="field-'.$fid.'" href="'.$this->renderPartial('//mc/fields/uimage-ss', ['value' => $image['image']], true).'">';
                echo __E($image['name']).'</a>';
                echo '</li>';
            }
            ?>
        </ul>
    </div>
    <?php
}//end foreach
?>


<?php
if (empty($fieldData)) :
    echo '<em>No fields available</em>';
endif;