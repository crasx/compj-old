<h1><?php echo __E(Yii::app()->config->get("name")) ?> Mc panel</h1>
<?php
  $b = Yii::app()->params['baseurl']."/mc";

    $m = [
        [
            'label' => 'Lineup',
            'url'   => $b."/lineup",
            'icon'  => ASSETSDIR.'icons/dashboard/44.png',
            'title' => 'Lineup',
        ],
        [
            'label' => 'Results',
            'url'   => $b."/results",
            'icon'  => ASSETSDIR.'icons/dashboard/111.png',
            'title' => 'Results',
        ],
        [
            'label' => 'Report',
            'url'   => $b."/report",
            'icon'  => ASSETSDIR.'icons/dashboard/111.png',
            'title' => 'Report',
        ],
    ];
    // <li class="current"><a href="#" class="dashboard"><img src="/css/".VERSION."/assets/icons/small_icons_3/dashboard.png" alt="" /><span class="current">Dashboard</span></a></li>
    ?> 
    <ul class="dash">
<?php
foreach ($m as $mm) {
    ?>
        <li class="fade_hover tooltip" title="<?php echo $mm['title']?>" >
            <a href="<?php echo $mm['url']?>" class="dashboard">
                <img src="<?php echo $mm['icon']?>" alt="" />
                <span><?php echo $mm['label']?></span>
            </a>
        </li>
            <?php
}
?>
    </ul>