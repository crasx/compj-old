<?php
$this->renderPartial('//generate/messages');
?>
<h1><?php echo __E(Yii::app()->config->get("name")) ?> Slideshow</h1>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Lineup</h2>

                <div class="filter-block pull-right">
                    <a href="#" onclick="mcList.ajax.reload(); return false" class="btn btn-primary pull-right">
                        <i class="fa fa-refresh fa-lg"></i> Reload
                    </a>
                </div>

                <br clear="both"/>
                <form class="form-inline" role="form">

                    <div class="form-group">
                        <label for="competitionList">Select competition:</label>
                        <?php
                        $events = $this->getEventList();

                        ?>
                    </div>
                    <select id="competitionList" class="form-control">
                        <option value="-1"></option>
                        <option value="-2">All</option>
                        <?php
                        foreach ($events as $eid => $e) :
                            ?>
                        <optgroup label="<?php echo __EF($e['name']) ?>">
                            <?php foreach ($e['competitions'] as $id => $c) : ?>
                            <option value="<?php echo $id ?>"><?php echo __E($c['name']) ?> (<?php echo $c['count']?>)</option>
                            <?php endforeach; ?>
                            </optgroup><?php
                        endforeach;
                        ?>
                    </select>
                </form>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive" id="fieldList">
                </div>
            </div>
        </div>
    </div>
</div>