
<div class="form-check form-switch">
    <input class="form-check-input status-checkbox" type="checkbox" id="<?= $fieldId . '-' . $id ?>" <?= ($checked ?? FALSE)  ? 'checked' : ''?> <?= ($disabled ?? FALSE)  ? 'disabled' : ''?> data-id="<?= $id ?>" data-entry="<?= $entry ?>" data-competition="<?= $competition ?>" >
    <label class="form-check-label visually-hidden" for="<?= $fieldId . '-' . $id ?>"><?= $label ?></label>
    <i class="fa fa-refresh fa-lg fa-spin d-none"></i>
    <span class="invalid-feedback d-none">Error</span>
</div>
