<?php

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false);
header("Content-Type: application/octet-stream");
header("Content-Type: text");
header("Content-Disposition: attachment; filename=\"export.csv\";");
header("Content-Transfer-Encoding: binary");

$outstream = fopen("php://output", 'w');


$addfields = [];
if (!empty($_GET['report']) && is_array($_GET['report'])) {
    $addfields = $_GET['report'];
}

fputcsv($outstream, [Yii::app()->config->get("name")." Report"], ',', '"');

fputcsv($outstream, [''], ',', '"');
// blank row
foreach ($events as $event) {
    foreach ($event['competitions'] as $id => $comp) {
        fputcsv($outstream, [''], ',', '"');
        // blank row
        fputcsv($outstream, [$event['name'], $comp->name], ',', '"');
        $afields    = [
            'Place',
            'Id',
            'Name',
            'Score',
        ];
        $faddfields = [];
        foreach ($comp->Fields_mc as $field) {
            if (in_array($field->field, $addfields)) {
                $faddfields[] = $field->field;
                $afields[]    = __E($field->Field->name);
            }
        }

        fputcsv($outstream, $afields, ',', '"');
        // blank row
        $this->renderPartial('presults_csv', ['results' => $comp->calculateResults(1), 'outstream' => $outstream, 'faddfields' => $faddfields]);
    }//end foreach
}//end foreach

fclose($outstream);
die();
exit;
Yii::app()->end();
throw new Exception("HH");
