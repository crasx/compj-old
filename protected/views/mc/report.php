<?php
$this->renderPartial('//generate/messages');
$dl = [];
if (!empty($_GET['report']) && is_array($_GET['report'])) {
    $dl['report'] = $_GET['report'];
}

$dl['csv'] = 1;
$dl        = http_build_query($dl);
?>

<h1><?php echo __E(Yii::app()->config->get("name")) ?> Report</h1>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">

                <form class="form-inline" role="form">
                    <div class="form-group">
                        <label for="numrecords">Show top:</label>
                        <select id="numrecords" class="form-control"
                                onchange="javascript:updatePage()">

                            <?php
                            $entryselects = [
                                1,
                                3,
                                5,
                                10,
                                20,
                                25,
                                50,
                                100,
                                'all',
                            ];
                            foreach ($entryselects as $i) :
                                ?>
                                <option
                                    value="<?php echo $i ?>" <?php echo ($i == $entries ? "selected=selected" : "") ?> ><?php echo $i ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>

                    <div class="filter-block pull-right">
                        <a href="#fieldList" id="addFields" class="btn btn-success pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> Set Fields
                        </a>
                        <a href="<?php echo Yii::app()->params['baseurl']?>/mc/report?<?php echo $dl?>"  class="btn btn-primary pull-right">
                            <i class="fa fa-download fa-lg"></i> Download as CSV
                        </a>
                    </div>
                </form>


            </header>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <?php
        $fields = [];
    foreach ($events as $event) {
        ?>
        <div class="col-lg-12 col-md-12">
            <div class="main-box clearfix">
                <header class="main-box-header clearfix">
                    <h2 class="pull-left"><?php echo __E($event['name']) ?></h2>
                </header>
                <?php
                foreach ($event['competitions'] as $id => $comp) {
                    $this->renderPartial('presults', ['results' => $comp->calculateResults(), 'cid' => $id, 'limit' => $entries, 'name' => $comp->name, 'fields' => $comp->Fields_mc, 'comp' => $comp]);
                    foreach ($comp->Fields_mc as $field) {
                        $fields[$field->field] = [
                            $field->Field->name,
                            $field->Field->description,
                        ];
                    }
                }

                ?>
            </div>
        </div>
        <?php
    }//end foreach
    ?>
</div>
<div class="hidden">
    <div id="fieldList" style="min-width:250px;">
        <?php

        foreach ($fields as $id => $data) :
            $name  = $data[0];
            $descr = $data[1];

            $checked = !empty($_GET['report']) && is_array($_GET['report']) && in_array($id, $_GET['report']);
            ?>
            <div class="checkbox-nice">
                <input type="checkbox" name="report[]"
                       id="report_<?php echo $id ?>" <?php echo $checked ? 'checked="checked"' : '' ?>  value="<?php echo $id?>">
                <label for="report_<?php echo $id ?>"><?php echo __E($name) ?> <?php echo !empty($descr) ? "<br /><em>".__E($descr)."</em>" : "" ?>
                </label>
            </div>
        <?php endforeach; ?>
        <br />
        <a onclick="updatePage()" href="#" class="btn btn-success pull-right">
            <i class="fa fa-plus-circle fa-lg"></i> Set Fields
        </a>

    </div>
    <div id="contestantInfo"></div>

</div>
