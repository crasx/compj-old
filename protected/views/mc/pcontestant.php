<dl class="contestantInfo">
    <dt>Name</dt>
    <dd><?php echo __E($entry->Entry->name); ?> (<?php echo $entry->id ?>)</dd>
    <?php
    foreach ($rows as $row) {
        if (!empty($row['value'])) {
            echo $this->renderPartial('//judge/fields/'.$row['ftype'], $row, true);
        }
    }
    ?>
</dl>