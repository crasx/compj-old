<?php
$this->renderPartial('//generate/messages');
?>
<h1><?php echo __E(Yii::app()->config->get("name")) ?> Lineup</h1>
<a id="list"></a>

<div class="row">
    <div class="col-lg-6 col-sm-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="mb-3">Contestants</h2>
                <span class="fsti-talic">Select a competition from the dropdown to view the contestants in that competition.
                    Note that contestants will not if they have withdrawn from the competition. These records can be viewed using the "Show inactive" checkbox.
                </span>

                <form class="form-inline" role="form">
                    <div class="container">
                        <div class="row mt-3 mb-3">
                            <div class="col">
                                <div class="form-floating mb-4">
                                    <?php $this->renderPartial("//generate/competitionDD", ["events" => $this->getEventList(), "id" => "competitionList"]); ?>
                                    <label for="competitionList">Select competition:</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-end">
                                <input type="checkbox" id="show-withdrawn">
                                <label for="show-withdrawn">
                                    Show Inactive
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table id="MCTable" class="fancy table"
                           data-url="<?php echo Yii::app()->params['baseurl'] ?>/mc/lineupCallback">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>In Queue</th>
                            <th>Called Onstage</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4">
                                <a href="#" title="Last updated" class="btn btn-link pull-right w-100"
                                   id="lineup-refresh">
                                    <i class="fa fa-refresh fa-lg me-2"></i> <span class="updated text-italic"></span>
                                </a>
                            </td>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-12">
        <div class="main-box clearfix">
            <a name="info" id="info"></a>
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Contestant Info</h2>
            </header>

            <div class="main-box-body clearfix" id="contestantInfo">

            </div>
        </div>
    </div>
</div>