<?php
$addfields = [];
if (!empty($_GET['report'])) {
    $addfields = $_GET['report'];
}

$faddfields = [];
?>
<div class="main-box-body clearfix">
    <div class="table-responsive">
        <h4><?php echo __E($name) ?></h4>
        <table class="fancy fullwidth dataTable">
            <thead>
                <tr>
                    <th>Place</th>
                    <th>Name</th>
                    <th>Score</th>
                    <?php
                    foreach ($renderedFields as $field) {
                        if (in_array($field->field, $addfields)) {
                            $faddfields[] = $field->field;
                            echo '<th>'.__E($field->Field->name).'</th>';
                        }
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $i     = 1;
                $round = Yii::app()->config->get('round');
                if (empty($round)) {
                    $round = 2;
                }

                foreach ($results as $r) :
                    if (!empty($faddfields)) {
                        $entry = Entry::model()->with("Fields")->findByPk($r[3]);
                    }

                    if ($limit != 'all' && $i > $limit) {
                        break;
                    }
                    ?>

                    <tr>
                        <td><?php echo $i++ ?></td>
                        <td><?php echo $r[1] ?></td>
                        <td><?php echo round($r[2], $round); ?></td>
                        <?php
                        foreach ($faddfields as $field) {
                            if (!empty($entry->Fields[$field])) {
                                echo '<td>'.__E($entry->Fields[$field]->value).'</td>';
                            }
                        }
                        ?>
                    </tr>
                    <?php
                endforeach;
                ?>
            </tbody>
        </table>

    </div>
</div>