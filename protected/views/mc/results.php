<?php
$this->renderPartial('//generate/messages');
?>
<h1><?php echo __E(Yii::app()->config->get("name")) ?> Results</h1>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Results</h2>

                <div class="filter-block pull-right">
                    <a href="#" onclick="mcList.ajax.reload(); return false" class="btn btn-primary pull-right">
                        <i class="fa fa-refresh fa-lg"></i> Reload
                    </a>
                </div>

                <br clear="both"/>
                <form class="form-inline" role="form">

                    <div class="form-group">
                        <label for="competitionList">Select competition:</label>
                        <?php $this->renderPartial("//generate/competitionDD", ["events" => $this->getEventList(true), "id" => "competitionList"]); ?>
                    </div>
                </form>
            </header>

            <div class="main-box-body clearfix">
                <div class="table-responsive">
                    <table id="MCTable" class="fancy fullwidth dataTable table-striped table-bordered" data-url="<?php echo Yii::app()->params['baseurl'] ?>/mc/presults">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left">Contestant Info</h2>
            </header>

            <div class="main-box-body clearfix" id="contestantInfo">

            </div>
        </div>
    </div>
</div>