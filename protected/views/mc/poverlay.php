<div class="attributes"><?php
foreach ($attributes as $a) {
    echo "<p><span class='name ".$a['type']."' >".__E($a['name']).'</span>: ';
    $this->renderPartial("//fields/display/".$a['type'], ["value" => $a['value']]);
    echo "</p>";
}

?></div>