
<?php
$this->pageTitle = Yii::app()->name.' - Error';
?>
<div class="container-md justify-content-center">
    <div class="w-60">
            <h1>Error <?php echo $code; ?></h1>
            <div class="error">
                <?php echo CHtml::encode($message); ?>
    <a href="/"><h2>Home</h2></a><a href="#" onclick="history.go(-1)"><h2>Back</h2></a>
            </div>
        </div>
</div>
