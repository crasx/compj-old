<?php
class ProfileController extends ControllerBase
{


    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => [
                'class'     => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ],
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'    => ['class' => 'CViewAction'],
        ];

    }//end actions()


    public function accessRules()
    {
        return [
            'deny', 'users' => ['*']
        ];

    }//end accessRules()


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', [
            'user' => Yii::app()->user,
            'errors' => [],
        ]);

    }//end actionIndex()


}//end class
