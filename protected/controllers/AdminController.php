<?php

class AdminController extends ControllerBase
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/none';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];

    }//end filters()


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            // allow authenticated user to perform 'create' and 'update' actions
                'actions' => [
                    'create',
                    'update',
                    'delete',
                    'admin',
                    'view',
                ],
                'expression' => '$user->isgod',
            ],
            [
                'deny',
            // deny all users
                'users' => ['*'],
            ],
        ];

    }//end accessRules()

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param  integer $id the ID of the model to be loaded
     * @return Clients the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Clients::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;

    }//end loadModel()


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Clients;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            if ($model->save()) {
                $this->redirect($model->path);
            }
        }

        $this->render('create', ['model' => $model]);

    }//end actionCreate()


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            if ($model->save()) {
                $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $this->render('update', ['model' => $model]);

    }//end actionUpdate()


    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     *
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(($_POST['returnUrl'] ?? ['admin']));
        }

    }//end actionDelete()


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (Yii::app()->user->getIsGuest()) {
            header('location: /user/login');
            exit();
            $owned = $partof = [];
        } else if (Yii::app()->user->isgod) {
            $owned  = (array) Clients::model()->findAll();
            $partof = [];
        } else {
            $owned  = (array) Clients::model()->findAllByAttributes(['owner' => Yii::app()->user->uid]);
            $partof = (Yii::app()->user->getUser()?->Clients ?? [])   ;
        }

        if ((sizeof($owned) + sizeof($partof)) == 1) {
            if (sizeof($partof) == 0) {
                header('location: /'.urlencode((string) $owned[0]->path));
            } else {
                header('location: /'.urlencode((string) $partof[0]->path));
            }
        }

        $this->layout = '//layouts/user';

        $this->render('index', ['owned' => $owned, 'partof' => $partof]);

    }//end actionIndex()


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Clients('search');
        $model->unsetAttributes();
        // clear any default values
        if (isset($_GET['Clients'])) {
            $model->attributes = $_GET['Clients'];
        }

        $this->render('admin', ['model' => $model]);

    }//end actionAdmin()


    /**
     * Performs the AJAX validation.
     *
     * @param Clients $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'clients-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

    }//end performAjaxValidation()


}//end class
