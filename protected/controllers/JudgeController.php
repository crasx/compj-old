<?php

class JudgeController extends ControllerBase
{

    private ?array $_events = null;

    private $_entry = null;


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'sliders',
                    'contestants',
                    'update',
                    'image',
                    'competitions',
                    'contestantData',
                ],
                'roles'   => ['judge'],
            ],
        ];

    }//end _actionRules()


    public function actionImage()
    {
        if (!isset($_GET['i'])) {
            return;
        }

        $command = Yii::app()->db->createCommand('SELECT * from uploads where client=:client and id=:id ');

        $client = Yii::app()->params['clientid'];
        $command->bindParam(":client", $client, PDO::PARAM_INT);
        $command->bindParam(":id", $_GET['i'], PDO::PARAM_INT);
        $row = $command->queryRow();
        if ($row) {
            $f = Yii::app()->basePath.$row['file'];
            if (!(file_exists($f) && realpath($f) == $f)) {
            }

            if (str_ends_with((string) $f, 'jpg')) {
                header('Content-Type: image/jpeg');
                echo file_get_contents($f);
            } else if (str_ends_with((string) $f, 'png')) {
                header('Content-Type: image/png');
                echo file_get_contents($f);
            }

            die();
        }

    }//end actionImage()

    public function beforeAction($action)
    {

        $r = parent::beforeAction($action);
        $this->breadcrumbs[] = [
            'label' => 'Judge',
            'url'   => Yii::app()->params['baseurl'] . '/judgge',
        ];

        return $r;

    }

    public function actionContestants()
    {
        $this->layout = "//layouts/none";
        $comp         = (int) $_GET['c'];
        $competition  = $this->getCompetition($comp);
        echo $this->renderPartial('contestants', ["competition" => $competition]);
        die();

    }//end actionContestants()


    private function getCompetition($comp)
    {

        $command = Yii::app()->db->createCommand(
            'SELECT count(*)
        FROM user_events ue
        join events e on e.id=ue.event
        join competitions c on e.id=c.event
         where e.client=:client
         and ue.user=:u
         and ue.permission = "judge"
         and c.id = :cid
         and e.judge=1
         '
        );

        $uid      = Yii::app()->user->getUID();
        $clientid = Yii::app()->params['clientid'];

        $command->bindParam(":client", $clientid, PDO::PARAM_INT);
        $command->bindParam(":cid", $comp, PDO::PARAM_INT);
        $command->bindParam(":u", $uid, PDO::PARAM_INT);

        $cnt = $command->queryScalar();
        if ($cnt == 0) {
            return null;
        }

        return Competitions::model()->with(["CEntries", "CEntries.Entry", "CriteriaCount"])->findByAttributes(['id' => $comp]);

    }//end getCompetition()


    public function actionCompetitions()
    {
        $this->layout = "//layouts/none";
        $result       = [
            'success'      => true,
            'competitions' => [],
        ];
        foreach ($this->getEventList() as $eid => $comps) {
            foreach ($comps['competitions'] as $c => $data) {
                $result['competitions'][] = [
                    'name' => $data['name']." (".$data['count'].")",
                    'id'   => $c,
                ];
            }
        }

        echo json_encode($result, JSON_THROW_ON_ERROR);
        die();

    }//end actionCompetitions()


    public function getEventList()
    {
        $events = [];
        $command = Yii::app()->db->createCommand(
            'SELECT e.id, e.name, c.id cid, c.name cname,
      (select count(*) as cnt from entry_competitions ec where ec.competition=c.id and ec.active=1) as cnt
        FROM user_events ue
        join events e on e.id=ue.event
        join competitions c on e.id=c.event
         where e.client=:client
         and ue.user=:u
         and ue.permission = "judge"
         and e.judge=1
         order by e.weight, c.weight
         '
        );

        $uid = Yii::app()->user->getUID();
        $cid = Yii::app()->params['clientid'];

        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $command->bindParam(":u", $uid, PDO::PARAM_INT);

        $events = $command->queryAll();

        $r = [];
        foreach ($events as $e) {
            if (!isset($r[$e['id']])) {
                $r[$e['id']] = [
                    'name'         => $e['name'],
                    'competitions' => [],
                ];
            }

            $r[$e['id']]['competitions'][$e['cid']] = [
                'name'  => $e['cname'],
                'count' => $e['cnt'],
            ];
        }

        return $r;

    }//end getEventList()


    public function actionIndex()
    {
        ;

        Yii::app()->clientScript->registerScriptFile("/js/theme/jquery.nouislider.min.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile("/js/cj/judge.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile('/css/libs/jquery.nouislider.css');
        $this->render('index');

    }//end actionIndex()


    public function actionContestantData()
    {
        $return       = [
            'success' => false,
            'error'   => '',
            'scores'  => [],
        ];
        $this->layout = "//layouts/none";
        $comp         = (int) $_GET['c'];
        $entry        = (int) $_GET['e'];
        $contestant   = $this->validateEntryAccess($comp, $entry);

        if ($contestant) {
            $return['success'] = true;
            $return['scores'] = $this->getScores($comp, $entry);
            $return['name']   = $contestant['name'].' ('.$contestant['id'].')';
            $return['fields'] = $this->getFields($comp, $entry);
        } else {
            $return['error'] = 'Invalid Entry';
        }

        echo json_encode($return, JSON_THROW_ON_ERROR);
        die();

    }//end actionSliders()


    private function validateEntryAccess($comp, $entry)
    {

        $command = Yii::app()->db->createCommand(
            'SELECT ec.entry, ec.id, en.name
        FROM user_events ue
        join events e on e.id=ue.event
        join competitions c on e.id=c.event
        join entry_competitions ec on ec.competition = c.id
        join entry en on en.id  = ec.entry
         where e.client=:client
         and ue.user=:u
         and ue.permission = "judge"
         and e.judge=1
         and c.id=:comp
         and ec.entry=:entry
         and ec.active=1
         '
        );

        $uid = Yii::app()->user->getUID();
        $cid = Yii::app()->params['clientid'];

        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $command->bindParam(":u", $uid, PDO::PARAM_INT);
        $command->bindParam(":comp", $comp, PDO::PARAM_INT);
        $command->bindParam(":entry", $entry, PDO::PARAM_INT);

        return $command->queryRow();

    }//end validateEntryAccess()


    private function getScores($comp, $entry)
    {
        if ($this->validateEntryAccess($comp, $entry) == null) {
            return [];
        }

        $command = Yii::app()->db->createCommand(
            'SELECT c.name, c.min, c.max, c.step,  s.score, cc.criteria
        FROM competition_criteria cc
        join criteria c on c.id=cc.criteria
        left join scores s on s.user=:u and s.criteria=cc.criteria and s.entry=:entry and s.competition=cc.competition
        where cc.competition = :comp
         order by cc.weight

         '
        );

        $uid = Yii::app()->user->getUID();

        $command->bindParam(":u", $uid, PDO::PARAM_INT);
        $command->bindParam(":comp", $comp, PDO::PARAM_INT);
        $command->bindParam(":entry", $entry, PDO::PARAM_INT);

        return $command->queryAll();

    }//end getScores()


    private function getFields($comp, $entry)
    {
        if (($entryObject = $this->validateEntryAccess($comp, $entry)) == null) {
            return [];
        }

        $command = Yii::app()->db->createCommand(
            'SELECT ef.entry, cf.competition, f.name, f.description, t.validator as ftype, ef.value
        FROM competition_fields cf
        join fields f on f.id=cf.field
        join field_types t on f.type=t.id
        left join entry_fields ef on ef.field = f.id and ef.entry=:entry
        where cf.competition = :comp
        and cf.judge=1
         order by cf.weight

         '
        );

        $command->bindParam(":comp", $comp, PDO::PARAM_INT);
        $command->bindParam(":entry", $entry, PDO::PARAM_INT);

        $rows  = $command->queryAll();
        $data  = '';
        $data .= $this->renderPartial('//judge/fields/text', ['name' => 'Name', 'value' => $entryObject['name']], true);
        foreach ($rows as $row) {
            $data .= $this->renderPartial('//judge/fields/'.$row['ftype'], $row, true);
        }

        return $data;

    }//end getFields()


    public function actionUpdate()
    {
        $result = [
            "success" => false,
            'score'   => 0,
        ];

        $competition = (int) $_GET['comp'];
        $criteria    = (int) $_GET['criteria'];
        $eid         = (int) $_GET['entry'];
        $score       = $_GET['value'];

        $entry = $this->validateEntryAccess($competition, $eid);
        if ($entry) {
            $validCriteria = $this->validateCriteria($competition, $criteria);

            $uid = Yii::app()->user->getUID();
            if ($validCriteria) {
                if ($score == 'C') {
                    $command = Yii::app()->db->createCommand(
                        'delete from scores
                        where criteria=:criteria and user=:user and entry=:entry and competition=:competition
                        '
                    );

                    $command->bindParam(":criteria", $criteria, PDO::PARAM_INT);
                    $command->bindParam(":user", $uid, PDO::PARAM_INT);
                    $command->bindParam(":entry", $eid, PDO::PARAM_INT);
                    $command->bindParam(":competition", $competition, PDO::PARAM_INT);
                    $command->execute();
                    $result['success'] = true;
                } else {
                    $score = (double) $score;

                    $score           = (floor((($score - $validCriteria['min']) / $validCriteria['step'])) * $validCriteria['step'] + $validCriteria['min']);
                    $score           = $score > $validCriteria['max'] ? $validCriteria['max'] : $score;
                    $result['score'] = $score;
                    $command         = Yii::app()->db->createCommand(
                        'insert into scores(criteria, score, user, entry, competition, updated)
                          values(:criteria, :score, :user, :entry, :competition, CURRENT_TIMESTAMP )
                      on duplicate key update score=:score, updated=CURRENT_TIMESTAMP '
                    );

                    $command->bindParam(":criteria", $criteria, PDO::PARAM_INT);
                    $command->bindParam(":user", $uid, PDO::PARAM_INT);
                    $command->bindParam(":score", $score);
                    $command->bindParam(":entry", $eid, PDO::PARAM_INT);
                    $command->bindParam(":competition", $competition, PDO::PARAM_INT);
                    $command->execute();
                    $result['success'] = true;
                }//end if
            }//end if
        }//end if

        die(json_encode($result, JSON_THROW_ON_ERROR));

    }//end actionUpdate()


    private function validateCriteria($competition, $criteria)
    {

        $command = Yii::app()->db->createCommand(
            'SELECT c.min, c.max, c.step
        FROM competition_criteria cc
        join criteria c on cc.criteria=c.id
        where cc.competition=:comp and cc.criteria=:criteria
         '
        );

        $command->bindParam(":comp", $competition, PDO::PARAM_INT);
        $command->bindParam(":criteria", $criteria, PDO::PARAM_INT);

        return $command->queryRow();

    }//end validateCriteria()


}//end class
