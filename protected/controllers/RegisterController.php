<?php

class RegisterController extends AEDController
{

    public $isClone = false;

    /**
     * Setup parent class
     *
     * @param CAction $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->_OBJ = "registration";
        $this->_CLASS = "Entry";
        $this->_NAME = "Registration";
        $this->_NAMEPLURAL = "Registrations";
        $this->_PATH = "register";
        $this->callback = "regCallback";
        $this->ajaxIndex = 1;

        $p = parent::beforeAction($action);
        $this->breadcrumbs[] = [
            'label' => 'Registration',
            'url' => Yii::app()->params['baseurl'] . '/register/',
        ];
        $this->fancybox();

        if ($action->id == 'index') {
            Yii::app()->clientScript->registerScriptFile("/js/cj/registration.index.js", CClientScript::POS_HEAD);
        } else {
            Yii::app()->clientScript->registerScriptFile("/js/cj/registration.js", CClientScript::POS_HEAD);
        }

        return $p;

    }//end beforeAction()


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'edit',
                    'ajaxIdList',
                    'add',
                    'delete',
                    'ajaxIndex',
                    'upload',
                    'image',
                    'setphoto',
                    'photos',
                ],
                'roles' => ['registration'],
            ],
        ];

    }//end _actionRules()


    public function actionPhotos()
    {
        $sortable = [1 => 'date-posted-'];

        $recache = isset($_GET['recache']) && $_GET['recache'] ? true : false;

        $page = 1;
        if (isset($_GET['start']) && isset($_GET['length'])) {
            $s = intval($_GET['start']);
            $l = intval($_GET['length']);
            if ($l > 0 && $s >= 0) {
                $page = (floor($s / $l) + 1);
            }
        }

        $sort = '';
        if (isset($_GET['order']) && is_array($_GET['order'])) {
            for ($i = 0; $i < sizeof($_GET['order']); $i++) {
                $col = intval($_GET['order'][$i]['column']);
                if (array_key_exists($col, $sortable)) {
                    $sort = $sortable[$col] . ($_GET['order'][$i]['dir'] == 'asc' ? 'asc' : 'desc');
                    break;
                }
            }
        }

        $length = 10;

        if (isset($_GET['length']) && is_numeric($_GET['length'])) {
            $length = $_GET['length'];
            if ($length < 1 || $length > 500) {
                $length = 10;
            }
        }

        $search = '';

        if (!empty($_GET['search']) && !empty($_GET['search']['value'])) {
            $search = $_GET['search']['value'];
        }

        $result = $this->getPhotos($page, $length, $sort, $search, $recache);

        $output = [
            "draw" => !empty($_GET['draw']) ? intval($_GET['draw']) : 0,
            "recordsTotal" => $result['total'],
            "recordsFiltered" => $result['total'],
            "data" => [],
        ];

        $adata = addslashes(json_encode(['page' => $page, 'length' => $length, 'sort' => $sort, 'search' => $search], JSON_THROW_ON_ERROR));

        foreach ($result['photo'] as $k => $photo) {
            $action = '<a href=# onclick=\'setMyPhoto("' . addslashes((string)$photo['id']) . '", "' . $adata . '");return false;\'>';
            $output['data'][] = [
                $action . $photo['title'] . '</a>',
                date("m-d-Y H:i:s", $photo['dateupload']),
                $action . '<img src="' . $photo['url_t'] . '" /></a>',
            ];
        }

        echo json_encode($output, JSON_THROW_ON_ERROR);
        // $this->renderPartial('indexRowArray', array('records' => $result['photo'], 'output' => $output));
        die();

    }//end actionPhotos()


    /**
     * Used to communicate with flickr. Pagination is done at flickr level
     * ... I keep typing picasa
     *
     * @param int $page
     * @param int $count
     * @param string $sort
     * @param string $search
     * @param bool $recache
     * @return bool|mixed
     * @throws CException
     */
    private function getPhotos($page = 1, $count = 10, $sort = 'post-date-desc', $search = '', $recache = false)
    {
        Yii::import('ext.phpflickr.phpFlickr', true);

        $cid = Yii::app()->params['clientid'];
        $fid = Yii::app()->config->get('flickr_id');

        $cacheId = "FLICKR_PHOTO_LIST_" . $cid . "_" . $fid . "_" . $page . "_" . $count . "_" . $sort . '_' . $search;
        $xml = Yii::app()->cache->get($cacheId);
        if ($xml === false || $recache) {
            $f = new phpFlickr(Yii::app()->params['flickr_key'], Yii::app()->params['flickr_secret']);
            $photos = $f->photos_search(['user_id' => Yii::app()->config->get("flickr_id"), 'extras' => "date_upload,url_t,url_c", 'sort' => $sort, 'text' => $search, "page" => $page, "per_page" => $count]);
            Yii::app()->cache->set($cacheId, $photos, 30);
        } else {
            $photos = $xml;
        }

        return $photos;

    }//end getPhotos()


    /**
     * Add a new object
     *
     * @return void
     */
    public function actionAdd()
    {

        if (isset($_GET['id']) && empty($_POST[$this->_OBJ])) {
            $id = (int)$_GET['id'];
            $cid = Yii::app()->params['clientid'];
            $obj = $this->_CLASS::model()->findByAttributes(['id' => $id, 'client' => $cid]);
            if ($obj != null) {
                $this->isClone = TRUE;
            }
        }

        $this->breadcrumbs[] = [
            'label' => ($this->isClone ? 'Clone' : 'Add' ) . ' ' . $this->_NAME,
            'url' => Yii::app()->params['baseurl'] . '/' . $this->_PATH . '/add',
        ];

        if (empty($obj)) {
            $obj = new $this->_CLASS('add');
        }

        if ($this->pullForm($obj, $this->_OBJ, 'add')) {
            $obj->save(false);
            if ($this->callback == null) {
                header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH . "/");
            } else {
                $this->{$this->callback}($obj, "added");
            }
            exit();
        } else {
            $this->render('add', [$this->_OBJ => $obj]);
        }

    }//end actionAdd()


    function actionSetphoto()
    {
        $pid = $_POST['url'];
        $params = $_POST['params'];

        $result = [
            'success' => false,
            'error' => 'No Image',
            'id' => 0,
            'src' => '',
        ];
        if (!empty($params) && $params != 'dropbox') {
            $params = json_decode((string)$params, null, 512, JSON_THROW_ON_ERROR);

            if (empty($params->page) || !is_numeric($params->page) || $params->page < 1) {
                $params->page = 1;
            }

            if (empty($params->length) || !is_numeric($params->length) || $params->length < 1) {
                $params->length = 10;
            }

            $dataUri = null;

            if (!empty($pid)) {
                $photos = $this->getPhotos($params->page, $params->length, $params->sort, $params->search, true);
                foreach ($photos['photo'] as $photo) {
                    if ($photo['id'] == $pid) {
                        $image = imagecreatefromjpeg($photo['url_c']);
                        $file = Yii::app()->params['upload_dir'];
                        do {
                            $rk = time() . '_' . random_int(0, 999);
                        } while (file_exists($file . $rk));
                        $file .= $rk . '.jpg';

                        imagejpeg($image, $file);

                        $result['success'] = true;
                        if ($image) {
                            $this->insertUploadRecord($file);
                            $result['id'] = Yii::app()->db->getLastInsertID();
                            $result['src'] = Yii::app()->params['baseurl'] . '/register/image?i=' . $result['id'];
                        }
                    }
                }
            }//end if
        } else {
            if (!empty($params) && $params == 'dropbox') {
                if (str_starts_with((string)$pid, 'https://www.dropbox.com')) {
                    $image = str_replace('bounding_box=75', 'bounding_box=800', (string)$pid);
                    $type = substr(strtolower(pathinfo($image, PATHINFO_EXTENSION)), 0, 3);
                    $file = Yii::app()->params['upload_dir'];
                    do {
                        $rk = time() . '_' . random_int(0, 999);
                    } while (file_exists($file . $rk));
                    $file .= $rk . '.jpg';

                    switch ($type) {
                        case 'png':
                            $file .= '.png';
                            $image = imagecreatefrompng($image);
                            imagepng($image, $file);
                            $result['success'] = true;
                            break;
                        case 'jpg':
                        case 'jpeg':
                            $file .= '.jpg';
                            $image = imagecreatefromjpeg($image);
                            imagejpeg($image, $file);
                            $result['success'] = true;
                            break;
                        default:
                            $result['error'] = 'Invalid Type';
                    }

                    if ($image && $result['success']) {
                        $this->insertUploadRecord($file);
                        $result['id'] = Yii::app()->db->getLastInsertID();
                        $result['src'] = Yii::app()->params['baseurl'] . '/register/image?i=' . $result['id'];
                    }
                } else {
                    $result['error'] = 'Not dropbox link';
                }//end if
            }//end if
        }//end if

        echo json_encode($result, JSON_THROW_ON_ERROR);
        die();

    }//end actionSetphoto()


    /**
     * Add record for uploaded file.
     *
     * @param type $image
     * @global type $user
     */
    private function insertUploadRecord($file)
    {
        $command = Yii::app()->db->createCommand('insert into uploads(client, session, ip, user, file, timestamp) values (:client, :session, :ip, :user, :file, CURRENT_TIMESTAMP )');
        $command->bindParam(':file', $file);
        $sid = session_id();
        $command->bindParam(':session', $sid);
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        $command->bindParam(':ip', $ip);
        $uid = Yii::app()->user->getUID();
        $command->bindParam(':user', $uid);
        $cid = Yii::app()->params['clientid'];
        $command->bindParam(':client', $cid);
        $command->execute();

    }//end insertUploadRecord()


    function actionUpload()
    {
        $inputValue = $_POST['imgData'];
        $dataUri = null;
        $result = [
            'success' => false,
            'error' => 'No Image',
            'id' => 0,
        ];

        if (DataUri::tryParse($inputValue, $dataUri)) {
            // Attempt to decode URI's data
            $data = false;
            if ($dataUri->tryDecodeData($data)) {
                $image = imagecreatefromstring($data);
                $file = '/data/uploads/' . date('Y-m') . '/';
                $destination = Yii::app()->basePath;
                mkdir($destination . $file) || throw new CException('Unable to create upload dir');

                do {
                    $fileName = time() . '_' . random_int(0, 999);
                } while (file_exists($destination . $file . $fileName));
                $destination .= $file . $fileName;
                $type = $dataUri->getMediaType();

                switch ($type) {
                    case 'image/png':
                        $file .= '.png';
                        imagepng($image, $destination);
                        $result['success'] = true;
                        break;
                    case 'image/jpeg':
                        $file .= '.jpg';
                        imagejpeg($image, $destination);
                        $result['success'] = true;
                        break;
                    default:
                        $result['error'] = 'Invalid Type';
                }

                if ($result['success']) {
                    $this->insertUploadRecord($file . $fileName);
                    $result['id'] = Yii::app()->db->getLastInsertID();
                }
            }//end if
        }//end if

        echo json_encode($result, JSON_THROW_ON_ERROR);
        die();

    }//end actionUpload()


    public function actionAjaxIdList()
    {

        $command = Yii::app()->db->createCommand()
            ->select(['c.id cid', '(select max(ec.id) mid from `entry_competitions` `ec` where ec.competition=c.id) as mid '])
            ->from('events e')
            ->where('e.client=:client')
            ->join('competitions c', 'e.id=c.event');

        if (!Yii::app()->user->isOwnerOrGod()) {
            $command->rightJoin('user_events ue', 'e.id=ue.event');
            $command->andWhere('ue.user=:u');
            $command->andWhere('ue.permission  = "registration"');

            $uid = Yii::app()->user->getUID();
            $command->bindParam(":u", $uid, PDO::PARAM_INT);
        }

        $command->andWhere('e.registration=1');

        $cid = Yii::app()->params['clientid'];
        $command->bindParam(":client", $cid, PDO::PARAM_INT);

        $data = $command->queryAll();
        echo json_encode($data, JSON_THROW_ON_ERROR);
        die();

    }//end actionAjaxIdList()


    public function actionImage()
    {
        if (!isset($_GET['i'])) {
            return;
        }

        $command = Yii::app()->db->createCommand('SELECT * from uploads where client=:client and id=:id ');

        $client = Yii::app()->params['clientid'];
        $command->bindParam(":client", $client, PDO::PARAM_INT);
        $command->bindParam(":id", $_GET['i'], PDO::PARAM_INT);
        $row = $command->queryRow();
        if ($row) {
            $f = Yii::app()->basePath .$row['file'];
            if (realpath($f) === false ||  !file_exists($f) || !is_file($f)){
                return;
            }

            if (str_ends_with((string)$f, 'jpg')) {
                header('Content-Type: image/jpeg');
                echo file_get_contents($f);
            } else if (str_ends_with((string)$f, 'png')) {
                header('Content-Type: image/png');
                echo file_get_contents($f);
            }
        }

    }//end actionImage()


    public function actionAjaxIndex()
    {

        $search = parent::pullDTData([1 => "name"], [1 => "name", 3 => "registered", 4 => "updated"]);

        $records = Entry::model()->findAll($search);
        $iFilteredTotal = Entry::model()->count($search);
        $iTotal = Entry::model()
            ->count(['condition' => 'client=' . Yii::app()->params['clientid']]);

        /*
         * Output
         */
        $output = [
            "draw" => !empty($_GET['draw']) ? intval($_GET['draw']) : 0,
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => [],
        ];

        $this->renderPartial('indexRowArray', ['records' => $records, 'output' => $output]);

        die();

    }//end actionAjaxIndex()


    protected function regCallback($obj, $action)
    {
        Yii::app()->user->setFlash('success', __E($obj->name) . ' ' . $action . ' successfully');
        header("Location:" . Yii::app()->params['baseurl'] . "/" . $this->_PATH);

    }//end regCallback()


}//end class
