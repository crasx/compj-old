<?php

class McController extends ControllerBase
{

    final public const TYPE_LINEUP = 0;
    final public const TYPE_RESULTS = 1;
    final public const TYPE_REPORT = 2;
    final public const TYPE_SLIDESHOW = 2;

    const CHECK_TYPE_IN_LINE = 'here';
    const CHECK_TYPE_ON_STAGE = 'called';

    private ?array $_events = null;

    private array $permission = [
        0 => 'mc_lineup',
        1 => 'mc_results',
        2 => 'mc_report',
        3 => 'mc_slideshow',
    ];

    private array $compStatus = [
        0 => 'e.mc = 1',
        1 => 'e.results = 1',
        2 => 'e.results = 1',
    ];


    /**
     * Note to self:
     * access controller does not validate user access to event
     **/

    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'lineup',
                    'lineupCallback',
                    'toggleStatus',
                    'entryInfo',
                    'image',
                    'competitions',
                ],
                'roles' => ['mc_lineup'],
            ],
            [
                'actions' => [
                    'results',
                    'presults',
                    'pcontestant',
                    'image',
                    'competitions',
                ],
                'roles' => ['mc_results'],
            ],
            [
                'actions' => [
                    'report',
                    'preport',
                    'pcontestant',
                    'image',
                    'competitions',
                ],
                'roles' => ['mc_results'],
            ],
            [
                'actions' => [
                    'slideshow',
                    'slideshowFields',
                    'image',
                    'competitions',
                ],
                'roles' => ['mc_slideshow'],
            ],
        ];

    }//end _actionRules()


    /**
     * Setup parent class
     *
     * @param CAction $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $actionResult = parent::beforeAction($action);
        $this->breadcrumbs[] = [
            'label' => 'MC',
            'url' => Yii::app()->params['baseurl'] . '/mc/index',
        ];
        return $actionResult;

    }//end beforeAction()


    /*
        public function actionIndex() {
        $this->render('index');
        }
    */
    public function actionLineup()
    {
        $this->breadcrumbs[] = 'Lineup';

        Yii::app()->clientScript->registerScriptFile("/js/cj/mc.lineup.js", CClientScript::POS_HEAD);

        $this->render('lineup');

    }


    public function actionlineupCallback()
    {
        $this->layout = "//layouts/none";
        $comp = -1;
        if (isset($_GET['c'])) {
            $comp = (int)$_GET['c'];
        }

        $competition = null;
        $compObject = $this->validateCompetitionAccess($comp, self::TYPE_LINEUP);
        if ($compObject) {
            $cid = Yii::app()->params['clientid'];
            $competition = Competitions::model()->with("CEntries", "CEntries.Entry", "Event_mc")->findByAttributes(['id' => $comp, 'client' => $cid]);
            if (!$competition && !empty($comp) && $comp != -1) {
                Yii::app()->user->setFlash('error-mclineup', "Unable to find competition");
            }
        }
        $adata = [];
        foreach ($competition?->CEntries ?? [] as $entry) :
            $data = [];
            if (!$entry->active && !(isset($_GET['w']) && $_GET['w'] == 'true')) {
                continue;
            }

            $data[] = $entry->id;
            $options = [
                'competition' => $entry->competition,
                'entry' => $entry->entry,
                'id' => $entry->id,
                'disabled' => !$entry->active,
            ];
            $data[] = $this->renderPartial('//mc/namelink', $options + ['name' => $entry->Entry->name], TRUE);
            $data[] = $this->renderPartial('//mc/checkbox', $options + ['fieldId' => 'heredcb', 'label' => 'In Line', 'checked' => $entry->here], TRUE);
            $data[] = $this->renderPartial('//mc/checkbox', $options + ['fieldId' => 'calledcb', 'label' => 'Called', 'checked' => $entry->called], TRUE);

            $adata[] = $data;
        endforeach;

        echo json_encode(['data' => $adata], JSON_THROW_ON_ERROR);
        die();

    }

    public function actionSlideshow()
    {
        $this->breadcrumbs[] = 'Slideshow';
        Yii::app()->clientScript->registerScriptFile("/js/theme/colorbox/jquery.colorbox-min.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile('/js/theme/colorbox/colorbox.css');
        Yii::app()->clientScript->registerScriptFile("/js/cj/mc.slideshow.js", CClientScript::POS_HEAD);

        $this->render('slideshow');

    }//end actionSlideshow()


    public function actionSlideshowFields()
    {

        if (isset($_GET['c'])) {
            $comp = (int)$_GET['c'];
        } else {
            $comp = -1;
        }

        $compObject = $this->validateCompetitionAccess($c, self::TYPE_SLIDESHOW);
        if (!$compObject) {
            throw new CHttpException(403, 'You do not have access to that.');
        }

        $fieldData = [];
        /*
         * field id -> images/name -> entry id ->image/competitions[]/name
         */
        $cid = Yii::app()->params['clientid'];

        if ($comp == -2) {
            $competitions = Competitions::model()->with("CEntries", "CEntries.Entry", "CEntries.Entry.Fields", "Event_mc", "Fields_mc")->findAllByAttributes(['client' => $cid]);
        } else {
            $competitions = [];
            $competitions[] = Competitions::model()->with("CEntries", "CEntries.Entry", "CEntries.Entry.Fields", "Event_mc", "Fields_mc")->findByAttributes(['id' => $comp, 'client' => $cid]);
        }

        foreach ($competitions as $competition) {
            foreach ($competition->Fields_mc as $field) {
                // loop through fields
                if ($field->Field->Type->validator == 'uimage') {
                    // if it is an image
                    if (empty($fieldData[$field->field])) {
                        // and it doesn't exist yet
                        $fieldData[$field->field] = [
                            // create it
                            'name' => $field->Field->name,
                            'images' => [],
                        ];
                    }

                    // add contestants to field
                    foreach ($competition->CEntries as $e) {
                        if ($e->here != 1) {
                            continue;
                        }

                        $e = $e->Entry;
                        foreach ($e->Fields as $cfield) {
                            if ($cfield->field == $field->field) {
                                if (empty($cfield->value)) {
                                    continue;
                                }

                                if (empty($fieldData[$field->field]['images'][$e->id])) {
                                    // add competitions to field
                                    $fieldData[$field->field]['images'][$e->id] = [
                                        'image' => $cfield->value,
                                        'competitions' => [],
                                        'name' => $e->name,
                                    ];
                                }

                                $fieldData[$field->field]['images'][$e->id]['competitions'][] = __EF($competition->name);
                            }
                        }
                    }//end foreach
                }//end if
            }//end foreach
        }//end foreach

        echo $this->renderPartial('pslideshowfields', ["fieldData" => $fieldData]);
        die();

    }//end actionSlideshowFields()


    private function translateType($type)
    {
        $type = match ($type) {
            'results' => self::TYPE_RESULTS,
            'report' => self::TYPE_REPORT,
            default => self::TYPE_LINEUP,
        };

        return $type;

    }//end translateType()


    public function actionCompetitions()
    {
        $this->layout = "//layouts/none";
        $result = [
            'success' => true,
            'competitions' => [],
        ];
        $type = $this->translateType($_GET['type']);

        foreach ($this->getEventList($type) as $eid => $comps) {
            foreach ($comps['competitions'] as $c => $data) {
                $result['competitions'][] = [
                    'name' => $data['name'] . " (" . $data['count'] . ")",
                    'id' => $c,
                ];
            }
        }

        echo json_encode($result, JSON_THROW_ON_ERROR);
        die();

    }//end actionCompetitions()


    // TODO: better security
    public function actionImage()
    {
        $i = $_GET['i'];
        str_replace('.png', '', (string)$i);
        if (!isset($i)) {
            return;
        }

        $command = Yii::app()->db->createCommand('SELECT * from uploads where client=:client and id=:id ');

        $client = Yii::app()->params['clientid'];
        $command->bindParam(":client", $client, PDO::PARAM_INT);
        $command->bindParam(":id", $i, PDO::PARAM_INT);
        $row = $command->queryRow();
        if ($row) {
            $f = $row['file'];
            header('Cache-control: max-age=' . (60 * 60 * 24));
            header('Expires: ' . gmdate(DATE_RFC1123, (time() + 60 * 60 * 24)));
            if (str_ends_with((string)$f, 'jpg')) {
                header('Content-Type: image/jpeg');
                echo file_get_contents($f);
            } else if (str_ends_with((string)$f, 'png')) {
                header('Content-Type: image/png');
                echo file_get_contents($f);
            }
        }

    }//end actionImage()


    public function actionToggleStatus()
    {
        $exit = fn($success, $message) => die(json_encode(['success' => $success, 'message' => $message], JSON_THROW_ON_ERROR));

        $entryId = (int)($_GET['entry'] ?? '');
        $entryQueueId = (int)($_GET['id'] ?? '');
        $competitionId = (int)($_GET['competition'] ?? '');

        $type = match ($_GET['type'] ?? '') {
            'here' => self::CHECK_TYPE_IN_LINE,
            'called' => self::CHECK_TYPE_ON_STAGE,
            default => -1,
        };

        $status = match ($_GET['status'] ?? '') {
            'true' => true,
            'false' => false,
            default => null,
        };

        $compObject = $this->validateCompetitionAccess($competitionId, self::TYPE_LINEUP);

        if (!$compObject || $type == -1 || $status === null) {
            $exit(FALSE, 'Unknown error, please try again.');
        }

        $entry = EntryCompetitions::model()->findByAttributes(['competition' => $competitionId, 'id' => $entryQueueId, 'entry' => $entryId]);
        if (!$entry) {
            $exit(FALSE, 'Unknown error, please try again.');
        }

        $entry->$type = ($_GET['status'] ?? '') == 'true';
        $exit($entry->save(), 'Status updated');
    }


    public function actionResults()
    {
        $this->breadcrumbs[] = 'Results';

        Yii::app()->clientScript->registerScriptFile("/js/cj/mc.results.js", CClientScript::POS_HEAD);

        $this->render('results');

    }//end actionResults()



    public function actionEntryInfo()
    {
        $competition = (int)($_GET['competition'] ?? '');
        $entry = (int)($_GET['entry'] ?? '');
        $id = (int)($_GET['id'] ?? '');
        $type = match ($_GET['type'] ?? '') {
            'results' => self::TYPE_RESULTS,
            'report' => self::TYPE_REPORT,
            default => self::TYPE_LINEUP,
        };

        if (($competitionModel = $this->validateCompetitionAccess($competition, $type)) &&
            ($entryModel = EntryCompetitions::model()->findByAttributes(['competition' => $competitionModel, 'entry' => $entry, 'id' => $id]))
        ) {

            $command = Yii::app()->db->createCommand(
                'SELECT ef.entry, cf.competition, f.name, f.description, t.validator as ftype, ef.value
      FROM competition_fields cf
      join fields f on f.id=cf.field
      join field_types t on f.type=t.id
      left join entry_fields ef on ef.field = f.id and ef.entry=:entry
      where cf.competition = :comp
      and cf.mc=1
      order by cf.weight

      '
            );

            $command->bindParam(":comp", $competition, PDO::PARAM_INT);
            $command->bindParam(":entry", $entry, PDO::PARAM_INT);

            $rows = $command->queryAll();

            echo $this->renderPartial('//mc/pcontestant', ['rows' => $rows, 'entry' => $entryModel, 'competition' => $competitionModel]);
        }//end if
        else {
            throw new CHttpException(403, "You do not have access to that");
        }

        die();

    }//end actionPcontestant()


    public function actionPresults()
    {
        $this->layout = "//layouts/none";
        $result = [
            'success' => false,
            'data' => [],
        ];
        $comp = (int)$_GET['c'];
        $compObject = $this->validateCompetitionAccess($comp, self::TYPE_RESULTS);

        if ($compObject) {
            $result['success'] = true;
            $competition = Competitions::model()->findByPk($comp);
            $result['data'] = $competition->calculateResults();
        }

        if (!$result['success']) {
            $result['e'] = 'Invalid Competition';
        }

        echo json_encode($result, JSON_THROW_ON_ERROR);
        // echo $this->renderPartial('presults', array("results" => $results, "cid" => $comp));
        die();

    }//end actionPresults()


    /**
     * Validate competition access
     *
     * @param int $compid The competition to check
     * @param int $type The access level to check
     * @return array|null
     */
    public function validateCompetitionAccess($compid, $type = self::TYPE_LINEUP)
    {
        if (!is_int($compid)) {
            return null;
        }

        $command = Yii::app()->db->createCommand()
            ->select(['c.id cid'])
            ->from('events e')
            ->where('e.client=:client')
            ->andWhere('c.id=:cid')
            ->join('competitions c', 'e.id=c.event');

        if (!Yii::app()->user->isOwnerOrGod()) {
            $command->rightJoin('user_events ue', 'e.id=ue.event');
            $command->andWhere('ue.user=:u');
            $command->andWhere('ue.permission = :perm');

            $uid = Yii::app()->user->getUID();
            $command->bindParam(":u", $uid, PDO::PARAM_INT);
            $command->bindParam(":perm", $this->permission[$type], PDO::PARAM_STR);
        }

        $command->andWhere($this->compStatus[$type]);

        $cid = Yii::app()->params['clientid'];
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $command->bindParam(":cid", $compid, PDO::PARAM_INT);

        return $command->queryRow();

    }//end validateCompetition()


    public function actionReport()
    {
        $rcompetitions = [];

        $entries = ($_GET['l'] ?? 3);
        if ($entries != 'all') {
            $entries = (int)$entries;

            if ($entries <= 0) {
                $entries = 3;
            }
        }

        $events = $this->getEventList(1);;
        $this->fancybox();

        Yii::app()->clientScript->registerScriptFile("/js/cj/mc.report.js", CClientScript::POS_HEAD);

        foreach ($events as $eid => $event) {
            foreach ($event['competitions'] as $cid => $name) {
                $comp = Competitions::model()->with("Fields_mc")->findByPk($cid);
                if ($comp) {
                    $events[$eid]['competitions'][$cid] = $comp;
                } else {
                    unset($events[$eid]['competitions'][$cid]);
                }
            }
        }

        if (isset($_GET['csv'])) {
            $this->renderPartial('report_csv', ["events" => $events, 'entries' => $entries]);
            die();
        } else {
            $this->render('report', ["events" => $events, 'entries' => $entries]);
        }

    }//end actionReport()


    public function getEventList($type = self::TYPE_LINEUP)
    {
        if (isset($this->_events)) {
            return $this->_events;
        }

        $command = Yii::app()->db->createCommand()
            ->select(['e.id', 'e.name', 'c.id cid', 'c.name cname', '(select count(*) as cnt from entry_competitions ec where ec.competition=c.id and ec.active=1) as cnt'])
            ->where('e.client=:client')
            ->order('e.weight, c.weight')
            ->from('events e')
            ->join('competitions c', 'e.id=c.event');

        if (!Yii::app()->user->isOwnerOrGod()) {
            $command->rightJoin('user_events ue', 'e.id=ue.event');
            $command->andWhere('ue.user=:u');
            $command->andWhere('ue.permission = :perm');

            $uid = Yii::app()->user->getUID();
            $command->bindParam(":u", $uid, PDO::PARAM_INT);
            $command->bindParam(":perm", $this->permission[$type], PDO::PARAM_STR);
        }

        $command->andWhere($this->compStatus[$type]);

        $command->order('e.weight, c.weight');

        $cid = Yii::app()->params['clientid'];
        $command->bindParam(":client", $cid, PDO::PARAM_INT);

        $events = $command->queryAll();
        $r = [];
        foreach ($events as $e) {
            if (!isset($r[$e['id']])) {
                $r[$e['id']] = [
                    'name' => $e['name'],
                    'competitions' => [],
                ];
            }

            $r[$e['id']]['competitions'][$e['cid']] = [
                'name' => $e['cname'],
                'count' => $e['cnt'],
            ];
        }

        return $this->_events = $r;

    }//end getEventList()


}//end class
