<?php

class FrontController extends ControllerBase
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/user';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];

    }//end filters()


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                // allow authorized user to logout and view index
                'actions' => [
                    'index',
                ],
                'users' => ['@'],
            ],
            [
                'deny',
                // deny all users
                'users' => ['*'],
            ],
        ];

    }//end accessRules()


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (Yii::app()->user->getIsGuest()) {
            header('location: /user/login');
            exit();
            $owned = $partof = [];
        } else if (Yii::app()->user->isgod) {
            $owned = (array)Clients::model()->findAll();
            $partof = [];
        } else {
            $owned = (array)Clients::model()->findAllByAttributes(['owner' => Yii::app()->user->uid]);
            $partof = (Yii::app()->user->getClients() ?? []);
        }

        if ((sizeof($owned) + sizeof($partof)) == 1) {
            if (sizeof($partof) == 0) {
                header('location: /' . urlencode((string)$owned[0]->path));
            } else {
                header('location: /' . urlencode((string)$partof[0]->path));
            }
        }

        $this->render('index', ['owned' => $owned, 'partof' => $partof]);

    }//end actionIndex()

}//end class
