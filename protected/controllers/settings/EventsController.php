<?php

class EventsController extends AEDController
{


    public function beforeAction($action)
    {

        $this->_OBJ          = "event";
        $this->_NAME         = "Event";
        $this->_NAMEPLURAL   = "Events";
        $this->_CLASS        = "Events";
        $this->_PATH         = "settings/events";
        $this->ajaxIndex     = 0;

        $r = parent::beforeAction($action);
        $this->breadcrumbs[] = [
            'label' => 'Settings',
            'url'   => Yii::app()->params['baseurl'] . '/settings',
        ];
        $this->breadcrumbs[] = [
            'label' => 'Events',
            'url'   => Yii::app()->params['baseurl'] .'/settings/events',
        ];
        Yii::app()->clientScript->registerScriptFile("/js/typeahead.jquery.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile("/js/cj/settings/events.form.js", CClientScript::POS_HEAD);

        return $r;

    }//end beforeAction()


    public function actionIndex()
    {
        Yii::app()->clientScript->registerCssFile('/css/cj/settings/events.css');
        Yii::app()->clientScript->registerScriptFile("/js/cj/settings/events.index.js", CClientScript::POS_HEAD);
        parent::actionIndex();

    }//end actionIndex()


    public function actionAjaxuser()
    {
        if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
            throw new CHttpException('403', 'Forbidden access.');
        }

        $result = ['valid' => false];
        if (isset($_GET['email'])) {
            $user = User::model()
              ->findByAttributes(['email' => $_GET['email']]);
            if ($user) {
                $result['valid']   = true;
                $result['data']['name']    = $user->getName();
                $result['data']['email']   = $user->email;
                $result['data']['uid']      = $user->id;
                $result['data']['permissions'] = [];
            }
        }

        echo json_encode($result, JSON_THROW_ON_ERROR);
        Yii::app()->end();

    }//end actionAjaxuser()


    public function getAssignedPermissions(Events $event)
    {

        if ($event->isNewRecord) {
            return [];
        }

        static $users = [];

        if (isset($users[$event->id])) {
            return $users[$event->id];
        }

        $users[$event->id] = [];

        $eventPermissions = UserEvents::model()->findAllByAttributes(['event' => $event->id]);

        // Build permissions array suitable for json encoding
        foreach ($eventPermissions as $userEvent) {

            // One record per user
            if (!isset($users[$event->id][$userEvent->user])) {
                $users[$event->id][$userEvent->user] = [
                    'name' => $userEvent->User->getName(),
                    'email' => $userEvent->User->email,
                    'user' => $userEvent->user,
                    'permissions' => [],
                ];
            }

            // Add permission to user
            $users[$event->id][$userEvent->user]['permissions'][] = $userEvent->permission;
        }

        return $users[$event->id];

    }

    public function actionAjaxcb()
    {
        $result = [
            'success' => 0,
            'msg'     => '',
        ];

        if (isset($_POST['type']) && isset($_POST['id']) && isset($_POST['checked'])) {
            $type    = $_POST['type'];
            $eid     = $_POST['id'];
            $checked = $_POST['checked'];

            $event = Events::model()->findByAttributes(['id' => $eid, 'client' => Yii::app()->params['clientid']]);
            if ($event) {
                switch ($type) {
                case 'mc':
                case 'registration':
                case 'judge':
                case 'results':
                    $event->setScenario('ajaxcb');
                    $event->$type      = $checked == 'on' ? 1 : 0;
                    $result['success'] = $event->save(false);
                    break;
                default:
                    $result['msg'] = 'Invalid checkbox type';
                }
            } else {
                $result['msg'] = 'Invalid event id';
            }
        }//end if

        die(json_encode($result, JSON_THROW_ON_ERROR));

    }//end actionAjaxcb()


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'ajaxuser',
                    'ajaxcb',
                    'edit',
                    'add',
                    'delete',
                    'clone',
                ],
                'roles'   => ['settings_event'],
            ],
        ];

    }//end _actionRules()


    /**
     * Returns json encoded list of already selected emails
     */
    public function listUsedEmails()
    {
        if (Yii::app()->user->getUID() < 3) {
            $command = Yii::app()->db->createCommand("select DISTINCT email from users u");
        } else {
            $command = Yii::app()->db->createCommand(
                "select DISTINCT email from users u
          join user_events ue on ue.user=u.id
          join events e on ue.event=e.id and e.client=:client
          "
            );
            $cid = Yii::app()->params['clientid'];
            $command->bindParam(":client", $cid, PDO::PARAM_INT);
        }


        $data = $command->queryColumn();

        return json_encode($data, JSON_THROW_ON_ERROR);

    }


    /**
     * Build an array of assignable permissions for quick use
     *
     * @return array
     */
    public static function getAssignablePermissions()
    {
        $allPerms = [];
        foreach (Yii::app()->params['permissions'] as $group => $permissions) {
            if ($group == 'Admin') {
                continue;
            }

            foreach ($permissions as $permissionKey => $permissionTitle) {
                if ($permissionKey[0] == '#') {
                    continue;
                }

                $allPerms[$permissionKey] = [
                    'title' => $permissionTitle,
                    'badge' => $permissions['#badge'],
                    'group' => $group,
                ];
            }
        }

        return $allPerms;

    }//end getPermArray()

}//end class
