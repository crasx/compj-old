<?php

class CriteriaController extends AEDController
{
    /*
        public function actionDelete()
        {
        $this->render('delete');
    }*/


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'edit',
                    'add',
                    'delete',
                ],
                'roles'   => ['settings_competition'],
            ],
        ];

    }//end _actionRules()


    protected function complete($obj, $event)
    {
        echo '<div><div class="'.$event.'">You should not see this</div></div>';
        die();

    }//end complete()


    public function actionIndex()
    {
        $criteria = Competitions::model()->getSelectableCriteria();
        echo $this->renderPartial("index", ['criteria' => $criteria]);
        die();

    }//end actionIndex()


    public function beforeAction($action)
    {

        $this->_OBJ        = "criteria";
        $this->_CLASS      = "Criteria";
        $this->_NAME       = "Criteria";
        $this->_NAMEPLURAL = "Criteria";
        $this->_PATH       = "settings/criteria";
        $this->layout      = '//layouts/none';
        $this->callback    = "complete";
        return  parent::beforeAction($action, false);

    }//end beforeAction()


}//end class
