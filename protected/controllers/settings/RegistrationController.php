<?php

class RegistrationController extends AEDController
{


    public function actionDelete()
    {
           $id = (int) $_GET['id'];
        $field = Fields::model()->findByPk($id);
        if ($field == null) {
            header("Location:".Yii::app()->params['baseurl']."/settings/registration/?message=invalid");
            return;
        }

        if (isset($_POST['registration'])) {
            if ($field->hasValues()) {
                EntryFields::model()->deleteAllByAttributes(["field" => $field->id]);
                CompetitionFields::model()->deleteAllByAttributes(["field" => $field->id]);
            }

            $i = $field->id;
            $field->delete();
            $this->actionIndexO($field, "deleted");
            return;
        }

        $this->render('delete', ["registration" => $field]);

    }//end actionDelete()


    public function actionIndex()
    {
        $fields = Competitions::model()->getSelectableRegistrationFields();
        $this->renderPartial("//settings/registration/index", ['fields' => $fields]);
        die();

    }//end actionIndex()


    protected function complete($obj, $event)
    {
        echo '<div><div class="'.$event.'">You should not see this</div></div>';
        die();

    }//end complete()


    public function beforeAction($action)
    {
        $this->_OBJ        = "registration";
        $this->_CLASS      = "Fields";
        $this->_NAME       = "Registration field";
        $this->_NAMEPLURAL = "Registration fields";
        $this->_PATH       = "settings/registration";
        $this->layout      = '//layouts/none';
        $this->callback    = "complete";
        $this->ajaxIndex   = 0;
        return parent::beforeAction($action, false);

    }//end beforeAction()


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'edit',
                    'add',
                    'delete',
                ],
                'roles'   => ['settings_competition'],
            ],
        ];

    }//end _actionRules()


}//end class
