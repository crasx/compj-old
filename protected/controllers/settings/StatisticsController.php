<?php

class StatisticsController extends ControllerBase
{


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'competition',
                    'data',
                    'user',
                    'judges',
                    'userc',
                ],
                'roles'   => ['settings_competition'],
            ],
        ];

    }//end _actionRules()


    public function beforeAction($action)
    {
        $this->breadcrumbs[] = 'Admin';
        $this->breadcrumbs[] = 'Statistics';

        $r = parent::beforeAction($action);
        ;
        Yii::app()->clientScript->registerScriptFile("/js/cj/settings/competition.js", CClientScript::POS_HEAD);
        return $r;

    }//end beforeAction()


    public function actionCompetition()
    {
        $this->render('competition');

    }//end actionCompetition()


    public function actionUser()
    {
        $this->breadcrumbs[] = 'User';
        $data = [];

        $query   = 'select u.id, u.firstName, u.lastName from users u join user_events ue on ue.user=u.id
join events e on e.id=ue.event and e.client = :client
join competitions c on e.id=c.event
where u.id=:uid
group by u.id';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":uid", $_GET['id'], PDO::PARAM_INT);
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $user = $command->queryRow();

        if (empty($user)) {
            Yii::app()->user->setFlash('error_statistics', 'Invalid user id');
            header("Location: ".Yii::app()->params['baseurl'].'/settings/statistics');
        }

        $data['user'] = $user;

        $query   = 'select c.id, e.name as ename, c.name as cname, count(cr.id) as criteria,
count(s1.score) as scores, avg(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as percentage,
stddev(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as stddev, cc.criteria*ce.entries as criteria
from scores  s1
    join competitions c on c.id=s1.competition
    join events e on e.id=c.event and e.client = :client
	join(select cc1.competition, count(cc1.criteria) as criteria from competition_criteria cc1
         group by cc1.competition
        ) cc on cc.competition=c.id
	join(select ce1.competition, count(ce1.entry) as entries from entry_competitions ce1 where ce1.active=1
         group by ce1.competition
        ) ce on ce.competition=c.id
left join criteria cr on cr.id=s1.criteria

    where s1.user=:uid
    group by s1.competition
    ORDER by  e.weight, c.weight
    ';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":uid", $_GET['id'], PDO::PARAM_INT);
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $scores = $command->queryAll();

        $data['scores'] = $scores;
        $this->render('user', $data);

    }//end actionUser()


    public function actionUserc()
    {
        $this->breadcrumbs[] = 'User Competition';
        $data = [];

        // get/validate current user
        $query   = 'select u.id, u.firstName, u.lastName from users u join user_events ue on ue.user=u.id
join events e on e.id=ue.event and e.client = :client
join competitions c on e.id=c.event
where u.id=:uid
group by u.id';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":uid", $_GET['id'], PDO::PARAM_INT);
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $user = $command->queryRow();

        if (empty($user)) {
            Yii::app()->user->setFlash('error_statistics', 'Invalid user id');
            header("Location: ".Yii::app()->params['baseurl'].'/settings/statistics');
        }

        $data['user'] = $user;

        // get/validate current competition
        $query   = 'select et.name, ec.id, count(s1.score) as scores, avg(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as percentage, stddev(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as stddev from scores  s1
    join competitions c on c.id=s1.competition and c.id=:competition
    join events e on e.id=c.event and e.client = :client
    join entry_competitions ec on ec.entry=s1.entry and ec.competition =s1.competition
    join entry et on et.id=s1.entry
    left join criteria cr on cr.id=s1.criteria
    where s1.user=:uid
    group by s1.entry
    ORDER by  e.weight, c.weight
    ';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":uid", $_GET['id'], PDO::PARAM_INT);
        $command->bindParam(":competition", $_GET['id2'], PDO::PARAM_INT);
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $scores = $command->queryAll();

        if (empty($scores)) {
            Yii::app()->user->setFlash('error_statistics', 'Invalid competition id');
            header("Location: ".Yii::app()->params['baseurl'].'/settings/statistics');
        }

        $data['scores'] = $scores;
        $this->render('userc', $data);

    }//end actionUserc()


    public function actionIndex()
    {
        $data = [
            'entries' => 'SELECT count(*) FROM entry where client = :client',
            'events'  => 'SELECT count(*) FROM events where client = :client',
        ];

        foreach ($data as $key => $query) {
            $command = Yii::app()->db->createCommand($query);
            $cid     = Yii::app()->params['clientid'];
            $command->bindParam(":client", $cid, PDO::PARAM_INT);
            $data[$key] = $command->queryScalar();
        }

        /*
         * select u.firstName, count(distinct e.id) as Events, count(distinct c.id), s.*  from  users u
            left join user_events ue on ue.user=u.id
            join events e on e.id=ue.event and e.client = 1
            left join competitions c on c.event=e.id
            left join (
            select avg(s1.score) as avg, count(s1.score) as scores,  s1.user from scores  s1
            join competitions c on c.id=s1.competition
            join events e on e.id=c.event and e.client = 1 group by s1.user

            ) as s on s.user=u.id
            group by u.id
         */

        $query   = 'select u.id, u.firstName, u.lastName, u.email, u.lastLogin, count(distinct e.id) as events, count(distinct c.id) as competitions  from users u join user_events ue on ue.user=u.id
join events e on e.id=ue.event and e.client = :client
join competitions c on e.id=c.event group by u.id';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $users         = $command->queryAll();
        $data['users'] = $users;

        $query   = 'select s1.user, count(s1.score) as scores, avg(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as percentage, stddev(if(cr.max-cr.min = 0, 0, s1.score/(cr.max-cr.min)))*100 as stddev from scores  s1
    join competitions c on c.id=s1.competition

    join events e on e.id=c.event and e.client = :client
left join criteria cr on cr.id=s1.criteria
group by s1.user';
        $command = Yii::app()->db->createCommand($query);
        $cid     = Yii::app()->params['clientid'];
        $command->bindParam(":client", $cid, PDO::PARAM_INT);
        $devs = $command->queryAll();
        foreach ($devs as $dev) {
            $data['scores'][$dev['user']] = $dev;
        }

        $data['competitions'] = Competitions::model()->with("FieldCount", "Event", "Event.JudgeCount", "EntryCount", "CriteriaCount", "ScoreCount")->findAllByAttributes(['client' => Yii::app()->params['clientid']], ['order' => "Event.weight, t.weight"]);

        $this->render('index', $data);

    }//end actionIndex()


    public function actionJudges()
    {
        $this->render('judges');

    }//end actionJudges()


    // Uncomment the following methods and override them if needed
    /*
        public function filters()
        {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
        }

        public function actions()
        {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
        }
    */
}//end class
