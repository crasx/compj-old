<?php

class VisibilityController extends ControllerBase
{


    public function actionIndex()
    {
        // $this->datatables();
        $events = Events::model()->findAll();
        $this->render('index', ["events" => $events]);

    }//end actionIndex()


    public function actionResults()
    {

        // $this->datatables();
        if (isset($_GET['result'])) {
            switch ((int) $_GET['result']) {
            case 1:
                // success
                $success = "Results calculated";
                break;
            case 2:
                // not found
                $error = "Unable to find event";
                break;
            case 3:
                // removed
                $success = "Removed results";
                break;
            case 4:
                // not found
                $error = "An unknown error occurred";
                break;
            default:
                // fail
            }
        } else if (isset($_GET['calculate'])) {
            $E     = (int) $_GET['calculate'];
            $event = Events::model()->findByPk($E);
            if (!$event) {
                header("location: results?result=2");
                die();
            }

            if ($event->calculateResults()) {
                $event->finished = true;
                $event->scenarioNAS();
                $event->save();
                header("location: results?result=1");
            } else {
                header("location: results?result=4");
            }

            exit;
        } else if (isset($_GET['remove'])) {
            $E     = (int) $_GET['remove'];
            $event = Events::model()->findByPk($E);
            if (!$event) {
                header("location: results?result=2");
                die();
            }

            if ($event->removeResults()) {
                $event->finished = false;
                $event->scenarioNAS();
                $event->save();
                header("location: results?result=3");
            } else {
                header("location: results?result=4");
            }

            exit;
        }//end if

        $events = Events::model()->findAll();
        $this->render('results', ["events" => $events, "error" => $error, "success" => $success]);

    }//end actionResults()


    public function accessRules()
    {
        return [
            [
                'allow', 'actions' => [
                    'index',
                    'update',
                    "results",
                ], 'users' => ['@'], 'roles' => ['settings_visible']
            ],
            [
                'deny', 'users' => ['*']
            ],
        ];

    }//end accessRules()


    public function actionUpdate()
    {
        $ret = [
            "success" => false,
            "result"  => -1,
        ];
        if (isset($_POST['event'])&& is_array($_POST['event'])&&sizeof($_POST['event'])) {
               $eid   = array_shift(array_keys($_POST['event']));
               $event = Events::model()->findByPk($eid);
            if ($event) {
                if (is_array($_POST['event'][$eid])&&sizeof($_POST['event'][$eid])) {
                    $type = array_shift(array_keys($_POST['event'][$eid]));
                    $on   = $_POST['event'][$eid][$type] == "true";

                    $ret['success'] = false;
                    $ret['result']  = $on;
                    $event->scenarioNAS();

                    switch ($type) {
                    case "registration":
                        $event->registration = $on;
                        $ret['success']      = $event->update();
                        break;
                    case "mc":
                          $event->mc      = $on;
                          $ret['success'] = $event->update();
                        break;
                    case "judge":
                           $event->judge   = $on;
                           $ret['success'] = $event->update();
                        break;
                    default:
                        $ret['success'] = false;
                        break;
                    }
                }//end if
            }//end if
        }//end if

        die(json_encode($ret, JSON_THROW_ON_ERROR));

    }//end actionUpdate()


    // Uncomment the following methods and override them if needed
    /*
        public function filters()
        {
        // return the filter configuration for this controller, e.g.:
        return array(
        'inlineFilterName',
        array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
        ),
        );
        }

        public function actions()
        {
        // return external action classes, e.g.:
        return array(
        'action1'=>'path.to.ActionClass',
        'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
        ),
        );
        }
    */
}//end class
