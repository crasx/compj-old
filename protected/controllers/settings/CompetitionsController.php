<?php

class CompetitionsController extends AEDController
{


    public function beforeAction($action)
    {

        $this->_OBJ          = "competition";
        $this->_CLASS        = "Competitions";
        $this->_NAME         = "Competition";
        $this->_NAMEPLURAL   = "Competitions";
        $this->_PATH         = "settings/competitions";
        $this->ajaxIndex     = 0;


        $r = parent::beforeAction($action);
        $this->breadcrumbs[] = [
            'label' => 'Settings',
            'url'   => Yii::app()->params['baseurl'] . '/settings',
        ];
        $this->breadcrumbs[] = [
            'label' => 'Competitions',
            'url'   => Yii::app()->params['baseurl'] .'/settings/competitions',
        ];
        Yii::app()->clientScript->registerScriptFile("/js/cj/settings/competition.js", CClientScript::POS_HEAD);
        return $r;

    }//end beforeAction()


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'edit',
                    'add',
                    'delete',
                    'clone',
                ],
                'roles'   => ['settings_competition'],
            ],
        ];

    }//end _actionRules()


}//end class
