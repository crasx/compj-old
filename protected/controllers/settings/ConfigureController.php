<?php

class ConfigureController extends ControllerBase
{

    private array $errors = [];


    public function getErrors()
    {
        return $this->errors;

    }//end getErrors()


    public function actionIndex()
    {

        $this->breadcrumbs[] = 'Admin';
        $this->breadcrumbs[] = 'Configuration';
        $this->pullForm("configure");
        $this->render("index");

    }//end actionIndex()


    protected function pullForm($form)
    {
        if (!isset($_POST[$form]) || !is_array($_POST[$form])) {
            return;
        }

        $c = $_POST[$form];
        $f = $_FILES[$form];
        foreach (Yii::app()->params['configuration'] as $k => $v) {
            if (!is_array($v)) {
                die(__E($k)." is not an array");
            }

            if (isset($v['fileCheck']) && (!isset($f['tmp_name'][$k]) || empty($f['tmp_name'][$k]))) {
                continue;
            } else if (!isset($v['fileCheck']) && !isset($c[$k])) {
                continue;
            }

            if (isset($v['specialCheck'])) {
                $fcn = $v['specialCheck'];
                if (!$this->$fcn($c[$k])) {
                    $this->errors[$k] = $v['validateError'];
                    continue;
                }
            }

            if (isset($v['fileCheck'])) {
                $fcn = $v['fileCheck'];
                if (!$this->$fcn($f, $k, $c, $v)) {
                    continue;
                }
            }

            Yii::app()->config->set($k, $c[$k]);
        }//end foreach

        if (empty($this->errors)) {
            Yii::app()->user->setFlash('success', 'Settings saved successfully');
        }

    }//end pullForm()


    public function _actionRules()
    {
        return [
            [
                'actions' => [
                    'index',
                    'edit',
                    'add',
                    'delete',
                ],
                'roles'   => ['settings_configure'],
            ],
        ];

    }//end _actionRules()


    public function validateFlickr($val)
    {
        $old = Yii::app()->config->get("flickr_id");
        if ($old === $val) {
            return true;
        }

        if (empty($val)) {
            return true;
        }

        Yii::import('ext.phpflickr.phpFlickr', true);
        $f = new phpFlickr(Yii::app()->params['flickr_key'], Yii::app()->params['flickr_secret']);
        $u = $f->people_getInfo($val);

        return  !($u === false);

    }//end validateFlickr()


    public function getError($k)
    {
        return array_key_exists($k, $this->errors) ? $this->errors[$k] : "";

    }//end getError()


    private function validateLogo($f, $k, &$c, $d)
    {
        // _FILES, key,  &_POST, CONFIG DATA
        $error = $this->fileErrorText($f['error'][$k]);
        if ($error !== false) {
            $this->errors[$k] = $error;
            return false;
        }

        $ext = end(explode(".", strtolower((string) $f['name'][$k])));
        if (!isset($d['ext'])) {
            $imageData = getimagesize($f['tmp_name'][$k]);
            if ($imageData == false) {
                $this->errors[$k] = 'Invalid image format';
                return false;
            }
        } else {
            if (!in_array($ext, $d['ext'])) {
                $this->errors[$k] = 'Invalid image format';
                return false;
            }
        }

        $livedst = DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.Yii::app()->params['clientid']."-$k.$ext";
        $dst     = dirname((string) Yii::app()->request->scriptFile).$livedst;

        if (!move_uploaded_file($f['tmp_name'][$k], $dst)) {
            $this->errors[$k] = 'Error moving uploaded file';
            return false;
        }

        $old = Yii::app()->config->get($k);

        if (!empty($old) && strcmp((string) $old, $livedst) != 0) {
            $oldpath = dirname((string) Yii::app()->request->scriptFile).$old;
            if (file_exists($oldpath)) {
                unlink($oldpath);
            }
        }

        $c[$k] = $livedst;
        return true;

    }//end validateLogo()


    private function validateAlbum($val)
    {
        $a = $this->getAlbums();
        return array_key_exists($val, $a);

    }//end validateAlbum()


    private function validateInt($val)
    {
        return is_int($val + 0);

    }//end validateInt()


    public function getAlbums()
    {
        $albums  = [];
        $pid     = Yii::app()->config->get('picasa_id');
        $cacheId = "PICASA_ALBUM_LIST_".$pid;
        if ($pid != "") {
            $xml = Yii::app()->cache->get($cacheId);
            if ($xml === false || isset($_GET['recacheAlbums'])) {
                $url = 'http://picasaweb.google.com/data/feed/api/user/'.Yii::app()->config->get('picasa_id');
                $xml = file_get_contents($url);
                // a workaround needed on my server
                $xml = str_replace("xmlns='http://www.w3.org/2005/Atom'", '', $xml);
                $dom = new domdocument;
                $dom->loadXml($xml);
                $xpath = new domxpath($dom);
                $nodes = $xpath->query('//entry');
                foreach ($nodes as $node) {
                    $id          = $xpath->query('.//gphoto:id', $node)->item(0)->textContent;
                    $title       = $xpath->query('.//gphoto:name', $node)->item(0)->textContent;
                    $albums[$id] = $title;
                }

                Yii::app()->cache->set($cacheId, serialize($albums), (60 * 60));
                // cache for an hour
            } else {
                $albums = unserialize($xml);
            }
        }//end if

        return $albums;

    }//end getAlbums()


    private function validateTimezone($val)
    {
        $t = $this->getTimezones();
        foreach ($t as $region => $timezones) {
            if (in_array($val, $timezones)) {
                return true;
            }
        }

        return false;

    }//end validateTimezone()


    public function getTimezones()
    {
        static $regions = [
            'America'    => DateTimeZone::AMERICA,
            'Africa'     => DateTimeZone::AFRICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Asia'       => DateTimeZone::ASIA,
            'Atlantic'   => DateTimeZone::ATLANTIC,
            'Europe'     => DateTimeZone::EUROPE,
            'Indian'     => DateTimeZone::INDIAN,
            'Pacific'    => DateTimeZone::PACIFIC,
        ];
        $cities         = [];
        foreach ($regions as $name => $mask) {
            $cities[$name] = DateTimeZone::listIdentifiers($mask);
        }

        foreach ($cities as $key => $value) {
            $cities[$key] = array_unique($value);
            ksort($cities[$key]);
        }

        return $cities;

    }//end getTimezones()


}//end class
