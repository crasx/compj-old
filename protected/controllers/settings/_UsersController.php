<?php

class UsersController extends AEDController
{


    public function beforeAction($action)
    {

        $this->_OBJ      = "user";
        $this->_CLASS    = "User";
        $this->_PATH     = "settings/users";
        $this->ajaxIndex = 0;
        return parent::beforeAction($action);

    }//end beforeAction()


    /*
        public function actionDelete()
        {
        $id=(int)$_GET['id'];
        $user=User::model()->findByPk($id);
        if($user==null){
        header("Location:".Yii::app()->params['baseurl']."/settings/users/?message=invalid");
        return;
        }
        if(isset($_POST['user'])){
        if($user->hasScores()){
                if(isset($_POST['user']['scores'])){
                    //delete scores
                }
        }
        $user->delete();
        header("Location:".Yii::app()->params['baseurl']."/settings/users/?message=deleted");
        }

        $this->render('delete', array("user"=>$user));
    }*/

    public function accessRules()
    {
        return [
            [
                'allow', 'actions' => [
                    'index',
                    'edit',
                    'add',
                    'delete',
                ], 'users' => ['@'], 'roles' => ['settings_user']
            ],
            [
                'deny', 'users' => ['*']
            ],
        ];

    }//end accessRules()


    // Uncomment the following methods and override them if needed
    /*
        public function filters()
        {
        // return the filter configuration for this controller, e.g.:
        return array(
        'inlineFilterName',
        array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
        ),
        );
        }

        public function actions()
        {
        // return external action classes, e.g.:
        return array(
        'action1'=>'path.to.ActionClass',
        'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
        ),
        );
        }
    */
}//end class
