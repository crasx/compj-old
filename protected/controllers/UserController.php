<?php

class UserController extends ControllerBase
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/user';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];

    }//end filters()


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                // allow authorized user to logout and view index
                'actions' => [
                    'index',
                    'profile',
                    'logout',
                ],
                'users' => ['@'],
            ],
            [
                'allow',
                // allow all users to perform 'index' and 'view' actions
                'actions' => [
                    'login',
                    'register',
                    'forgot',
                    'restore',
                ],
                'users' => ['?'],
            ],
        ];

    }//end accessRules()

    public function actionProfile()
    {
        $user = Yii::app()->user->getUser();
        $originalPassword = $user->password;

        if (isset($_POST['profile'])) {
            if (!empty($_POST['profile']['passwordCurrent'])) {
                $identity = new UserIdentity($user->email, $_POST['profile']['passwordCurrent']);
                if (!$identity->authenticate()) {
                    $user->addError('passwordCurrent', 'Invalid current password');
                } else {
                    $user->password = $_POST['profile']['password'];
                    $user->password2 = $_POST['profile']['password2'];
                }
            }

            if (empty($user->password2)) {
                $user->password2 = $originalPassword;
            }

            foreach (['firstName', 'lastName', 'email'] as $field) {
                    $user->$field = $_POST['profile'][$field];
            }

            if ($user->validate() && !empty($user->hasErrors()) && $user->save()) {
                Yii::app()->user->setFlash('success_profile', 'Profile updated');
                Yii::app()->user->cInit();
            }
        }
        $this->render('profile',
            [
                'user' => $user,
                'errors' => $user->getErrors(),
            ]);

    }//end actionProfile()

    /**
     * Logs in the user and redirects to the homepage if successful.
     * If the user is already logged in, it redirects to the homepage.
     * If the user is a guest and the login form is submitted, it authenticates the user credentials,
     * logs in the user, initializes user data, and updates the last login timestamp.
     * It generates a unique key for CSRF protection and renders the login form view.
     *
     * @return void
     */
    public function actionLogin()
    {
        if (Yii::app()->user->isGuest && isset($_POST['loginForm'])) {
            if (!empty($_POST['fkey']) && !empty($_SESSION['fkey']) && strcmp((string)$_POST['fkey'], (string)$_SESSION['fkey']) == 0) {
                $identity = new UserIdentity($_POST['loginForm']['email'], $_POST['loginForm']['password']);
                if ($identity->authenticate()) {
                    Yii::app()->user->login($identity, (3600 * 24 * 7));
                    Yii::app()->user->cInit();
                    Yii::app()->user->updateLastLogin();
                }
            }
        }

        if (!Yii::app()->user->isGuest) {
            $this->redirect('/');
        }

        $uniq = uniqid(random_int(0, mt_getrandmax()), true);
        $_SESSION['fkey'] = md5($_SERVER['REMOTE_ADDR'] . $uniq);
        $this->render('login');

    }//end actionLogin()


    /**
     * Sends a reset email to the user if the submitted captcha is successful and email is provided.
     * Also sets flash messages for success or error.
     */
    public function actionForgot()
    {
        if (!Yii::app()->user->isGuest) {
            Yii::app()->user->setFlash('error_email', 'You are already logged in!');
        }

        $this->layout = '//layouts/user';
        $user = false;

        if (!empty($_POST['forgot'])) {
            if ($this->recaptcha()->verify($_POST['g-recaptcha-response'] ?? '', $_SERVER['REMOTE_ADDR'])->isSuccess()) {
                if (!empty($_POST['forgot']['email'])) {
                    $user = User::model()->findByAttributes(['email' => $_POST['forgot']['email']]);
                    if ($user) {
                        $user->sendResetEmail($this);
                    }
                    Yii::app()->user->setFlash('success_email', 'If the email provided belonged to an account, a reset link has been sent to it. Please check your email.');
                    $this->redirect('/');
                }
            }
            Yii::app()->user->setFlash('error_email', 'There was a problem with your submission, please try again.');
        }


        $this->render('forgot');

    }


    public function actionRestore()
    {
        if (!Yii::app()->user->isGuest) {
            Yii::app()->user->setFlash('error_email', 'You are already logged in!');
        }

        $this->layout = '//layouts/user';
        $this->recaptcha = true;
        $errors = [];
        if (!empty($_POST['restore'])) {
            $this->recaptcha();
            $recaptcha = new \ReCaptcha\ReCaptcha(Yii::app()->params['recaptcha_private']);

            $validCaptcha = false;

            if (!empty($_POST['g-recaptcha-response'])) {
                $validCaptcha = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])->isSuccess();
            }

            if (!$validCaptcha) {
                Yii::app()->user->setFlash('error_recaptcha', 'Unable to validate you are human');
            } else {
                if (!empty($_POST['restore']['email'])) {
                    $user = User::model()->findByAttributes(['email' => $_POST['restore']['email'], 'resetHash' => $_POST['restore']['hash']]);
                    if ($user) {
                        $rtime = strtotime((string)$user->resetExpires);
                        if ($rtime < time()) {
                            Yii::app()->user->setFlash('error_recaptcha', 'This reset code has expired, please request a new one');
                        } else {
                            $user->password = $_POST['restore']['password'];
                            $user->password2 = $_POST['restore']['password2'];

                            $user->setScenario("restore");
                            if (!$user->save()) {
                                $errors = $user->getErrors();
                            } else {
                                Yii::app()->user->setFlash('success_reset', 'Your password has been reset');
                                $identity = new UserIdentity($_POST['restore']['email'], $_POST['restore']['password']);
                                if ($identity->authenticate()) {
                                    Yii::app()->user->login($identity, (3600 * 24 * 7));
                                    Yii::app()->user->cInit();
                                    Yii::app()->user->updateLastLogin();
                                }

                                $this->redirect('/');
                            }
                        }//end if
                    } else {
                        Yii::app()->user->setFlash('error_email', 'Sorry, that email is not in our system or you supplied an invalid reset code');
                    }//end if
                }//end if
            }//end if
        }//end if

        $this->render('restore', ['errors' => $errors]);

    }//end actionRestore()


    public function actionRegister()
    {
        $user = new User;
        if (isset($_POST['email'])) {
            $user->attributes = $_POST;
            if ($user->validate() && $user->save(false)) {
                Yii::app()->user->setFlash('success_register', 'Registration successful!');
                $identity = new UserIdentity($_POST['email'], $_POST['password']);
                if ($identity->authenticate()) {
                    Yii::app()->user->login($identity, (3600 * 24 * 7));
                    Yii::app()->user->cInit();
                    Yii::app()->user->updateLastLogin();
                }

                $this->redirect('/');
            }
        }

        $this->render('register', ['user' => $user]);

    }//end actionRegister()


    /**
     * Displays the login page
     */


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('/');

    }//end actionLogout()


    /**forgot
     * Lists all models.
     */
    public function actionIndex()
    {
        if (Yii::app()->user->getIsGuest()) {
            header('location: /user/login');
            exit();
            $owned = $partof = [];
        } else if (Yii::app()->user->isgod) {
            $owned = (array)Clients::model()->findAll();
            $partof = [];
        } else {
            $owned = (array)Clients::model()->findAllByAttributes(['owner' => Yii::app()->user->uid]);
            $partof = (Yii::app()->user->getClients() ?? []);
        }

        if ((sizeof($owned) + sizeof($partof)) == 1) {
            if (sizeof($partof) == 0) {
                header('location: /' . urlencode((string)$owned[0]->path));
            } else {
                header('location: /' . urlencode((string)$partof[0]->path));
            }
        }

        $this->render('index', ['owned' => $owned, 'partof' => $partof]);

    }//end actionIndex()

}//end class
