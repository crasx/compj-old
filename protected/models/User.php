<?php

/**
 * This is the model class for table "dev_users".
 *
 * The followings are the available columns in table 'dev_users':
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $salt
 * @property string $password
 * @property string $lastLogin
 * @property string $resetHash
 * @property integer $permissions
 *
 * The followings are the available model relations:
 * @property DevEvents[] $devEvents
 * @property DevScores[] $devScores
 */
class User extends CActiveRecord
{

    public $password2;

    public $isgod;

    public $lastLogin;

    protected $_c;

    public $Clients;


    /**
     * Returns the static model of the specified AR class.
     *
     * @return User the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }

    public function getName()
    {
        return $this->firstName.' '.$this->lastName;

    }

    public function sendResetEmail($obj)
    {

        if (empty($this->resetHash) || strtotime((string) $this->resetExpires) < time()) {
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $h          = '';
            $totalChars = (strlen($characters) - 1);

            for ($i = 0; $i < 32; $i++) {
                $h .= $characters[random_int(0, $totalChars)];
            }

            $this->resetHash    = $h;
            $this->resetExpires = new CDbExpression("now() + INTERVAL 1 DAY");
            $this->save(false);
        }

        $name = $this->firstName.' '.$this->lastName;
        mail(
            $this->email,
            "Replacement login information for ".$name." at ".Yii::app()->params['name'],
            (string) $obj->renderPartial('//email/forgot', ['name' => $name, 'hash' => $this->resetHash], true),
            "From: ".Yii::app()->params['fromEmail']
        );

    }//end sendResetEmail()


    public function hasPermission($bit)
    {
        return ($this->permissions & $bit) == $bit;

    }//end hasPermission()


    public function hasScores()
    {
        return false;

    }//end hasScores()


    public function beforeDelete()
    {
        // delete all your minions
        Scores::model()->deleteAllByAttributes(["user" => $this->id]);
        UserEvents::model()->deleteAllByAttributes(["user" => $this->id]);
        return parent::beforeDelete();

    }//end beforeDelete()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'firstName, lastName, email, password,',
                'required',
            ],
            [
                'email, firstName, lastName', 'length', 'max' => 255
            ],
            [
                'email', 'email', 'message' => 'Invalid email'
            ],
            [
                'email', 'unique', 'allowEmpty' => false
            ]
        ];

    }//end rules()


    public function beforeSave()
    {
        if ($this->isNewRecord || $this->scenario == 'restore') {
            $this->makeHashedPassword($this->password);
            $this->resetHash = $this->resetExpires = new CDbExpression("null");
        }

        return parent::beforeSave();

    }//end beforeSave()


    public function makeHashedPassword($password)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $h          = '';
        $totalChars = (strlen($characters) - 1);

        for ($i = 0; $i < 22; $i++) {
            $h .= $characters[random_int(0, $totalChars)];
        }

        $this->salt     = $h;
        $this->password = $this->applyHash($password);
        return $h;

    }//end makeHashedPassword()


    public function applyHash($pass)
    {
        return crypt((string) $pass, '$2a$08$'.$this->salt);

    }//end applyHash()


    public function avgScore($comp)
    {
        $criteria = new CDbCriteria(['select' => 'avg(score) as avgScore', 'condition' => '"competition"=:comp and "user"=:user', 'params' => [':comp' => $comp, 'user' => $this->id]]);

        $arows = $this->dbConnection->commandBuilder->createFindCommand(Scores::model()->tableSchema, $criteria)->queryAll();
        return $arows[0]['avgscore'];

    }//end avgScore()


    public function getCommandBuilder()
    {
        return $this->getDbConnection()->getSchema()->getCommandBuilder();

    }//end getCommandBuilder()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            // 'devEvents' => array(self::MANY_MANY, 'DevEvents', 'dev_user_events(user, event)'),
            'Scores'     => [
                self::HAS_MANY,
                'Scores',
                'user',
            ],
//            'UserEvents' => [
//                self::HAS_MANY,
//                'UserEvents',
//                'user',
//                'group' => ['event'],
//                'index' => 'event'
//            ],
//            'Events'     => [
//                self::HAS_MANY, 'Events', ['event' => 'id'], 'through' => 'UserEvents'
//            ],
//            'Clients'    => [
//                self::HAS_MANY, 'Clients', ['client' => 'id'], 'through' => 'Events'
//            ],
        ];

    }//end relations()


}//end class
