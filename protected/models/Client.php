<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 *
 * @property string $id
 * @property string $path
 * @property integer $owner
 * @property integer $license
 */
class Client extends CActiveRecord
{


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clients';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'path, owner, license',
                'required',
            ],
            [
                'owner, license', 'numerical', 'integerOnly' => true
            ],
            [
                'path', 'length', 'max' => 32
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            [
                'id, path, owner, license', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'path'    => 'Path',
            'owner'   => 'Owner',
            'license' => 'License',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('path', $this->path, true);
        $criteria->compare('owner', $this->owner);
        $criteria->compare('license', $this->license);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param  string $className active record class name.
     * @return Client the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


}//end class
