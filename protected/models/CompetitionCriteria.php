<?php

/**
 * This is the model class for table "dev_competition_criteria".
 *
 * The followings are the available columns in table 'dev_competition_criteria':
 *
 * @property integer $id
 * @property integer $competition
 * @property integer $criteria
 *
 * The followings are the available model relations:
 * @property Competitions $competition0
 * @property Criteria $criteria0
 * @property Scores[] $scores
 */
class CompetitionCriteria extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @return CompetitionCriteria the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'competition_criteria';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'competition, criteria',
                'required',
            ],
            [
                'competition, criteria', 'numerical', 'integerOnly' => true
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'id, competition, criteria', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Competition' => [
                self::BELONGS_TO,
                'Competitions',
                'competition',
            ],
            'Criteria'    => [
                self::BELONGS_TO,
                'Criteria',
                'criteria',
            ],
            'Scores'      => [
                self::HAS_MANY,
                'Scores',
                'criteria',
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'competition' => 'Competition',
            'criteria'    => 'Criteria',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('competition', $this->competition);
        $criteria->compare('criteria', $this->criteria);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
