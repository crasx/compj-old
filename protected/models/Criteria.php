<?php

/**
 * This is the model class for table "dev_criteria".
 *
 * The followings are the available columns in table 'dev_criteria':
 *
 * @property integer $id
 * @property double $min
 * @property double $max
 * @property string $name
 * @property string $description
 * @property double $step
 *
 * The followings are the available model relations:
 * @property CompetitionCriteria[] $competitionCriterias
 */
class Criteria extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return Criteria the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'criteria';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Competition' => [
                self::MANY_MANY,
                'Competitions',
                'competition_criteria(competition, criteria)',
            ],
        ];

    }//end relations()


    public function hasValues()
    {
        if ($this->isNewRecord) {
            return false;
        }

        $r = (int) Scores::model()->countByAttributes(["criteria" => $this->id]);
        return $r > 0;

    }//end hasValues()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'name, description, min, max, step',
                'required',
            ],
            [
                'min, max, step',
                'numerical',
            ],
            [
                'name', 'length', 'max' => 255
            ],
            [
                'max',
                'minMaxCheck',
            ],
        ];

    }//end rules()


    public function minMaxCheck($attribute, $params)
    {

        if ($this->max < $this->min) {
            $this->addError($attribute, 'Min should be less than max');
        }

    }//end minMaxCheck()


    /*
     *
        public function checkCompetitions($attribute, $params){
            if(!is_array($this->competitions)){
                $this->competitions=array();
                return;
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition("id", $this->competitions);
            $results= (int)Competitions::model()->count($criteria);
            if($results!=sizeof($this->conditions)){
                $this->addError($attribute,'Error processing .');
            }

        }

        public function onAfterSave($event) //run after update also
        {

            parent::onAfterSave($event);
        }

        public function setData($data, $scenario='add'){
            if(!is_array($data))return false;

            if($scenario=="add")$this->isNewRecord=true;

            $this->scenario=$scenario;
            $validData=array("name", "description");
            foreach($validData as $k){
                if(isset($data[$k]))
                    $this->$k=$data[$k];
            }

            if(isset($data['judges']['selected']))$this->judges=$data['judges']['selected'];
            if(isset($data['competitions']['selected']))$this->competitions=$data['competitions']['selected'];


            //        $this->addError($attribute,'You must keep the "User Settings" permission.');



            return $this->validate();


        }




     */

}//end class
