<?php

/**
 * This is the model class for table "dev_entry".
 *
 * The followings are the available columns in table 'dev_entry':
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $registered
 *
 * The followings are the available model relations:
 * @property Competitions[] $devCompetitions
 * @property Scores[] $scores
 * @property Fields[] $devFields
 */
class Entry extends CActiveRecord
{

    public $name = '';
    public $competitions = [];
    public $fields = [];

    protected $here = [];
    protected $validImageFields = [];

    /**
     * Returns the static model of the specified AR class.
     *
     * @return Entry the static model class
     */
    public static function model($className = self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'entry';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Fields' => [
                self::HAS_MANY, 'EntryFields', 'entry', 'index' => 'field'
            ],
            'Competitions' => [
                self::MANY_MANY,
                'Competitions',
                'entry_competitions(entry, competition)',
            ],
            'Scores' => [
                self::HAS_MANY,
                'Scores',
                'entry',
            ],
            'EnteredCompetitions' => [
                self::HAS_MANY, 'EntryCompetitions', 'entry', 'index' => 'competition'
            ],
            'ActiveCompetitions' => [
                self::HAS_MANY, 'EntryCompetitions', 'entry', 'condition' => 'active=1', 'index' => 'competition'
            ],
            'Fields_mc' => [
                self::HAS_MANY, 'EntryFields', 'entry', 'condition' => '`Field`.`mc`=true', "with" => ["Field"], 'index' => 'field'
            ],
            'Fields_judge' => [
                self::HAS_MANY, 'EntryFields', 'entry', 'condition' => '`Field`.`judge`=true', "with" => ["Field"]
            ],
        ];

    }//end relations()


    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'name',
                'length',
                'max' => 255,
                'on' => 'add, edit'
            ],
            ['name', 'required', 'on' => 'add, edit'],
            [
                'competitions',
                'checkCompetitions',
                'on' => 'add, edit'
            ],
            [
                'fields',
                'checkFields',
                'on' => 'add, edit'
            ],
            [
                'here',
                'checkHere',
                'on' => 'add, edit'
            ],
        ];

    }//end rules()

    /**
     * This method is used to validate the 'competitions' attribute of the Entry model.
     * It checks if the competitions entered by the user are valid and registerable.
     * If the competitions are not valid or registerable, it adds an error message to the model.
     *
     * @param string $attribute The name of the attribute to be validated.
     * @param array $params Additional parameters for the validation rule.
     */
    public function checkCompetitions($attribute, $params)
    {
        if (empty($this->competitions)) {
            return;
        }

        $possibleCompetitions = $this->getRegisterableCompetitions();

        foreach ($this->competitions as $cid => $status) {
            // make sure each comp is registerable
            if (!isset($possibleCompetitions[$cid])) {
                $this->addError($attribute, 'Competition ' . $cid . ' is not registerable');
                continue;
            }
            switch ($status) {
                case 'withdrawn':
                    $this->competitions[$cid] = false;
                    break;
                case 'entered':
                    $this->competitions[$cid] = true;
                    break;
                case 'available':
                    unset($this->competitions[$cid]);
                    break;
                default:
                    $this->addError($attribute, 'Invalid status for competition ' . $cid);
            }
        }
    }


    public function checkFields($attribute, $params)
    {

        $comps = $this->getRegisterableCompetitions();
        $validatedFields = [];


        foreach ($this->competitions as $c => $status) {
            if (!$status) {
                continue;
            }
            $comp = Competitions::model()->findByPk($c);

            foreach ($comp->Fields as $field) {

                if (isset($validatedFields[$field->field])) {
                    continue;
                }

                if ($field->Field->Type->validator == 'uimage') {
                    $value = $this->getFileField($field->field);
                } else {
                    $value = $this->fields[$field->field] ?? null;
                }


                if ($field->required && empty($value)) {
                    $this->addFieldError("fields[{$field->field}]", 'This field is required for competition "' . __E($comp->name) . '"');
                }

                if (empty($value)) {
                    continue;
                }

                $validator = 'check_c_' . $field->Field->Type->validator;

                // We set this even if the field is invalid so we don't validate it again.
                // The validation function will add an errror and return null if it's invalid.
                $validatedFields[$field->field] = $this->$validator($field->field, $value);

            }
        }//end foreach

        $this->fields = $validatedFields;
    }//end checkFields()

    public function checkHere($attribute, $params)
    {
        $competitions = $this->getRegisterableCompetitions();
        foreach ($this->here as $c => $status) {
            if (!isset($competitions[$c])) {
                unset($this->here[$c]);
            } else {
                $this->here[$c] = $status == 'on';
            }
        }
    }//end checkHere()

    public function getFieldValue($field)
    {
        return $this->Fields[$field]?->value ?? null;
    }


    public function getFileField($field)
    {

        $error = $_FILES['registration']['error']['fields'][$field] ?? 4;

        // Error 4 is no file, which is fine for us.
        if ($error != 4) {
            return [
                'name' => $_FILES['registration']['name']['fields'][$field],
                'tmp_name' => $_FILES['registration']['tmp_name']['fields'][$field],
                'type' => $_FILES['registration']['type']['fields'][$field],
                'error' => $_FILES['registration']['error']['fields'][$field],
                'size' => $_FILES['registration']['size']['fields'][$field],
            ];
        }
        return null;
    }


    public function getPossibleFields()
    {
        static $possibleFields;

        if ($possibleFields === null) {
            $possibleFields = [];
            $comps = $this->getRegisterableCompetitions();
            foreach ($comps as $c) {
                foreach ($c->Fields as $f) {
                    $possibleFields[$f->field] = $f->Field;
                }
            }
        }
        return $possibleFields;
    }

    public function getRegisterableCompetitions()
    {
        static $registerableCompetitions;
        if ($registerableCompetitions === null) {
            $registerableCompetitions = [];
            $events = $this->getRegisterableEvents();
            foreach ($events as $e) {
                foreach ($e->Competitions as $c) {
                    $registerableCompetitions[$c->id] = $c;
                }
            }
        }

        return $registerableCompetitions;
    }

    public function getRegisterableEvents()
    {
        static $selectable_events;
        if ($selectable_events === null) {
            $selectable_events = Events::model()
                ->with('Competitions', 'Competitions.Fields')
                ->findAllByAttributes(
                    [
                        'client' => Yii::app()->params['clientid'],
                        'registration' => 1
                    ], ['index' => 'id']);
        }
        return $selectable_events;
    }


    private function check_c_uimage($key, $value)
    {
        if (empty($value)) {
            return;
        }

        if ($value['error'] != 0) {
            $this->addFieldError($key, "Error uploading file, try again");
            return;
        }

        $info = getimagesize($value['tmp_name']);
        if ($info === false) {
            $this->addFieldError($key, "Invalid file type");
            return;
        } elseif ($info[2] != IMAGETYPE_JPEG && $info[2] != IMAGETYPE_PNG) {
            $this->addFieldError($key, "Invalid file type, only jpeg and png are allowed");
            return;
        }

        $value['extension'] = $info[2] == IMAGETYPE_JPEG ? 'jpg' : 'png';

        $this->validImageFields[$key] = $key;
        return $value;


    }//end check_c_uimage()


    private function check_c_date($key, $value)
    {
        $d = strtotime((string)$value);
        if ($d === false && !empty($value)) {
            $this->addFieldError($key, '"' . __E($value) . '" is not a valid date');
        }

        return (string)$value;
    }//end check_c_date()


    private function check_c_email($key, $value)
    {
        $d = filter_var($value, FILTER_VALIDATE_EMAIL);
        if ($d === false && !empty($value)) {
            $this->addFieldError($key, '"' . __E($value) . '" is not a valid email');
        }
        return (string)$value;

    }//end check_c_email()


    private function check_c_text($key, $value)
    {
        return $value;

    }//end check_c_text()


    private function check_c_pimage($key, $value)
    {

        // TODO: RRemove this function if it's not used.
        return '';

    }//end check_c_pimage()


    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->registered = new CDbExpression("now()");
        }

        $this->updated = new CDbExpression("now()");
        $this->client = Yii::app()->params['clientid'];
        return parent::beforeSave();

    }//end beforeSave()


    public function afterSave()
    {
        $this->saveCompetitions();
        $this->saveFields();

        return parent::afterSave();

    }//end afterSave()

    private function saveCompetitions()
    {
        foreach ($this->competitions as $cid => $status) {
            $entry = EntryCompetitions::model()->findByPk(["entry" => $this->id, "competition" => $cid]);
            if ($entry == null) {
                // get current id
                $row = Yii::app()->db
                    ->createCommand('select max(id) as m from entry_competitions where competition=:c')
                    ->bindParam(':c', $cid)
                    ->queryRow();

                $currid = (($row['m'] ?? 0) + 1);

                $entry = new EntryCompetitions();
                $entry->entry = $this->id;
                $entry->competition = $cid;
                $entry->id = $currid;
            }

            if (isset($this->here[$cid])) {
                $entry->here = $this->here[$cid];
            }
            $entry->active = (bool)$status;
            $entry->save();

        }


        $compIdMsg = '<strong>Contestant competition ids:</strong>';
        $entries = EntryCompetitions::model()->findAllByAttributes(["entry" => $this->id]);

        foreach ($entries as $e) {
            $compIdMsg .= '<br />' . __E($e->Competition->name) . ': ' . $e['id'];
        }

        Yii::app()->user->setFlash('info-cids', $compIdMsg);

    }


    protected function saveFields()
    {
        foreach ($this->getPossibleFields() as $k => $value) {

            if (!isset($this->fields[$k])) {
                continue;
            }

            $entryField = EntryFields::model()->findByPk(["entry" => $this->id, "field" => $k]);

            if (isset($this->validImageFields[$k])) {
                $upload = new Upload('insert');
                $upload->attributes = [
                    'uploadedFile' => $this->fields[$k],
                    'entry' => $this->id,
                    'client' => Yii::app()->params['clientid'],
                ];
                $upload->save();
                // Write id as field value.
                $this->fields[$k] = $upload->id;

                // Mark old recorrd as deleted.
                if ($entryField != null) {
                    $oldUploadRecored = Upload::model()->findByPk($entryField->value);
                    if ($oldUploadRecored != null) {
                        $oldUploadRecored->deleted = 1;
                        $oldUploadRecored->save();
                    }
                }
            }

            if ($entryField == null) {
                $entryField = new EntryFields();
                $entryField->entry = $this->id;
                $entryField->field = $k;
            }

            $entryField->value = $this->fields[$k];

            if (!$entryField->save(false)) {
                Yii::app()->user->setFlash('error', 'Error saving field ' . $value->name);
            }
        }

    }//end saveFields()


    public function getScoreCount($competition)
    {

        return Scores::model()->countByAttributes(["competition" => $competition, "user" => Yii::app()->user->getUID(), "entry" => $this->id]);

    }//end getScoreCount()


    public


    function getJudgeAverage($competition, $judge)
    {
        $command = Yii::app()->db->createCommand('select avg(score) from scores where entry=:entry and competition = :competition and user = :user');
        $command->bindParam(':user', $judge);
        $command->bindParam(':competition', $competition);
        $entry = $this->id;
        $command->bindParam(':entry', $entry);
        return $command->queryScalar();

    }//end getJudgeAverage()


    public function avgScore($comp, $judgeNormals)
    {
        $r = 0;

        foreach ($judgeNormals as $jid => $jnormal) {
            $command = Yii::app()->db->createCommand('select avg(s.score) as a from scores s where s.user=:jid and s.competition=:comp and s.entry=:ent ');
            $i = $this->id;
            $command->bindParam(":comp", $comp, PDO::PARAM_INT);
            $command->bindParam(":ent", $i, PDO::PARAM_INT);
            $command->bindParam(":jid", $jid, PDO::PARAM_INT);
            $row = $command->queryScalar();

            $r += ($row * $jnormal);
        }

        $round = Yii::app()->config->get('precision');
        if (empty($round)) {
            $round = 2;
        }

        return round($r, 2);

    }//end avgScore()


    public


    function beforeDelete()
    {
        // delete all your minions
        EntryFields::model()->deleteAllByAttributes(["entry" => $this->id]);
        EntryCompetitions::model()->deleteAllByAttributes(["entry" => $this->id]);
        Scores::model()->deleteAllByAttributes(["entry" => $this->id]);
        return parent::beforeDelete();

    }//end beforeDelete()


    /*
        protected function beforeValidate(){
        if(grabLicenseData('demo', false)){
            $cont=Entry::model()->count();
            if($cont>=3)
                $this->addError('name', 'Only 3 registrations allowed in demo mode!');
        }
        return parent::beforeValidate();
        }
    */


}//end class
