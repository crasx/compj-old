<?php

/**
 * This is the model class for table "dev_competitions".
 *
 * The followings are the available columns in table 'dev_competitions':
 *
 * @property integer $id
 * @property string $name
 * @property integer $event
 * @property integer $type
 * @property string $description
 * @property integer $maxContestants
 * @property integer $bestSelect
 * @property boolean $bestOrderMatters
 *
 * The followings are the available model relations:
 * @property Events $event0
 * @property CompetitionCriteria[] $competitionCriterias
 * @property CompetitionFields[] $competitionFields
 * @property Entry[] $devEntries
 */
class Competitions extends CActiveRecord
{

    protected $criteria = [];

    protected $registration = [];

    public $cloneOptions;

    private array $_criteriaToDelete = [];

    private array $_registrationToDelete = [];


    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Competitions the static model class
     */
    public static function model($className = self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'competitions';

    }//end tableName()


    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'name, description, weight, event',
                'required',
                'on' => 'add, edit',
            ],
            [
                'event', 'exist', 'className' => 'Events', 'attributeName' => 'id', 'message' => 'Invalid event',
                'criteria' => [
                    'condition' => 'client=:client',
                    'params' => [':client' => Yii::app()->params['clientid']],
                ],
                'on' => 'add, edit',
            ],
            [
                'weight', 'numerical', 'integerOnly' => 1,
                'on' => 'add, edit',
            ],
            [
                'criteria',
                'checkCriteria',
                'on' => 'add, edit',
            ],
            [
                'registration',
                'checkRegistration',
                'on' => 'add, edit',
            ],
            [
                'cloneOptions', 'checkCloneOptions', 'on' => 'clone'
            ],
        ];

    }//end rules()



    function getMaxEntryId()
    {
        $command = Yii::app()->db->createCommand('SELECT ifnull(max(id), 0) as id FROM entry_competitions where competition=:comp');
        $id = $this->id;
        $command->bindParam(":comp", $id, PDO::PARAM_INT);
        $result = $command->queryRow();
        return $result['id'];

    }//end getMaxEntryId()


    public function checkCriteria($attribute, $params)
    {
        if (!isset($_POST['criteria'])) {
            return;
        }

        $criteria = $_POST['criteria'];
        $diffCriteria = $this->CompetitionCriteria;
        // afterwords we will delete these
        $allCriteria = [];
        $validCriteria = $this->getSelectableCriteria();
        if (isset($criteria['selected']) && is_array($criteria['selected'])) {
            foreach ($criteria['selected'] as $cid) {
                // loop through submissions
                if (array_key_exists($cid, $validCriteria)) {
                    if (isset($diffCriteria[$cid])) {
                        // if its allready a thing no worries
                        $allCriteria[$cid] = $diffCriteria[$cid];
                        unset($diffCriteria[$cid]);
                        // unset so it is not deleted
                    } else {
                        // if its not then make an object
                        $allCriteria[$cid] = new CompetitionCriteria();
                        $allCriteria[$cid]->criteria = $cid;
                    }

                    // set all weights
                    if (isset($criteria['weight']) && is_array($criteria['weight']) && isset($criteria['weight'][$cid])) {
                        $allCriteria[$cid]->weight = (int)$criteria['weight'][$cid];
                    }
                }
            }
        }//end if

        // TODO: CHECK IF $diffCriteria HAS SCORES
        $this->_criteriaToDelete = $diffCriteria;
        $this->criteria = $allCriteria;

    }//end checkCriteria()


    public function getSelectableCriteria()
    {

        return Criteria::model()->findAllByAttributes(['client' => Yii::app()->params['clientid']], ['index' => 'id']);

    }//end getSelectableCriteria()


    public function checkRegistration($attribute, $params)
    {
        if (!isset($_POST['registration'])) {
            return;
        }

        $registration = $_POST['registration'];

        $diffRegistration = $this->Fields;
        // afterwords we will delete these
        $allRegistration = [];
        $validRegistration = $this->getSelectableRegistrationFields();
        if (isset($registration['selected']) && is_array($registration['selected'])) {
            foreach ($registration['selected'] as $rid) {
                // loop through submissions
                if (array_key_exists($rid, $validRegistration)) {
                    if (isset($diffRegistration[$rid])) {
                        // if its allready a thing no worries
                        $allRegistration[$rid] = $diffRegistration[$rid];
                        unset($diffRegistration[$rid]);
                        // unset so it is not deleted
                    } else {
                        // if its not then make an object
                        $allRegistration[$rid] = new CompetitionFields();
                        $allRegistration[$rid]->field = $rid;
                    }

                    // set all weights
                    if (isset($registration['weight']) && is_array($registration['weight']) && isset($registration['weight'][$rid])) {
                        $allRegistration[$rid]->weight = (int)$registration['weight'][$rid];
                    }

                    $allRegistration[$rid]->required = $this->_grabCBValue($registration, 'required', $rid);
                    $allRegistration[$rid]->mc = $this->_grabCBValue($registration, 'mc', $rid);
                    $allRegistration[$rid]->report = $this->_grabCBValue($registration, 'report', $rid);
                    $allRegistration[$rid]->judge = $this->_grabCBValue($registration, 'judge', $rid);
                }//end if
            }//end foreach
        }//end if

        // TODO: CHECK IF $diffRegistration HAS SCORES
        $this->_registrationToDelete = $diffRegistration;
        $this->registration = $allRegistration;

    }//end checkRegistration()


    public function getSelectableRegistrationFields()
    {
        return Fields::model()->findAllByAttributes(['client' => Yii::app()->params['clientid']], ['index' => 'id']);

    }//end getSelectableRegistrationFields()


    private function _grabCBValue($array, $index, $id)
    {
        if (isset($array[$index]) && is_array($array[$index])) {
            return isset($array[$index][$id]) && $array[$index][$id] == 'on';
        }

        return false;

    }//end _grabCBValue()


    //
    // Criteria frontend/////////////////
    //
    public function removeScores()
    {
        Scores::model()->deleteAllByAttributes(["competition" => $this->id]);
        return true;

    }//end removeScores()


    public function getSelectableEvents()
    {
        $events = Events::model()->findAllByAttributes(['client' => Yii::app()->params['clientid']], ['order' => 'weight']);
        $ret = [];
        if ($events) {
            foreach ($events as $e) {
                $ret[$e->id] = $e->name;
            }
        }

        return $ret;

    }//end getSelectableEvents()


    //
    // Reg frontend/////////////////
    //
    public function getSelectedCriteria()
    {
        if (empty($_POST['criteria'])) {
            return $this->CompetitionCriteria;
        } else {
            return $this->criteria;
        }

    }//end getSelectedCriteria()


    public function getSelectedRegistrationFields()
    {
        if (empty($_POST['registration'])) {
            return $this->Fields;
        } else {
            return $this->registration;
        }

    }//end getSelectedRegistrationFields()


    public function calculateResults($nolink = false)
    {

        // first check results cache
        $command = Yii::app()->db->createCommand(
            '
        select max(updated) as u from scores where competition=:competition
union all
select max(updated) as u from results where competition=:competition

'
        );
        $i = $this->id;
        $command->bindParam(":competition", $i, PDO::PARAM_INT);
        $r = $command->queryAll();
        $entries = [];
        $scores = [];
        $uid = [];

        if ($r[0]['u'] != $r[1]['u']) {
            // recache scores
            $lastupdate = $r[0]['u'];
            $result = [];
            $judgeA = $this->getJudgeAverages();
            $normals = [];
            // entry->score
            if (!sizeof($judgeA)) {
                return $result;
            }

            $judgeAverage = (array_sum($judgeA) / sizeof($judgeA));
            foreach ($judgeA as $k => $v) {
                if ($v != 0) {
                    $normals[$k] = ($judgeAverage / $v);
                } else {
                    $normals[$k] = 0;
                }
            }

            foreach ($this->CEntries as $e) {
                $id = $e->id;
                $e = $e->Entry;
                $uid[$id] = $e->id;
                $scores[$id] = $e->avgScore($this->id, $normals);
                $entries[$id] = $e->name;
                $command = Yii::app()->db->createCommand(
                    'insert into results(score, entry, competition, updated)
                          values(:score, :entry, :competition, :updated )
                      on duplicate key update score=:score, updated=:updated'
                );

                $eid = $e->id;
                $cid = $this->id;
                $command->bindParam(":score", $scores[$id]);
                $command->bindParam(":entry", $eid, PDO::PARAM_INT);
                $command->bindParam(":competition", $cid, PDO::PARAM_INT);
                $command->bindParam(":updated", $lastupdate);
                $command->execute();
            }
        } else {
            // else pull from results cache
            $command = Yii::app()->db->createCommand(
                'select ec.id, e.id as eid, e.name, r.score from results r
          left join entry e on e.id = r.entry
          left join entry_competitions ec on ec.entry = r.entry and ec.competition=r.competition
            where r.competition=:competition'
            );
            $i = $this->id;
            $command->bindParam(":competition", $i, PDO::PARAM_INT);
            $r = $command->queryAll();

            foreach ($r as $result) {
                $uid[$result['id']] = $result['eid'];
                $scores[$result['id']] = $result['score'];
                $entries[$result['id']] = $result['name'];
            }
        }//end if

        arsort($scores);
        $result = [];
        foreach ($scores as $k => $s) {
            if ($nolink) {
                $result[] = [
                    $k,
                    $entries[$k],
                    $s,
                    $uid[$k],
                ];
            } else {
                $result[] = [
                    $k,
                    '<a href="#" onclick="populateContestant(' . $this->id . ',' . $uid[$k] . ');return false;">' . __E($entries[$k]) . '</a>',
                    $s,
                    $uid[$k],
                ];
            }
        }

        return $result;

    }//end calculateResults()


    public function getJudgeAverages()
    {
        $command = Yii::app()->db->createCommand(
            'SELECT ue.user, (select avg(s.score) from scores s where s.user=ue.user and s.competition=c.id) as avg
        FROM competitions c
         join user_events ue on ue.event=c.event
         where
         c.id=:competition
         and ue.permission = "judge"'
        );
        $i = $this->id;
        $command->bindParam(":competition", $i, PDO::PARAM_INT);
        $return = [];
        $r = $command->queryAll();

        foreach ($r as $row) {
            $return[$row['user']] = $row['avg'];
        }

        return $return;

    }//end getJudgeAverages()

    public function checkCloneOptions($attribute, $params) {
        if (empty($this->cloneOptions)) {
            $this->addError($attribute, 'No options selected');
        }
        $this->cloneOptions = [
            'eventOverride' => $this->cloneOptions['eventOverride'] ?? FALSE,
            'replaceEnabled' => !empty($this->cloneOptions['replaceEnabled']),
            'replaceText' => $this->cloneOptions['replaceText'] ?? '',
            'replaceWith' => $this->cloneOptions['replaceWith'] ?? '',
            'weightAdjust' => !empty($this->cloneOptions['weightAdjust']),
            'weightAdjustmentAmount' => (int) $this->cloneOptions['weightAdjustmentAmount'] ?? 0,
        ];

        if ($this->cloneOptions['replaceEnabled'] && empty($this->cloneOptions['replaceText'])) {
            $this->addError($attribute.'[replaceEnabled]', '"Replace text" is enabled but no text to replace is provided');
        }
    }


    public function clone()
    {
        if ($this->cloneOptions['eventOverride'] != FALSE) {
            $this->event = $this->cloneOptions['eventOverride'];
        }

        if (!empty($this->cloneOptions['replaceText'])) {
            $this->name = str_replace($this->cloneOptions['replaceText'], $this->cloneOptions['replaceWith'], $this->name);
            $this->description = str_replace($this->cloneOptions['replaceText'], $this->cloneOptions['replaceWith'], $this->description);
        }

        if ($this->cloneOptions['weightAdjust']) {
            $this->weight += $this->cloneOptions['weightAdjustmentAmount'];
        }



        $this->criteria = $this->cCompetitionCriteria;
        $this->registration = $this->cRegFields;

        foreach ($this->criteria as $c => $criteria) {
            $this->criteria[$c]->isNewRecord = true;
        }

        foreach ($this->registration as $c => $reg) {
            $this->registration[$c]->isNewRecord = true;
        }

        $this->isNewRecord = true;
        unset($this->id);
        return $this->save();

    }//end _clone()


    public function afterSave()
    {
        foreach ($this->criteria as $c) {
            $c->competition = $this->id;
            $c->save();
        }

        foreach ($this->registration as $f) {
            $f->competition = $this->id;
            $f->save();
        }

        foreach ($this->_criteriaToDelete as $c) {
            $c->delete();
        }

        foreach ($this->_registrationToDelete as $f) {
            $f->delete();
        }

        return parent::afterSave();

    }//end afterSave()


    public function beforeDelete()
    {

        // delete all your minions
        Scores::model()->deleteAllByAttributes(["competition" => $this->id]);
        Results::model()->deleteAllByAttributes(["competition" => $this->id]);
        EntryCompetitions::model()->deleteAllByAttributes(["competition" => $this->id]);
        CompetitionCriteria::model()->deleteAllByAttributes(["competition" => $this->id]);
        CompetitionFields::model()->deleteAllByAttributes(["competition" => $this->id]);
        return parent::beforeDelete();

    }//end beforeDelete()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Event' => [
                self::BELONGS_TO,
                'Events',
                'event',
            ],
            'CompetitionCriteria' => [
                self::HAS_MANY, 'CompetitionCriteria', "competition", "with" => ["Criteria"], 'index' => 'criteria', 'order' => 'weight'
            ],
            'Entries' => [
                self::MANY_MANY,
                'Entry',
                'entry_competitions(competition, entry)',
            ],
            'CEntries' => [
                self::HAS_MANY,
                'EntryCompetitions',
                'competition',
            ],
            'cCompetitionCriteria' => [
                self::HAS_MANY, 'CompetitionCriteria', "competition", 'index' => 'criteria', 'order' => 'weight'
            ],
            'cRegFields' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', 'index' => 'field'
            ],
            'Event_mc' => [
                self::BELONGS_TO, 'Events', 'event', 'condition' => 'Event_mc.mc=true'
            ],
            'Event_judge' => [
                self::BELONGS_TO, 'Events', 'event', 'condition' => 'judge=true'
            ],
            'Event_results' => [
                self::BELONGS_TO, 'Events', 'event', 'condition' => 'results=true'
            ],
            'Fields' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', "with" => [
                    "Field",
                    'Field.Type',
                ], 'index' => 'field', 'order' => 'Fields.weight'
            ],
            'RegFields' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', "with" => [
                    "Field",
                    'Field.Type',
                ], 'index' => 'field'
            ],
            'Fields_mc' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', 'condition' => 'Fields_mc.mc=true', "with" => ["Field"]
            ],
            'Fields_judge' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', 'condition' => 'judge=true', "with" => ["Field"]
            ],
            'Fields_report' => [
                self::HAS_MANY, 'CompetitionFields', 'competition', 'condition' => 'report=true', "with" => ["Field"]
            ],
            'Results' => [
                self::HAS_MANY,
                'Results',
                'competition',
            ],
            'CriteriaCount' => [
                self::STAT,
                'CompetitionCriteria',
                "competition",
            ],
            'EntryCount' => [
                self::STAT,
                'EntryCompetitions',
                'competition',
            ],
            'FieldCount' => [
                self::STAT,
                'CompetitionFields',
                'competition',
            ],
            'ScoreCount' => [
                self::STAT,
                'Scores',
                'competition',
            ],
        ];

    }//end relations()


}//end class
