<?php

/**
 * This is the model class for table "dev_results".
 *
 * The followings are the available columns in table 'dev_results':
 *
 * @property integer $entry
 * @property integer $competition
 * @property double $score
 *
 * The followings are the available model relations:
 * @property EntryCompetitions $entry0
 * @property EntryCompetitions $competition0
 */
class Results extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return Results the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'results';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'entry, competition',
                'required',
            ],
            [
                'entry, competition', 'numerical', 'integerOnly' => true
            ],
            [
                'score',
                'numerical',
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'entry, competition, score', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'CEntry'      => [
                self::BELONGS_TO,
                'EntryCompetitions',
                'entry, competition',
            ],
            'Competition' => [
                self::BELONGS_TO,
                'EntryCompetitions',
                'competition, entry',
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'entry'       => 'Entry',
            'competition' => 'Competition',
            'score'       => 'Score',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('entry', $this->entry);
        $criteria->compare('competition', $this->competition);
        $criteria->compare('score', $this->score);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
