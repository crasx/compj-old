<?php

/**
 * This is the model class for table "dev_fields".
 *
 * The followings are the available columns in table 'dev_fields':
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property boolean $field
 * @property boolean $mc
 * @property boolean $register
 *
 * The followings are the available model relations:
 * @property CompetitionFields[] $competitionFields
 * @property FieldTypes $type0
 * @property Entry[] $devEntries
 */
class Fields extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return Fields the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'fields';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'competitionFields' => [
                self::HAS_MANY,
                'CompetitionFields',
                'field',
            ],
            'Type'              => [
                self::BELONGS_TO,
                'FieldTypes',
                'type',
            ],
            'Entries'           => [
                self::MANY_MANY,
                'Entry',
                'dev_entry_fields(field, entry)',
            ],
        ];

    }//end relations()


    public function getValidTypes()
    {
        $F  = FieldTypes::model()->findAll(['order' => 'id']);
        $AF = [];
        foreach ($F as $field) {
            $AF[$field->id] = $field->name;
        }

        return $AF;

    }//end getValidTypes()


    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            // array('permissions', 'checkrules'),
            [
                'name,  type',
                'required',
            ],
            [
                'type',
                'typeExists',
            ],
            // special check for exist
            [
                'description',
                'safe',
            ],
        ];

    }//end rules()


    public function typeExists($attribute, $params)
    {

        $T = FieldTypes::model()->findByPk($this->type);
        if ($T == null) {
            $this->addError($attribute, 'Invalid field type');
        }

    }//end typeExists()


    public function hasValues()
    {
        if ($this->isNewRecord) {
            return false;
        }

        $r = (int) EntryFields::model()->countByAttributes(["field" => $this->id]);
        return $r > 0;

    }//end hasValues()


}//end class
