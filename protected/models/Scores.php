<?php

/**
 * This is the model class for table "dev_scores".
 *
 * The followings are the available columns in table 'dev_scores':
 *
 * @property integer $criteria
 * @property integer $score
 * @property integer $user
 * @property integer $entry
 * @property integer $competition
 *
 * The followings are the available model relations:
 * @property CompetitionCriteria $criteria0
 * @property EntryCompetitions $competition0
 * @property EntryCompetitions $entry0
 * @property Users $user0
 */
class Scores extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return Scores the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'scores';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'criteria, score, user, entry, competition',
                'required',
            ],
            [
                'criteria, score, user, entry, competition', 'numerical', 'integerOnly' => true
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'criteria, score, user, entry, competition', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Criteria'    => [
                self::BELONGS_TO,
                'CompetitionCriteria',
                'criteria',
            ],
            'Competition' => [
                self::BELONGS_TO,
                'EntryCompetitions',
                'competition',
            ],
            'Entry'       => [
                self::BELONGS_TO,
                'EntryCompetitions',
                'entry',
            ],
            'User'        => [
                self::BELONGS_TO,
                'Users',
                'user',
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'criteria'    => 'Criteria',
            'score'       => 'Score',
            'user'        => 'User',
            'entry'       => 'Entry',
            'competition' => 'Competition',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('criteria', $this->criteria);
        $criteria->compare('score', $this->score);
        $criteria->compare('user', $this->user);
        $criteria->compare('entry', $this->entry);
        $criteria->compare('competition', $this->competition);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
