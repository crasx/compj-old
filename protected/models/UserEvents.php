<?php

/**
 * This is the model class for table "dev_user_events".
 *
 * The followings are the available columns in table 'dev_user_events':
 *
 * @property integer $user
 * @property integer $event
 */
class UserEvents extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @return UserEvents the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_events';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'user, event',
                'required',
            ],
            [
                'user, event', 'numerical', 'integerOnly' => true
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'user, event', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            "Event" => [
                self::BELONGS_TO,
                'Events',
                'event',
            ],
            "User"  => [
                self::BELONGS_TO,
                'User',
                'user',
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'user'  => 'User',
            'event' => 'Event',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('user', $this->user);
        $criteria->compare('event', $this->event);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
