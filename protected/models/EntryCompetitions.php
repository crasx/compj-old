<?php

/**
 * This is the model class for table "dev_entry_competitions".
 *
 * The followings are the available columns in table 'dev_entry_competitions':
 *
 * @property integer $entry
 * @property integer $competition
 * @property integer $id
 */
class EntryCompetitions extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @return EntryCompetitions the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'entry_competitions';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'entry, competition',
                'required',
            ],
            [
                'entry, competition, id', 'numerical', 'integerOnly' => true
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'entry, competition, id', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression("now()");
        }

        $this->updated = new CDbExpression("now()");
        return parent::beforeSave();

    }//end beforeSave()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Competition' => [
                self::BELONGS_TO,
                "Competitions",
                "competition",
            ],
            'Entry'       => [
                self::BELONGS_TO,
                "Entry",
                "entry",
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'entry'       => 'Entry',
            'competition' => 'Competition',
            'id'          => 'ID',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('entry', $this->entry);
        $criteria->compare('competition', $this->competition);
        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
