<?php

/**
 * This is the model class for table "dev_settings".
 *
 * The followings are the available columns in table 'dev_settings':
 *
 * @property string $key
 * @property string $value
 */
class Settings extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return Settings the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'key',
                'required',
            ],
            [
                'key', 'length', 'max' => 255
            ],
            [
                'value',
                'safe',
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'key, value', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'key'   => 'Key',
            'value' => 'Value',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('key', $this->key, true);
        $criteria->compare('value', $this->value, true);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
