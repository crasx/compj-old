<?php

/**
 * This is the model class for table "dev_events".
 *
 * The followings are the available columns in table 'dev_events':
 *
 * @property integer $id
 * @property string $name
 * @property integer $publicPermissions
 * @property string $description
 * @property boolean $registration
 * @property boolean $mc
 * @property boolean $judge
 * @property boolean $finished
 *
 * The followings are the available model relations:
 * @property Competitions[] $competitions
 * @property Users[] $devUsers
 */
class Events extends CActiveRecord
{

    protected $users;
    protected $cloneOptions;


    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Events the static model class
     */
    public static function model($className = self::class)
    {
        return parent::model($className);

    }

    public function listAllUsers()
    {
        $judges = UserEvents::model()->findAllByAttributes(["event" => $this->id]);
        $r = [];
        foreach ($judges as $j) {
            $r[$j->id] = $j->username;
        }

        return $r;

    }//end listAllUsers()


    public function listSelectedJudges()
    {
        $judges = $this->Judges;
        $r = [];
        foreach ($judges as $j) {
            $r[$j->id] = $j->username;
        }

        return $r;

    }//end listSelectedJudges()


    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name', 'length', 'max' => 255, 'on' => 'add, edit',],
            ['name, weight', 'required', 'on' => 'add, edit',],
            ['name', 'length', 'max' => 255, 'on' => 'add, edit',],
            ['weight', 'numerical', 'on' => 'add, edit',],
            ['judge, registration, mc, results', 'checkbox', 'on' => 'add, edit',],
            ['users', 'checkUsers', 'on' => 'add, edit',],
            ['description', 'safe', 'on' => 'add, edit',],
            ['cloneOptions', 'checkCloneOptions', 'on' => 'clone']
        ];

    }//end rules()


    public function checkbox($attribute, $params)
    {
        if ($this->attributes[$attribute] == 'on') {
            $this->$attribute = 1;
        } else {
            $this->$attribute = 0;
        }

    }//end checkbox()


    public function afterSave()
    {
        if (in_array($this->getScenario(), ['add', 'edit'])) {
            UserEvents::model()->deleteAllByAttributes(['event' => $this->id]);
            foreach ($this->users as $u => $perms) {
                foreach ($perms as $p) {
                    $ue = new UserEvents;
                    $ue->user = $u;
                    $ue->event = $this->id;
                    $ue->permission = $p;
                    $ue->save();
                }
            }
        }

        parent::afterSave();

    }//end afterSave()


    public function beforeDelete()
    {
        // delete all your minions
        UserEvents::model()->deleteAllByAttributes(["event" => $this->id]);
        Competitions::model()->updateAll(["event" => 0], 'event=:e', [":e" => $this->id]);
        return parent::beforeDelete();

    }//end beforeDelete()

    public function checkCloneOptions($attribute, $params) {
        if (empty($this->cloneOptions)) {
            $this->addError($attribute, 'No options selected');
        }
        $this->cloneOptions = [
            'cloneCompetitions' => !empty($this->cloneOptions['cloneCompetitions']),
            'cloneUsers' => !empty($this->cloneOptions['cloneUsers']),
            'cloneStatusFields' => !empty($this->cloneOptions['cloneStatusFields']),
            'weightAdjust' => !empty($this->cloneOptions['weightAdjust']),
            'weightAdjustmentAmount' => (int) $this->cloneOptions['weightAdjustmentAmount'] ?? 0,
            'replaceEnabled' => !empty($this->cloneOptions['replaceEnabled']),
            'replaceText' => $this->cloneOptions['replaceText'] ?? '',
            'replaceWith' => $this->cloneOptions['replaceWith'] ?? '',
        ];

        if ($this->cloneOptions['replaceEnabled'] && empty($this->cloneOptions['replaceText'])) {
            $this->addError($attribute.'[replaceEnabled]', '"Replace text" is enabled but no text to replace is provided');
        }
    }


    public function clone()
    {
        $users = $this->UserEvents;
        $competitions = $this->Competitions;
        $this->isNewRecord = true;
        unset($this->id);

        if (!empty($this->cloneOptions['replaceText'])) {
            $this->name = str_replace($this->cloneOptions['replaceText'], $this->cloneOptions['replaceWith'], $this->name);
            $this->description = str_replace($this->cloneOptions['replaceText'], $this->cloneOptions['replaceWith'], $this->description);
        }

        if (!empty($this->cloneOptions['prefix'])) {
            $this->name = $this->cloneOptions['prefix'] . ' ' . $this->name;
            $this->description = $this->cloneOptions['prefix'] . ' ' . $this->description;
        }

        if (!$this->cloneOptions['cloneStatusFields']) {
            $this->registration = 0;
            $this->mc = 0;
            $this->judge = 0;
            $this->results = 0;
        }

        if ($this->cloneOptions['weightAdjust']) {
            $this->weight += $this->cloneOptions['weightAdjustmentAmount'];
        }

        if ($this->save(false)) {
            if ($this->cloneOptions['cloneCompetitions'] === true) {
                foreach ($competitions as $competition) {
                    $competition->setScenario('clone');
                    $competition->attributes = [
                        'cloneOptions' => $this->cloneOptions + ['eventOverride' => $this->id],
                    ];

                    if($competition->validate()) {
                        $competition->clone();
                    } else {
                        $this->addError('clone', 'Failed to clone competition: ' . $competition->name);
                    }
                }
            }
            if ($this->cloneOptions['cloneUsers'] === true) {
                foreach ($users as $ue) {
                    $ue->isNewRecord = true;
                    $ue->event = $this->id;
                    $ue->save();
                }
            }

            return true;
        }

        return false;

    }//end _clone()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'events';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Competitions' => [
                self::HAS_MANY,
                'Competitions',
                'event',
            ],
            'UserEvents' => [
                self::HAS_MANY,
                'UserEvents',
                'event',
            ],
            'Judges' => [
                self::MANY_MANY,
                'User',
                'user_events(event, user)',
            ],
            'JudgeCount' => [
                self::STAT, 'User', 'user_events(event, user)', 'condition' => 'user_events.permission="judge"'
            ],
        ];

    }//end relations()

    public function checkUsers($attribute, $params)
    {

        if (empty($_POST['event']) || empty($_POST['event']['users'])) {
            $this->users = [];
            return;
        }

        // build a list of all valid perms
        $validPermissions = EventsController::getAssignablePermissions();

        $this->users = [];

        $permissions = $_POST['event']['permissions'] ?? [];
        $users = $_POST['event']['users'] ?? [];

        // check posted users
        foreach ($users as $userData) {
            if (!empty($userData['uid'])) {
                // Load by email
                $user = User::model()->findAllByPk($userData['uid']);
            } else {
                $user = User::model()->findByAttributes(['email' => $userData['name'] ?? '']);
                if (empty($userData)) {
                    $this->addError('users', 'User not found: ' . $userData['name'] . '. Ask them to register first to add them to this competitioin.');
                }
            }

            if (!$user) {
                continue;
            }

            $this->users[$user->id] = [];

            if (!empty($permissions[$user->id])) {
                foreach ($permissions[$user->id] as $permission) {
                    if (array_key_exists($permission, $validPermissions)) {
                        $this->users[$user->id][] = $permission;
                    }
                }
            }
        }

    }//end checkUsers()

}//end class
