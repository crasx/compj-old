<?php

/**
 * This is the model class for table "field_types".
 *
 * The followings are the available columns in table 'field_types':
 *
 * @property integer $id
 * @property string $name
 * @property string $regex
 * @property boolean $file
 *
 * The followings are the available model relations:
 * @property DevFields[] $devFields
 */
class FieldTypes extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @return FieldTypes the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'field_types';

    }//end tableName()


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [
                'id',
                'required',
            ],
            [
                'id', 'numerical', 'integerOnly' => true
            ],
            [
                'name', 'length', 'max' => 255
            ],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'id, name', 'safe', 'on' => 'search'
            ],
        ];

    }//end rules()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
