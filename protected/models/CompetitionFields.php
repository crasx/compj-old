<?php

/**
 * This is the model class for table "dev_competition_fields".
 *
 * The followings are the available columns in table 'dev_competition_fields':
 *
 * @property integer $field
 * @property integer $competition
 * @property boolean $required
 *
 * The followings are the available model relations:
 * @property Competitions $competition0
 * @property Fields $field0
 */
class CompetitionFields extends CActiveRecord
{


    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return CompetitionFields the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'competition_fields';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'Competition' => [
                self::BELONGS_TO,
                'Competitions',
                'competition',
            ],
            'Field'       => [
                self::BELONGS_TO,
                'Fields',
                'field',
            ],
        ];

    }//end relations()


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'field'       => 'Field',
            'competition' => 'Competition',
            'required'    => 'Required',
        ];

    }//end attributeLabels()


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('field', $this->field);
        $criteria->compare('competition', $this->competition);
        $criteria->compare('required', $this->required);

        return new CActiveDataProvider($this, ['criteria' => $criteria]);

    }//end search()


}//end class
