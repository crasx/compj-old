<?php

class Upload extends CActiveRecord
{
    protected $uploadedFile;

    protected $deleted;

    const MAX_WIDTH = 1920;
    const MAX_HEIGHT = 1280;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param  string $className active record class name.
     * @return CompetitionFields the static model class
     */
    public static function model($className=self::class)
    {
        return parent::model($className);

    }//end model()


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'uploads';

    }//end tableName()


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
//            'Competition' => [
//                self::BELONGS_TO,
//                'Competitions',
//                'competition',
//            ],
//            'Field'       => [
//                self::BELONGS_TO,
//                'Fields',
//                'field',
//            ],
        ];

    }//end relations()

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['entry, client', 'required', 'on' => 'insert'],
            ['uploadedFile', 'resizeUpload', 'on' => 'insert'],
            ['uploadedFile', 'moveUpload', 'on' => 'insert'],
            ['deleted', 'safe'],
        ];

    }//end rules()

    public function resizeUpload($attribute, $params)
    {
        $image = new Imagick($this->uploadedFile['tmp_name']);
        $image->thumbnailImage(self::MAX_WIDTH, self::MAX_HEIGHT, true);
        $image->writeImage($this->uploadedFile['tmp_name']);
    }//end resizeUpload()


    public function moveUpload($attribute, $params)
    {
        $destination = Yii::app()->basePath;
        $file = '/data/uploads/' . date('Y-m') . '/';

        if (!is_dir($destination . $file)) {
            mkdir($destination . $file) || throw new CException('Unable to create upload dir');
        }
        do {
            $fileName = time() . '_' . random_int(0, 999) . '.' . $this->uploadedFile['extension'];
        } while (file_exists($destination . $file . $fileName));

        $fullDestination = $destination . $file . $fileName;
        if (!move_uploaded_file($this->uploadedFile['tmp_name'], $fullDestination)) {
            $this->addError('file', 'Unable to move uploaded file');
        }
        $this->file = $file . $fileName;

    }//end moveUpload()

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->timestamp = new CDbExpression('NOW()');
            $this->session = Yii::app()->session->sessionID;
            $this->ip = ip2long(Yii::app()->request->userHostAddress);
            $this->user = Yii::app()->user->id;
        }

        return parent::beforeSave(); // TODO: Change the autogenerated stub
    }


}//end class
